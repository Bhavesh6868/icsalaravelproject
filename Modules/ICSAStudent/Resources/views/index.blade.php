@extends('icsastudent::layouts.master')

@section('content')
    <h1>Hello World</h1>

    <p>
        This view is loaded from module: {!! config('icsastudent.name') !!}
    </p>
    <form method="POST" action="/logout">
        @csrf

        <button type="submit" class="dropdown-item" style="width: 90%;">
            {{ __('Logout') }}
        </button>
    </form>
@endsection
