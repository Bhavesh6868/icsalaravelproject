<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsacourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsacourses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('course_fee');
            $table->string('duration_month')->nullable();
            $table->string('duration_hours')->nullable();
            $table->enum('is_group_course',['0', '1'])->default('0');
            $table->enum('show_in_register_form',['0', '1'])->default('0');
            $table->enum('status',['active', 'disabled'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsacourse');
    }
}
