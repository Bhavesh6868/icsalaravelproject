<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsastudentdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsastudentdetails', function (Blueprint $table) {
            $table->engine = 'InnoDB'; // <- add this
            $table->id();
            $table->string('civil_id');
            $table->index('civil_id');
            $table->foreign('civil_id')->references('civil_id')->on('icsastudents')->onDelete('cascade')->onUpdate('cascade');

            $table->string('fullname');
            $table->string('avatar')->default('/Images/Users/user.png');
            $table->enum('gender',['male', 'female']);
            $table->date('birthdate');
            $table->string('isd_code')->nullable();
            $table->string('mobile');
            $table->string('currently_residing');
            $table->string('nationality');
            $table->string('occupation');
            $table->text('address')->nullable();
            $table->string('email');
            $table->string('facebook')->nullable();
            $table->string('skype_isd_code')->nullable();
            $table->string('skype')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsastudentdetails');
    }
}
