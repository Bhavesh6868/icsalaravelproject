<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('userid')->unique();
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->nullable();
            $table->string('civil_id')->nullable();
            $table->string('position')->nullable();
            $table->enum('gender',['male', 'female']);
            $table->date('birthdate')->nullable();;
            $table->string('nationality')->nullable();
            $table->string('address')->nullable();
            $table->string('currentRole')->nullable();
            $table->string('currentBranch')->nullable();
            $table->string('avatar')->default('/Images/Users/user.png');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('status',['active', 'disabled'])->default('active');
            $table->enum('isInstructor',['Yes', 'No'])->default('No');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
