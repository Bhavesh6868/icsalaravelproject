<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsapaymentdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsapaymentdetails', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('enrollment_id')->unsigned();
            $table->foreign('enrollment_id')->references('id')->on('icsastudents')->onDelete('cascade')->onUpdate('cascade');
            $table->string('amount');
            $table->string('philippine_peso')->nullable();
            $table->string('transaction_no')->nullable();
            $table->dateTime('transaction_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('date_paid')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->string('payment_status')->nullable();
            $table->string('payment_bank')->nullable();
            $table->string('pay_detail')->nullable();
            $table->string('pay_type')->nullable();
            $table->string('transfer_note')->nullable();
            $table->string('pay_branch');
            $table->bigInteger('oldenrollment_no')->nullable();
            $table->string('process_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsapaymentdetails');
    }
}
