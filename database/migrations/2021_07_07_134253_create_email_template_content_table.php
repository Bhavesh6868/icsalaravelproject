<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailTemplateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_template_content', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('template_id')->unsigned();
            //$table->index('template_id');
            $table->foreign('template_id')->references('id')->on('email_templates')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('icsacourses')->onDelete('cascade')->onUpdate('cascade');
            $table->longText('content');
            $table->enum('status',['active', 'disabled'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_template_content');
    }
}
