<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsapaymentreceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsapaymentrecepts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('enrollment_id')->unsigned();
            $table->foreign('enrollment_id')->references('id')->on('icsastudents')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('payment_id')->unsigned();
            $table->foreign('payment_id')->references('id')->on('icsapaymentdetails')->onDelete('cascade')->onUpdate('cascade');
            $table->string('amount')->nullable();
            $table->string('image')->default('/Images/Users/user.png');
            $table->dateTime('date_paid')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->enum('is_approved',['approve', 'disapprove'])->nullable();
            $table->longText('manualcomment')->nullable();
            $table->longText('resonnotcredit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsapaymentrecepts');
    }
}
