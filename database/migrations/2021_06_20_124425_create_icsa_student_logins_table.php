<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsaStudentLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsa_student_logins', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('enrollment_id');
            $table->index('enrollment_id');
            $table->foreign('enrollment_id')->references('enrollment_id')->on('icsastudents')->onDelete('cascade')->onUpdate('cascade');
            $table->date('birthdate');
            $table->string('mobile');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsa_student_logins');
    }
}
