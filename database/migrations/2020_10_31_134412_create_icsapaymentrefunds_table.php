<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsapaymentrefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsapaymentrefunds', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('enrollment_id')->unsigned();
            $table->foreign('enrollment_id')->references('id')->on('icsastudents')->onDelete('cascade')->onUpdate('cascade');
            $table->string('amount');
            $table->text('reason');
            $table->dateTime('date_refunded')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('pay_branch')->nullable();
            $table->string('process_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsapaymentrefunds');
    }
}
