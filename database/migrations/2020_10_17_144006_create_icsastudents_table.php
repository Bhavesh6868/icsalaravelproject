<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsastudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsastudents', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('enrollment_id');
            $table->string('civil_id');
            $table->index('civil_id');

            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('icsacourses')->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('instructor_id')->unsigned();
            $table->foreign('instructor_id')->references('id')->on('instructors')->onDelete('cascade')->onUpdate('cascade');

            $table->longText('note')->nullable();
            $table->string('status');
            $table->string('frequency');
            $table->string('ielts_type')->nullable();
            $table->string('schedule');
            $table->string('time');
            $table->integer('setHours')->nullable();
            $table->bigInteger('batch_id')->nullable();
            $table->bigInteger('source_id');
            $table->integer('oldenrollment_no')->nullable();
            $table->bigInteger('refer_enrollment')->nullable();
            $table->timestamp('enrollment_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('join_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsastudents');
    }
}
