<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIcsaregistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('icsaregistrations', function (Blueprint $table) {
            $table->id();
            $table->string('registration_id');
            $table->string('fullname');
            $table->enum('gender',['male', 'female']);
            $table->date('birthdate');
            $table->string('isd_code')->nullable();
            $table->string('mobile');
            $table->string('civil_id');
            $table->string('currently_residing');
            $table->string('nationality');
            $table->string('occupation')->nullable();
            $table->text('address')->nullable();
            $table->string('email');
            $table->string('facebook')->nullable();
            $table->string('skype_isd_code')->nullable();
            $table->string('skype')->nullable();
            $table->bigInteger('course_id')->unsigned();
            $table->string('frequency');
            $table->string('schedule');
            $table->string('time');
            $table->timestamp('date_registration')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->bigInteger('branch')->unsigned();
            $table->bigInteger('batch')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icsaregistrations');
    }
}
