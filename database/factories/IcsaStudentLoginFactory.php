<?php

namespace Database\Factories;

use App\Models\IcsaStudentLogin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IcsaStudentLoginFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = IcsaStudentLogin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enrollment_id' => '1210001',
            'birthdate' => '1991-05-05',
            'mobile' => '97585299',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
}
