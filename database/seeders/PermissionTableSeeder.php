<?php

namespace Database\Seeders;

use App\Models\Branches;
use App\Models\Paymentmethod;
use App\Models\Source;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-impersonate',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'branch-list',
            'branch-create',
            'branch-edit',
            'branch-delete',
            'instructor-list',
            'instructor-create',
            'instructor-edit',
            'instructor-delete',
            'icsacourse-list',
            'icsacourse-create',
            'icsacourse-edit',
            'icsacourse-delete',
            'source-list',
            'source-create',
            'source-edit',
            'source-delete',
            'icsabatch-list',
            'icsabatch-create',
            'icsabatch-edit',
            'icsabatch-delete',
            'icsastudent-list',
            'icsastudent-create',
            'icsastudent-edit',
            'icsastudent-delete',
            'paymentmethod-list',
            'paymentmethod-create',
            'paymentmethod-edit',
            'paymentmethod-delete',
            'emailtemplate-list',
            'emailtemplate-create',
            'emailtemplate-edit',
            'emailtemplate-delete',
            'adscourse-list',
            'adscourse-create',
            'adscourse-edit',
            'adscourse-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        $branches = [
            'Kuwait City',
            'Mahboulla',
            'Administration',
            'Saudi',
            'UAE',
            'Qatar',
            'Oman',
            'Bahrain',
        ];

        foreach ($branches as $branch) {
            Branches::create(['name' => $branch]);
        }

        $paymentmehtods = [
            'Cash',
            'K-Net',
            'Transfer From Another',
            'BPI',
            'BDO',
            'Matro Bank',
            'ICSA CBK',
            'ICSA KFH',
            'ICSA Fatoora',
            'Western',
        ];

        foreach ($paymentmehtods as $paymentmehtod) {
            Paymentmethod::create(['name' => $paymentmehtod]);
        }

        $sources = [
            'Brochure/Envelope',
            'Text Message',
            'Internet',
            'Student',
            'Friend',
            'Other'
        ];

        foreach ($sources as $source) {
            Source::create(['name' => $source]);
        }
    }
}
