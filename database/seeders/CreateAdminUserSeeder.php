<?php

namespace Database\Seeders;

use App\Models\RoleBranch;
use App\Models\User;
use App\Models\UserBranch;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'USER'"));
        if (count($checkCounter) > 0) {
            $count = Counters::increment('USER');
            $counter = Counters::getValue('USER');
        } else {
            Counter::create([
                'key' => 'USER',
                'name' => 'user_counter',
                'initial_value' => 0,
                'step' => 1
            ]);
            $count = Counters::increment('USER');
            $counter = Counters::getValue('USER');
        }
        //$request->userid = 'USER' . sprintf("%04d", $counter);
        $user = User::create([
            'userid' => 'USER0001',
            'name' => 'Aamir Faaroq',
            'email' => 'aamir@icsa.us',
            'username' => 'aamir',
            'currentRole' => '1',
            'currentBranch' => '1',
            'password' => bcrypt('12345678')
        ]);

        $count = Counters::increment('USER');
        $user = User::create([
            'userid' => 'USER0002',
            'name' => 'Bhavesh Darji',
            'email' => 'bhaveshdarji386@gmail.com',
            'username' => 'bhavesh',
            'currentRole' => '1',
            'password' => bcrypt('12345678')
        ]);

        $role = Role::create(['name' => 'Admin']);

        $branch = RoleBranch::create([
            'user_id' => 1,
            'branch_id' => 1,
            'role_id' => $role->id,
        ]);

        $branch = RoleBranch::create([
            'user_id' => 2,
            'branch_id' => 1,
            'role_id' => $role->id,
        ]);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);


    }
}
