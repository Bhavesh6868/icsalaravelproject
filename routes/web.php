<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\ImageCropController;
use App\Http\Controllers\InstructorController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\IcsaStudentLoginController;
use App\Http\Controllers\IcsacourseController;
use App\Http\Controllers\IcsastudentController;
use App\Http\Controllers\IcsaregistrationController;

use App\Http\Controllers\SourceController;
use App\Http\Controllers\IcsabatchController;
use App\Http\Controllers\PaymentmethodController;
use App\Http\Controllers\IcsapaymentdetailController;
use App\Http\Controllers\IcsapaymentrefundController;
use App\Http\Controllers\ProspectgenerateLinkController;
use App\Http\Controllers\EmailTemplateController;
use App\Http\Controllers\AdscourseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', '/login');
/*Route::get('/', function () {
    return view('welcome');
});*/



Auth::routes();
Route::impersonate();
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/employee/login', [IcsaStudentLoginController::class, 'login'])->name('employee.login');


Route::group(['middleware' => ['auth']], function() {
   /* Route::get('/users/impersonate/{user_id}', [UserController::class, 'impersonate'])->name('users.impersonate');
    Route::get('/users/impersonate_leave', [UserController::class, 'impersonate_leave'])->name('impersonate.leave');*/

    Route::get('/changeRole/{id}', [UserController::class, 'changeRole'])->name('users.changeRole');
    Route::get('/changeBranch/{id}', [UserController::class, 'changeBranch'])->name('users.changeBranch');
    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index']);

    Route::any('icsastudents/editenrollment', [IcsastudentController::class, 'editenrollment'])->name('icsastudents.editenrollment');
    Route::any('icsastudents/getenrolldetails', [IcsastudentController::class, 'getenrolldetails'])->name('icsastudents.getenrolldetails');

    Route::any('icsastudentsearch', [IcsastudentController::class, 'searchenroll'])->name('icsastudents.searchenroll');
    Route::any('icsapaymentsearch', [IcsapaymentdetailController::class, 'searchenroll'])->name('icsapayment.searchenroll');

    Route::get('icsapayment/addpayment', [IcsapaymentdetailController::class, 'addpayment'])->name('icsapayment.addpayment');
    Route::get('icsapayment/addpayment/{id}', [IcsapaymentdetailController::class, 'icsaaddpayment'])->name('icsapayment.icsaaddpayment');

    Route::get('icsapaymentrefund/refundpayment', [IcsapaymentrefundController::class, 'refundpayment'])->name('icsapayment.refundpayment');
    Route::get('icsapaymentrefund/refundpayment/{id}', [IcsapaymentrefundController::class, 'icsarefundpayment'])->name('icsapayment.icsarefundpayment');

    Route::get('icsapayment/printinvoice', [IcsapaymentdetailController::class, 'showinvoice'])->name('icsapayment.showinvoice');
    Route::get('icsapayment/printinvoice/{id}', [IcsapaymentdetailController::class, 'printinvoice'])->name('icsapayment.printinvoice');

    Route::any('icsapayment/getenrolldetails', [IcsapaymentdetailController::class, 'getenrolldetails'])->name('icsapayment.getenrolldetails');

    Route::resource('users',UserController::class);
    Route::resource('roles',RoleController::class);
    Route::resource('branch',BranchController::class);
    Route::resource('instructor',InstructorController::class);
    Route::resource('icsacourse',IcsacourseController::class);
    Route::resource('icsastudents',IcsastudentController::class);
    Route::resource('source',SourceController::class);
    Route::resource('icsabatch',IcsabatchController::class);
    Route::resource('paymentmethod',PaymentmethodController::class);

    Route::resource('icsapayment',IcsapaymentdetailController::class);
    Route::resource('icsapaymentrefund',IcsapaymentrefundController::class);

    Route::resource('adscourse',AdscourseController::class);
    Route::resource('emailtemplate',EmailTemplateController::class);

    Route::get('emailtemplate/attach/{id}', [EmailTemplateController::class, 'contentlist'])->name('emailtemplate.attach.content');

    Route::get('emailtemplate/attach/create/{id}', [EmailTemplateController::class, 'contentcreate'])->name('emailtemplate.attach.create');

    Route::post('emailtemplate/attach/store/{id}', [EmailTemplateController::class, 'contentstore'])->name('emailtemplate.attach.store');

    Route::get('emailtemplate/attach/{id}/edit/{template_id}', [EmailTemplateController::class, 'contentedit'])->name('emailtemplate.attach.edit');

    Route::PATCH('emailtemplate/attach/{id}/update/{template_id}', [EmailTemplateController::class, 'updatecontent'])->name('emailtemplate.attach.update');

    Route::get('emailtemplate/attach/{template_id}/{id}', [EmailTemplateController::class, 'contentshow'])->name('emailtemplate.attach.show');

    Route::delete('emailtemplate/attach/{id}/{template_id}', [EmailTemplateController::class, 'destroycontent'])->name('emailtemplate.attach.destroy');

    //Route::get('dropzone', [EmailTemplateController::class, 'dropzone']);
    Route::post('dropzone/store/{val}', [EmailTemplateController::class, 'dropzoneStore'])->name('dropzone.store');

    Route::post('icsastudents/getdetails', [IcsastudentController::class, 'getdetails'])->name('icsastudents.getdetails');

    /*Route::get('icsastudent/editenrollment', [IcsastudentController::class, 'editenrollment'])->name('icsastudent.editenrollment');*/



    Route::post('users/getdetails', [UserController::class, 'getdetails'])->name('users.getdetails');

    Route::post('image_crop/upload', [ImageCropController::class, 'upload'])->name('image_crop.upload');


    Route::get('generate/link', [ProspectgenerateLinkController::class, 'generateLinkPage'])->name('generate.link');
    Route::post('generate/link', [ProspectgenerateLinkController::class, 'generateLink'])->name('generate.linkshow');

    Route::get('/manager', function () {
        return 'hello Manager';
    })->name('manager');

});
Route::get('user', function()
{
    return View::make('pages.user');
});
Route::get('tables', function()
{
    return View::make('pages.tables');
});
Route::get('typography', function()
{
    return View::make('pages.typography');
});
Route::get('icons', function()
{
    return View::make('pages.icons');
});
Route::get('map', function()
{
    return View::make('pages.map');
});
Route::get('notifications', function()
{
    return View::make('pages.notifications');
});
Route::get('icsaregistrationform', function()
{
    return View::make('pages.icsaregistration.icsaregistrationform');
});

/*Route::get('icsaregistration', [IcsaregistrationController::class, 'index'])->name('icsaregistration.index');
Route::post('icsaregistration', [IcsaregistrationController::class, 'store'])->name('icsaregistration.store');*/

Route::resource('icsaregistration',IcsaregistrationController::class);

Route::post('icsaregistration/getbatches', [IcsaregistrationController::class, 'getbatches'])->name('icsaregistration.getbatches');

Route::post('icsaregistration/getcountrycode', [IcsaregistrationController::class, 'getcountrycode'])->name('icsaregistration.getcountrycode');

Route::get('refer/link/{user}/{course}/{branch}', [ProspectgenerateLinkController::class, 'referLink'])->name('refer.link');