<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class icsaRemittanceMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    public $images;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
        $this->images = $details['images'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->subject('Mail from ItSolutionStuff.com')
            ->view('emails.icsaRemittanceMail');

        /*if(is_countable($this->images) && count($this->images) > 0 ) {
            foreach($this->images as $filePath){
                $email->attach(public_path('/Images/icsaremittance/').$filePath);
            }
        }*/
        return $email;
    }
}
