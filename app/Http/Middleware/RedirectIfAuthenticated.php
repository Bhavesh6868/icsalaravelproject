<?php

namespace App\Http\Middleware;

use App\Models\RoleBranch;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;
        if(Auth::guard('student')->check()) {
            return redirect()->intended('/icsastudent');
        } else if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            $user->currentRole = Auth::user()->roles()->first()->id;
            /*$user->currentBranch = Auth::user()->userbranches()->first()->branch_id;*/
            $branches = RoleBranch::where('role_id', $user->currentRole)->orderBy('branch_id', 'Asc')->get();

            if(is_countable($branches)) {
                $user->currentBranch = $branches->first()->branch_id;
            }
            $user->update();

            $rolePermissions = [];
            foreach (Auth::user()->roles->pluck('id') as $roleId) {
                if(Auth::user()->roles()->first()->id == $roleId) {
                    $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
                        ->where("role_has_permissions.role_id",$roleId)
                        ->get();
                }
            }

            $permissionArray = [];
            foreach ($rolePermissions as $permissions) {
                $permissionArray[] = ($permissions->name);
            }
            Session::put('permission_array', $permissionArray);
        }
        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return redirect(RouteServiceProvider::HOME);
            }
            /*if (Auth::check() && Auth::user()->roles->contains('name', 'Admin')) {
                return redirect('/dashboard');

            } else if (Auth::check() && Auth::user()->roles->contains('name', 'Manager')) {
                return redirect('/manager');
            }*/
        }

        return $next($request);
    }
}
