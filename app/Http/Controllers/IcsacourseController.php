<?php

namespace App\Http\Controllers;

use App\Models\Icsacourse;
use Illuminate\Http\Request;

class IcsacourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icsacourse = Icsacourse::orderBy('id', 'DESC')->get();

        return view('pages.icsacourses.index', compact('icsacourse'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.icsacourses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'course_fee' => 'required|integer',
            'duration_month' => 'required|integer',
            'duration_hours' => 'required|integer',
        ]);

        $icsacourse = Icsacourse::create($request->all());

        return redirect()->route('icsacourse.index')
            ->with('success', 'ICSA Course created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsacourse $icsacourse
     * @return \Illuminate\Http\Response
     */
    public function show(Icsacourse $icsacourse)
    {
        return view('pages.icsacourses.show', compact('icsacourse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsacourse $icsacourse
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsacourse $icsacourse)
    {
        return view('pages.icsacourses.edit', compact('icsacourse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Icsacourse $icsacourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsacourse $icsacourse)
    {
        $this->validate($request, [
            'name' => 'required',
            'course_fee' => 'required|integer',
            'duration_month' => 'required|integer',
            'duration_hours' => 'required|integer',
        ]);

        $icsacourse->update($request->all());

        return redirect()->route('icsacourse.index')
            ->with('success', 'ICSA Course Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsacourse $icsacourse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsacourse $icsacourse)
    {
        if ($icsacourse->status == 'active') {
            $icsacourse->status = 'disabled';
        } else {
            $icsacourse->status = 'active';
        }
        $icsacourse->update();
        return redirect()->route('icsacourse.index')
            ->with($icsacourse->status =='active' ? 'success' : 'error', 'ICSA Course  ' . ucfirst($icsacourse->status) . ' successfully');
    }
}
