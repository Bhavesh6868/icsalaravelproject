<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Models\Instructor;
use App\Models\RoleBranch;
use App\Models\User;
use App\Models\UserBranch;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','store']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id', 'DESC')->get();//->paginate(5);
        return view('pages.users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_option_roles = array('' => 'Select Roles');

        $roles = $empty_option_roles + Role::pluck('name', 'name')->all();
        $branches = Branches::pluck('name', 'id')->all();
        return view('pages.users.create', compact('roles', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*echo "<pre>";
        var_dump($request->input('roles'));
        var_dump($request->input('branch'));
        echo "</pre>";
        die;*/
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'username' => 'required|unique:users,username',
            'civil_id' => 'required|unique:users,civil_id',
            'roles' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'USER'"));
        if (count($checkCounter) > 0) {
            $count = Counters::increment('USER');
            $counter = Counters::getValue('USER');
        } else {
            Counter::create([
                'key' => 'USER',
                'name' => 'user_counter',
                'initial_value' => 0,
                'step' => 1
            ]);
            $count = Counters::increment('USER');
            $counter = Counters::getValue('USER');
        }
        $request->userid = 'USER' . sprintf("%04d", $counter);
        //die;

        $input = $request->all();
        $input['userid'] = 'USER' . sprintf("%04d", $counter);
        $input['password'] = Hash::make($input['password']);
        /*echo "<pre>";
                var_dump($input);
                die;*/
        if ($request->hasFile('avatar')) {
            if (file_exists(public_path() . '/Images/crop_image/' . $request->username . '.png')) {
                chmod(public_path('/Images/'), 0777);
                File::move(public_path('/Images/crop_image/' . $request->username . '.png'), public_path('/Images/Users/' . $request->username . '.png'));

                $input['avatar'] = '/Images/Users/' . $request->username . '.png';
            }
        }
        $user = User::create($input);
        if ($user) {

            if (is_countable($request->input('roles')) && count($request->input('roles')) > 0) {
                //DB::table('user_branch')->where('user_id', $id)->delete();
                foreach ($request->input('roles') as $key => $role) {
                    //var_dump($role);
                    // echo count($request->input('branch')[$key]);
                    if (array_key_exists($key, $request->input('branch'))) {
                        $role = Role::findByName($role[0]);
                        foreach ($request->input('branch')[$key] as $k => $branch) {
                            $rolebranch = new RoleBranch();
                            $rolebranch->user_id = $user->id;
                            $rolebranch->role_id = $role->id;
                            $rolebranch->branch_id = $branch;
                            $rolebranch->save();
                        }

                        //var_dump($request->input('branch')[$key]);
                    }
                }
            }
            if ($input['isInstructor'] == 'Yes') {

                $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'INST'"));
                if (count($checkCounter) > 0) {
                    $count = Counters::increment('INST');
                    $counter = Counters::getValue('INST');
                } else {
                    Counter::create([
                        'key' => 'INST',
                        'name' => 'instructor_counter',
                        'initial_value' => 0,
                        'step' => 1
                    ]);
                    $count = Counters::increment('INST');
                    $counter = Counters::getValue('INST');
                }

                $instructor = new Instructor();
                $instructor->instructor_id = 'INST' . sprintf("%04d", $counter);;
                $instructor->name = $user->name;
                $instructor->speaking_name = $user->username;
                //$instructor->user_id = $user->id;
                $user->instructor()->save($instructor);
            }
        }
        $user->assignRole($request->input('roles'));

        /*if (is_countable($request->input('branch')) && count($request->input('branch')) > 0) {
            //DB::table('user_branch')->where('user_id', $id)->delete();
            foreach ($request->input('branch') as $branch) {
                $userbranch = new UserBranch();
                $userbranch->user_id = $user->id;
                $userbranch->branch_id = $branch;
                $userbranch->save();
            }
        }*/

        return redirect()->route('users.index')
            ->with('success', 'User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('pages.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empty_option_roles = array('' => 'Select Roles');
        $user = User::find($id);
        $roles = $empty_option_roles + Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        $branches = Branches::pluck('name', 'id')->all();
        //$userBranch = User::find($id);//->with('userbranches');
        /*foreach($userBranch->userbranches as $filler) {
            echo $filler;
        }*/
        $userBranch = [];
        $userRole = [];
        /*$userBranch = DB::table('branches AS pr')
            ->select(DB::raw('pr.name, pr.id'))
            ->join('user_branch AS pu', 'pu.branch_id', '=', 'pr.id')
            ->join('users AS u', 'u.id', '=', 'pu.user_id')
            ->where('u.id', '=', $id)
            ->pluck('id')->all();*/

        $userBranchs = DB::table('roles AS pr')
            ->select(DB::raw('pr.name, pu.branch_id'))
            ->join('role_branches AS pu', 'pu.role_id', '=', 'pr.id')
            //->join('users AS u', 'u.id', '=', 'pu.user_id')
            ->where('pu.user_id', '=', $id)
            ->get();
/*var_dump($userBranchs);
die;*/
        //$userBranchs = RoleBranch::where('user_id', $id)->get();

        foreach ($userBranchs as $k => $v) {
            //echo $k;
            $userBranch[$v->name][] = $v->branch_id;
        }
        /*echo "<pre>";
        var_dump($userBranch);
        echo "</pre>";
        die;*/
        return view('pages.users.edit', compact('user', 'branches', 'userBranch', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'username' => 'required|unique:users,username,' . $id,
            'civil_id' => 'required|unique:users,civil_id,' . $id,
            'roles' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048,' . $id,
        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        /*if (is_countable($request->input('branch')) && count($request->input('branch')) > 0) {
            DB::table('user_branch')->where('user_id', $id)->delete();
            foreach ($request->input('branch') as $branch) {
                $userbranch = new UserBranch();
                $userbranch->user_id = $id;
                $userbranch->branch_id = $branch;
                $userbranch->save();
            }
        }*/
        //die;
        $user = User::with('instructor')->find($id);
        if ($request->hasFile('avatar')) {
            if (file_exists(public_path() . '/Images/crop_image/' . $user->username . '.png')) {
                chmod(public_path('/Images/'), 0777);
                File::move(public_path('/Images/crop_image/' . $user->username . '.png'), public_path('/Images/Users/' . $user->username . '.png'));

                $input['avatar'] = '/Images/Users/' . $user->username . '.png';
            }
        }
        if ($user->update($input)) {

            if (is_countable($request->input('roles')) && count($request->input('roles')) > 0) {
                DB::table('role_branches')->where('user_id', $id)->delete();
                foreach ($request->input('roles') as $key => $role) {
                    //var_dump($role);
                    // echo count($request->input('branch')[$key]);
                    if (array_key_exists($key, $request->input('branch'))) {
                        $role = Role::findByName($role[0]);
                        foreach ($request->input('branch')[$key] as $k => $branch) {
                            $rolebranch = new RoleBranch();
                            $rolebranch->user_id = $user->id;
                            $rolebranch->role_id = $role->id;
                            $rolebranch->branch_id = $branch;
                            $rolebranch->save();
                        }

                        //var_dump($request->input('branch')[$key]);
                    }
                }
            }

            if ($input['isInstructor'] == 'Yes') {

                if ($user->instructor()->exists()) {
                    /*echo 'sss';
                    die;*/
                    //$ins['instructor_id'] = 'INST002';
                    $ins['name'] = $user->name;
                    $ins['speaking_name'] = $user->username;
                    //$ins['user_id'] = $user->civil_id;
                    $user->instructor()->update($ins);

                } else {
                    $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'INST'"));
                    if (count($checkCounter) > 0) {
                        $count = Counters::increment('INST');
                        $counter = Counters::getValue('INST');
                    } else {
                        Counter::create([
                            'key' => 'INST',
                            'name' => 'instructor_counter',
                            'initial_value' => 0,
                            'step' => 1
                        ]);
                        $count = Counters::increment('INST');
                        $counter = Counters::getValue('INST');
                    }

                    $instructor = new Instructor();
                    $instructor->instructor_id = 'INST' . sprintf("%04d", $counter);;
                    $instructor->name = $user->name;
                    $instructor->speaking_name = $user->username;
                    //$instructor->user_id = $user->id;
                    $user->instructor()->save($instructor);
                }


            }
        }
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->status == 'active') {
            $user->status = 'disabled';
        } else {
            $user->status = 'active';
        }
        $user->update();
        return redirect()->route('users.index')
            ->with($user->status == 'active' ? 'success' : 'error', 'User ' . ucfirst($user->status) . ' successfully');
    }

///1198432 -1198383
    public function changeRole(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);
        //var_dump(Auth::user()->roles()->first()->id);
        $user->currentRole = $id;

        /** @var $branches
         Added New for role wise branches
         */
        $branches = RoleBranch::where('role_id', $user->currentRole)->orderBy('branch_id', 'Asc')->get();

        if(is_countable($branches)) {
            $user->currentBranch = $branches->first()->branch_id;
        }
        /**
         * End Add role wise branch
        */

        $user->update();

        $rolePermissions = [];
        foreach (Auth::user()->roles->pluck('id') as $roleId) {
            if ($id == $roleId) {
                $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
                    ->where("role_has_permissions.role_id", $roleId)
                    ->get();
            }
        }

        $permissionArray = [];
        foreach ($rolePermissions as $permissions) {
            $permissionArray[] = ($permissions->name);
        }
        Session::put('permission_array', $permissionArray);
        return redirect('/dashboard');
    }

    public function changeBranch(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);
        //var_dump(Auth::user()->roles()->first()->id);
        $user->currentBranch = $id;
        $user->update();
        return redirect('/dashboard');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getdetails(Request $request)
    {
        if ($request->ajax()) {
            $userId = $request->userid;
            if ($userId == 0) {
                $employees = [];
            } else {
                $employees = User::select('*')->where('id', $userId)->get();
            }
            $userdata = [];
            foreach ($employees as $emp) {
                $userdata['name'] = $emp->name;
                $userdata['username'] = $emp->username;
                $userdata['email'] = $emp->email;
                $userdata['mobile'] = $emp->mobile;
                $userdata['birthdate'] = $emp->birthdate;
                $userdata['civil_id'] = $emp->civil_id;
                $userdata['position'] = $emp->position;
                $userdata['gender'] = $emp->gender;
                $userdata['nationality'] = $emp->nationality;
            }
            return response()->json(['userdetail' => $userdata]);
        }
    }
}