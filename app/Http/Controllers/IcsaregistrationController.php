<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Models\Countrie;
use App\Models\Icsabatch;
use App\Models\Icsacourse;
use App\Models\Icsaregistration;
use App\Models\Instructor;
use Illuminate\Http\Request;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;

class IcsaregistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empty_option = array(
            '' => 'Select Option'
        );
        $branches = Branches::where('status', 'active')->where('id', '!=', '3')->pluck('name', 'id')->all();
        $branches = $empty_option + $branches;

        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $icsabatch = Icsabatch::where('status', 'active')->pluck('name', 'id')->all();
        $icsabatch = $empty_option + $icsabatch;

        $nationality = Countrie::pluck('nicename', 'id')->all();
        $nationality = $empty_option + $nationality;

        $isd = Countrie::all();

        $batch = $empty_option;
        return view('pages.icsaregistration.icsaregistrationform', compact('courses', 'branches', 'icsabatch', 'nationality', 'isd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'email' => 'required|email',
            'civil_id' => 'required',
            'birthdate' => 'required|date_format:Y-m-d|before:today',
            'mobile' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'currently_residing' => 'required',
            'course_id' => 'required',
            'branch' => 'required',
            'frequency' => 'required',
            'time' => 'required',
            'schedule' => 'required',
            //'batch' => 'required',
        ]);

        $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'ICSA'"));
        if (count($checkCounter) > 0) {
            $count = Counters::increment('ICSA');
            $counter = Counters::getValue('ICSA');
        } else {
            Counter::create([
                'key' => 'ICSA',
                'name' => 'icsa_registration_counter',
                'initial_value' => 0,
                'step' => 1
            ]);
            $count = Counters::increment('ICSA');
            $counter = Counters::getValue('ICSA');
        }

        $input = $request->all();
        $input['registration_id'] = 'ICSA' . sprintf("%04d", $counter);

        $icsacourse = Icsaregistration::create($input);

        return redirect()->route('icsaregistration.show', $icsacourse->id);
        // ->with('success', 'ICSA Course created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsaregistration $icsaregistration
     * @return \Illuminate\Http\Response
     */
    public function show(Icsaregistration $icsaregistration)
    {
        $course = Icsacourse::select('name')->where('id', $icsaregistration->course_id)->get();
        return view('pages.icsaregistration.icsaregistrationsuccess', compact('icsaregistration', 'course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsaregistration $icsaregistration
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsaregistration $icsaregistration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Icsaregistration $icsaregistration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsaregistration $icsaregistration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsaregistration $icsaregistration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsaregistration $icsaregistration)
    {
        //
    }

    public function getbatches(Request $request)
    {
        if ($request->ajax()) {
            $courseId = $request->courseid;
            $branchId = $request->branchid;

            $icsabatch = Icsabatch::where('course_id', $courseId)->where('branch_id', $branchId)->pluck('name', 'id')->all();
            $selectOption = "<option value=''>Select Batch</option>";
            if (count($icsabatch) > 0) {
                foreach ($icsabatch as $key => $batch) {
                    $selectOption .= "<option value='".$key."'>".$batch."</option>";
                }
                return response()->json(['code' => 200, 'icsabatch' => $selectOption]);
            } else {
                return response()->json(['code' => 400, 'icsabatch' => $selectOption]);
            }
        }
    }

    public function getcountrycode(Request $request) {
        if ($request->ajax()) {
            $branchId = $request->countryid;

            $country = Countrie::where('id', $branchId)->pluck('phonecode')->all();
            //var_dump($country);
            if (count($country) > 0) {
                return response()->json(['code' => 200, 'country' => $country]);
            } else {
                return response()->json(['code' => 400, 'country' => '']);
            }
        }
    }
}
