<?php

namespace App\Http\Controllers;

use App\Models\Countrie;
use App\Models\Icsabatch;
use App\Models\Icsacourse;
use App\Models\Icsapaymentdetail;
use App\Models\Icsapaymentrefund;
use App\Models\Icsaregistration;
use App\Models\Icsastudent;
use App\Models\Icsastudentdetails;
use App\Models\Instructor;
use App\Models\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;
use File;

class IcsastudentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:icsabatch-list|icsabatch-create|icsabatch-edit|icsabatch-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:icsabatch-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:icsabatch-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:icsabatch-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');
        $course_id = '';
        $instructor_id = '';

        $empty_option = array(
            '' => 'Select Course Option'
        );
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $empty_option = array(
            '' => 'Select Instructor Option'
        );
        $instructor = Instructor::where('status', 'active')->pluck('name', 'id')->all();
        $instructor = $empty_option + $instructor;

        /*$icsastudents = Icsastudent::orderBy('id', 'DESC')->get();//->paginate(5);*/
        $icsastudents = Icsastudent::query();
        $icsastudents->when(auth()->user()->currentBranch != '3', function ($q) {
            return $q->where('enrollment_id', 'Like', auth()->user()->currentBranch.'%');
        });
        /*$icsastudents->when(request('filter_by') == 'date', function ($q) {
            return $q->orderBy('id', 'desc');
        });*/
        $icsastudents = $icsastudents->whereBetween('enrollment_date', array($start_date, $end_date));
        $icsastudents = $icsastudents->orderBy('id', 'DESC')->get();
        return view('pages.icsastudent.index', compact('icsastudents', 'start_date', 'end_date', 'courses', 'course_id', 'instructor', 'instructor_id'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /***
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchenroll(Request $request) {
        if((isset($request->start_date) && isset($request->end_date)) || isset($request->course_id) || isset($request->instructor_id)) {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $course_id = $request->course_id;
            $instructor_id = $request->instructor_id;
        } else {
            $start_date = date('Y-m-01');
            $end_date = date('Y-m-t');
            $course_id = '';
            $instructor_id = '';
        }

        $empty_option = array(
            '' => 'Select Course Option'
        );
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $empty_option = array(
            '' => 'Select Instructor Option'
        );
        $instructor = Instructor::where('status', 'active')->pluck('name', 'id')->all();
        $instructor = $empty_option + $instructor;

        /*$icsastudents = Icsastudent::orderBy('id', 'DESC')->get();//->paginate(5);*/
        $icsastudents = Icsastudent::query();
        $icsastudents->when($course_id != '', function($q) use ($course_id) {
            return $q->where('course_id', '=', $course_id);
        });
        $icsastudents->when($instructor_id != '', function($q) use ($instructor_id) {
            return $q->where('instructor_id', '=', $instructor_id);
        });
        $icsastudents->when(auth()->user()->currentBranch != '3', function ($q) {
            return $q->where('enrollment_id', 'Like', auth()->user()->currentBranch.'%');
        });
        /*$icsastudents->when(request('filter_by') == 'date', function ($q) {
            return $q->orderBy('id', 'desc');
        });*/

        $icsastudents = $icsastudents->whereBetween('enrollment_date', array($start_date, $end_date));
        $icsastudents = $icsastudents->orderBy('id', 'DESC')->get();
        return view('pages.icsastudent.index', compact('icsastudents', 'start_date', 'end_date', 'courses', 'course_id', 'instructor', 'instructor_id'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_option = array(
            '' => 'Select Option'
        );
        $instructor = Instructor::where('status', 'active')->pluck('name', 'id')->all();
        $instructor = $empty_option + $instructor;
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $icsabatch = Icsabatch::where('status', 'active')->pluck('name', 'id')->all();
        $icsabatch = $empty_option + $icsabatch;

        $source = Source::where('status', 'active')->pluck('name', 'id')->all();
        $source = $empty_option + $source;

        $nationality = Countrie::pluck('nicename', 'id')->all();
        $nationality = $empty_option + $nationality;

        $isd = Countrie::all();

        $batch = $empty_option;
        return view('pages.icsastudent.create', compact('courses', 'instructor', 'icsabatch', 'source', 'nationality', 'isd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'birthdate' => 'required|date_format:Y-m-d|before:today',
            'civil_id' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'currently_residing' => 'required',
            'occupation' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'join_date' => 'required',
            'course_id' => 'required',
            'instructor_id' => 'required',
            'frequency' => 'required',
            'schedule' => 'required',
            'time' => 'required',
            'source_id' => 'required',
        ]);

        $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'icsa_branch_" . auth()->user()->currentBranch . "'"));
        if (count($checkCounter) > 0) {
            $count = Counters::increment('icsa_branch_' . auth()->user()->currentBranch);
            $counter = Counters::getValue('icsa_branch_' . auth()->user()->currentBranch);
        } else {
            Counter::create([
                'key' => 'icsa_branch_' . auth()->user()->currentBranch,
                'name' => 'icsa_enrollment_branch_' . auth()->user()->currentBranch,
                'initial_value' => 0,
                'step' => 1
            ]);
            $count = Counters::increment('icsa_branch_' . auth()->user()->currentBranch);
            $counter = Counters::getValue('icsa_branch_' . auth()->user()->currentBranch);
        }

        $schedule = [];
        if (count($request->schedule) > 0) {
            foreach ($request->schedule as $sch) {
                $schedule[] = $sch;
            }
        }
        //die;
        $enrollNo = auth()->user()->currentBranch . date('y') . sprintf("%04d", $counter);
        $inputStudent['enrollment_id'] = auth()->user()->currentBranch . date('y') . sprintf("%04d", $counter);
        $inputStudent['fullname'] = $request->fullname;
        $inputStudent['civil_id'] = $request->civil_id;
        $inputStudent['course_id'] = $request->course_id;
        $inputStudent['instructor_id'] = $request->instructor_id;
        $inputStudent['note'] = $request->note;
        $inputStudent['status'] = 'ENROLLED';
        $inputStudent['frequency'] = $request->frequency;
        $inputStudent['ielts_type'] = $request->ielts_type;
        $inputStudent['schedule'] = implode(',', $schedule);
        $inputStudent['time'] = $request->time;
        $inputStudent['setHours'] = $request->setHours;
        $inputStudent['batch_id'] = $request->batch_id;
        $inputStudent['source_id'] = $request->source_id;
        $inputStudent['join_date'] = $request->join_date;

        $inputStudentDetails['civil_id'] = $request->civil_id;
        $inputStudentDetails['fullname'] = $request->fullname;
        $inputStudentDetails['gender'] = $request->gender;
        $inputStudentDetails['birthdate'] = $request->birthdate;
        $inputStudentDetails['isd_code'] = $request->isd_code;
        $inputStudentDetails['mobile'] = $request->mobile;
        $inputStudentDetails['currently_residing'] = $request->currently_residing;
        $inputStudentDetails['nationality'] = $request->nationality;
        $inputStudentDetails['occupation'] = $request->occupation;
        $inputStudentDetails['address'] = $request->address;
        $inputStudentDetails['email'] = $request->email;
        $inputStudentDetails['facebook'] = $request->facebook;
        $inputStudentDetails['skype_isd_code'] = $request->skype_isd_code;
        $inputStudentDetails['skype'] = $request->skype;

        if ($request->hasFile('avatar')) {
            if (file_exists(public_path() . '/Images/crop_image/' . $request->civil_id . '.png')) {
                chmod(public_path('/Images/'), 0777);
                File::move(public_path('/Images/crop_image/' . $request->civil_id . '.png'), public_path('/Images/ICSAStudents/' . $request->civil_id . '.png'));

                $inputStudentDetails['avatar'] = '/Images/ICSAStudents/' . $request->civil_id . '.png';
            }
        }
        $checkStudent = Icsastudent::with('icsastudentdetails')->where('civil_id', $request->civil_id)->first();

        if ($checkStudent) {
            $icsastudent = Icsastudent::create($inputStudent);
            $checkStudent->icsastudentdetails()->update($inputStudentDetails);
        } else {

            $icsastudent = Icsastudent::create($inputStudent);
            $icsastudentdetail = Icsastudentdetails::create($inputStudentDetails);
        }

        /*return redirect()->route('icsastudent.create')
            ->with('success', 'Student created successfully');*/
        return redirect()->route('icsapayment.icsaaddpayment', ['id' => $icsastudent->id]);
        //->with('enroll', $request->get('enrollment_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsastudent $icsastudent
     * @return \Illuminate\Http\Response
     */
    public function show(Icsastudent $icsastudent)
    {
        $paidAmt = Icsapaymentdetail::where('enrollment_id', $icsastudent->id)->whereIn('pay_detail', ['NEW STUDENT', 'BALANCE PAYMENT'])->value(DB::raw("SUM(amount)"));

        $discountAmt = Icsapaymentdetail::where('enrollment_id', $icsastudent->id)->whereIn('pay_detail', ['DISCOUNT'])->value(DB::raw("SUM(amount)"));

        $additionalAmt = Icsapaymentdetail::where('enrollment_id', $icsastudent->id)->whereIn('pay_detail', ['ADDITIONAL'])->value(DB::raw("SUM(amount)"));

        $refundAmtSum = Icsapaymentrefund::where('enrollment_id', $icsastudent->id)->value(DB::raw("SUM(amount)"));

        return view('pages.icsastudent.show', compact('icsastudent', 'paidAmt', 'discountAmt', 'additionalAmt', 'refundAmtSum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsastudent $icsastudent
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsastudent $icsastudent)
    {
        $empty_option = array(
            '' => 'Select Option'
        );
        $instructor = Instructor::where('status', 'active')->pluck('name', 'id')->all();
        $instructor = $empty_option + $instructor;
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        //$icsabatch = Icsabatch::where('status', 'active')->pluck('name', 'id')->all();
        $icsabatch = Icsabatch::where('course_id', $icsastudent->course_id)->where('branch_id', auth()->user()->currentRole)->pluck('name', 'id')->all();

        $icsabatch = $empty_option + $icsabatch;

        $source = Source::where('status', 'active')->pluck('name', 'id')->all();
        $source = $empty_option + $source;

        $nationality = Countrie::pluck('nicename', 'id')->all();
        $nationality = $empty_option + $nationality;

        $batch = $empty_option;

        $isd = Countrie::all();

        return view('pages.icsastudent.edit', compact('courses', 'instructor', 'icsabatch', 'source', 'nationality', 'icsastudent', 'isd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Icsastudent $icsastudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsastudent $icsastudent)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'birthdate' => 'required|date_format:Y-m-d|before:today',
            'civil_id' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'currently_residing' => 'required',
            'occupation' => 'required',
            'email' => 'required|email',
            'mobile' => 'required',
            'join_date' => 'required',
            'course_id' => 'required',
            'instructor_id' => 'required',
            'frequency' => 'required',
            'schedule' => 'required',
            'time' => 'required',
            'source_id' => 'required',
        ]);

        $schedule = [];
        if (count($request->schedule) > 0) {
            foreach ($request->schedule as $sch) {
                $schedule[] = $sch;
            }
        }

        $inputStudent['enrollment_id'] = $icsastudent->enrollment_id;
        $inputStudent['fullname'] = $request->fullname;
        $inputStudent['civil_id'] = $request->civil_id;
        $inputStudent['course_id'] = $request->course_id;
        $inputStudent['instructor_id'] = $request->instructor_id;
        $inputStudent['note'] = $request->note;
        $inputStudent['status'] = 'ENROLLED';
        $inputStudent['frequency'] = $request->frequency;
        $inputStudent['ielts_type'] = $request->ielts_type;
        $inputStudent['schedule'] = implode(",", $schedule);
        $inputStudent['time'] = $request->time;
        $inputStudent['setHours'] = $request->setHours;
        $inputStudent['batch_id'] = $request->batch_id;
        $inputStudent['source_id'] = $request->source_id;
        $inputStudent['join_date'] = $request->join_date;

        $inputStudentDetails['civil_id'] = $request->civil_id;
        $inputStudentDetails['fullname'] = $request->fullname;
        $inputStudentDetails['gender'] = $request->gender;
        $inputStudentDetails['birthdate'] = $request->birthdate;
        $inputStudentDetails['isd_code'] = $request->isd_code;
        $inputStudentDetails['mobile'] = $request->mobile;
        $inputStudentDetails['currently_residing'] = $request->currently_residing;
        $inputStudentDetails['nationality'] = $request->nationality;
        $inputStudentDetails['occupation'] = $request->occupation;
        $inputStudentDetails['address'] = $request->address;
        $inputStudentDetails['email'] = $request->email;
        $inputStudentDetails['facebook'] = $request->facebook;
        $inputStudentDetails['skype_isd_code'] = $request->skype_isd_code;
        $inputStudentDetails['skype'] = $request->skype;

        if ($request->hasFile('avatar')) {
            if (file_exists(public_path() . '/Images/crop_image/' . $request->civil_id . '.png')) {
                chmod(public_path('/Images/'), 0777);
                File::move(public_path('/Images/crop_image/' . $request->civil_id . '.png'), public_path('/Images/ICSAStudents/' . $request->civil_id . '.png'));

                $inputStudentDetails['avatar'] = '/Images/ICSAStudents/' . $request->civil_id . '.png';
            }
        }

        //$icsastudent = Icsastudent::create($inputStudent);
        $icsastudent->update($inputStudent);
        $icsastudent->icsastudentdetails()->update($inputStudentDetails);
        //$icsastudentdetail = Icsastudentdetails::create($inputStudentDetails);

        return redirect()->route('icsastudents.edit', $icsastudent->id)
            ->with('success', 'Student Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsastudent $icsastudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsastudent $icsastudent)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getdetails(Request $request)
    {
        if ($request->ajax()) {
            $userId = $request->registration_id;
            $fromSearch = $request->fromSearch;
            $fromCivilId = $request->fromCivilId;
            $civilId = $request->civil_id;

            $employees = Icsaregistration::select('*')->where('registration_id', $userId)->get();

            $userdata = [];
            if (count($employees) > 0) {

                $studentDetail = Icsastudent::where('civil_id', $employees[0]->civil_id)->latest()->first();
                if ($studentDetail) {
                    if ($fromSearch == 'false') {
                        foreach ($employees as $emp) {
                            $userdata['fullname'] = $studentDetail->icsastudentdetails->fullname;
                            $userdata['email'] = $studentDetail->icsastudentdetails->email;
                            $userdata['mobile'] = $studentDetail->icsastudentdetails->mobile;
                            $userdata['birthdate'] = $studentDetail->icsastudentdetails->birthdate;
                            $userdata['civil_id'] = $studentDetail->civil_id;
                            $userdata['gender'] = $studentDetail->icsastudentdetails->gender;
                            $userdata['occupation'] = $emp->occupation;
                            $userdata['nationality'] = $emp->nationality;
                            $userdata['currently_residing'] = $emp->currently_residing;
                            $userdata['address'] = $emp->address;
                            $userdata['facebook'] = $emp->facebook;
                            $userdata['skype_isd_code'] = $studentDetail->icsastudentdetails->skype_isd_code;
                            $userdata['skype'] = $emp->skype;
                            $userdata['course_id'] = $emp->course_id;
                            $userdata['frequency'] = $emp->frequency;
                            $userdata['schedule'] = $emp->schedule;
                            $userdata['time'] = $emp->time;
                            $userdata['batch_id'] = $emp->batch;
                        }
                    }

                    $html = $this->getstudenthtml($studentDetail);

                    if ($fromSearch == 'false') {
                        return response()->json(['code' => 200, 'userdetail' => $userdata]);
                    } else {
                        return response()->json(['code' => 201, 'userdetail' => $studentDetail, 'details' => $html]);
                    }

                } else {
                    foreach ($employees as $emp) {
                        $userdata['fullname'] = $emp->fullname;
                        $userdata['email'] = $emp->email;
                        $userdata['isd_code'] = $emp->isd_code;
                        $userdata['mobile'] = $emp->mobile;
                        $userdata['birthdate'] = $emp->birthdate;
                        $userdata['civil_id'] = $emp->civil_id;
                        $userdata['gender'] = $emp->gender;
                        $userdata['occupation'] = $emp->occupation;
                        $userdata['nationality'] = $emp->nationality;
                        $userdata['currently_residing'] = $emp->currently_residing;
                        $userdata['address'] = $emp->address;
                        $userdata['facebook'] = $emp->facebook;
                        $userdata['skype_isd_code'] = $emp->skype_isd_code;
                        $userdata['skype'] = $emp->skype;
                        $userdata['course_id'] = $emp->course_id;
                        $userdata['frequency'] = $emp->frequency;
                        $userdata['schedule'] = $emp->schedule;
                        $userdata['time'] = $emp->time;
                        $userdata['batch_id'] = $emp->batch;

                        return response()->json(['code' => 200, 'userdetail' => $userdata]);
                    }
                }
            } else if ($fromCivilId == 'true') {
                $studentDetail = Icsastudent::where('civil_id', $request->civil_id)->latest()->first();
                if ($studentDetail) {
                    if ($fromSearch == 'false') {

                        $userdata['fullname'] = $studentDetail->icsastudentdetails->fullname;
                        $userdata['email'] = $studentDetail->icsastudentdetails->email;
                        $userdata['isd_code'] = $studentDetail->icsastudentdetails->isd_code;
                        $userdata['mobile'] = $studentDetail->icsastudentdetails->mobile;
                        $userdata['birthdate'] = $studentDetail->icsastudentdetails->birthdate;
                        $userdata['civil_id'] = $studentDetail->civil_id;
                        $userdata['gender'] = $studentDetail->icsastudentdetails->gender;
                        $userdata['occupation'] = $studentDetail->icsastudentdetails->occupation;
                        $userdata['nationality'] = $studentDetail->icsastudentdetails->nationality;
                        $userdata['currently_residing'] = $studentDetail->icsastudentdetails->currently_residing;
                        $userdata['address'] = $studentDetail->icsastudentdetails->address;
                        $userdata['facebook'] = $studentDetail->icsastudentdetails->facebook;
                        $userdata['skype'] = $studentDetail->icsastudentdetails->skype;
                        $userdata['skype_isd_code'] = $studentDetail->icsastudentdetails->skype_isd_code;
                        //$userdata['course_id'] = $studentDetail->course_id;
                        //$userdata['frequency'] = $studentDetail->frequency;
                        //$userdata['schedule'] = $studentDetail->schedule;
                        //$userdata['time'] = $studentDetail->time;
                        //$userdata['batch_id'] = $studentDetail->batch;

                    }
                    $html = $this->getstudenthtml($studentDetail);

                    if ($fromSearch == 'false') {
                        return response()->json(['code' => 200, 'userdetail' => $userdata]);
                    } else {
                        return response()->json(['code' => 201, 'userdetail' => $studentDetail, 'details' => $html]);
                    }

                }
                return response()->json(['code' => 400, 'userdetail' => $userdata]);
            }
            return response()->json(['code' => 400, 'userdetail' => $userdata]);

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editenrollment(Request $request)
    {
        $empty_option = array(
            '' => 'Select Option'
        );
        $instructor = Instructor::where('status', 'active')->pluck('name', 'id')->all();
        $instructor = $empty_option + $instructor;
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $icsabatch = Icsabatch::where('status', 'active')->pluck('name', 'id')->all();
        $icsabatch = $empty_option + $icsabatch;

        $source = Source::where('status', 'active')->pluck('name', 'id')->all();
        $source = $empty_option + $source;

        $nationality = Countrie::pluck('nicename', 'id')->all();
        $nationality = $empty_option + $nationality;

        $isd = Countrie::all();

        $icsastudent = [];

        return view('pages.icsastudent.edit', compact('courses', 'instructor', 'icsabatch', 'source', 'nationality', 'icsastudent', 'isd'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     *
     */
    public function getenrolldetails(Request $request)
    {
        $this->validate($request, [
            'enrollment_id' => 'required',
        ]);

        $checkEnroll = Icsastudent::where('enrollment_id', $request->get('enrollment_id'))->first();

        //if(auth()->user()->currentBranch )
        if ((substr($request->get('enrollment_id'), 0, 1) == auth()->user()->currentBranch) || auth()->user()->currentBranch == 3) {
            if (!is_null($checkEnroll)) {
                return redirect()->route('icsastudents.edit', $checkEnroll->id);
                //->with('success', 'Student created successfully');
            } else {
                return redirect()->route('icsastudents.editenrollment')
                    ->with('error', 'Student Information Not Found')
                    ->with('enroll', $request->get('enrollment_id'));
            }

        } else {
            return redirect()->route('icsastudents.editenrollment')
                ->with('error', 'Error: Searching of other branch accounts is not allowed, please use admin account.')
                ->with('enroll', $request->get('enrollment_id'));
        }
    }

    /**
     * @param $studentDetail
     * @return string
     */
    public function getstudenthtml($studentDetail)
    {
        $studentHtml = '
            <div class="row">
                <div class="col-sm-12">
                    <h4 style="background: #9B34B2; color: white;" class="text-center  p-1">Personal Details</h4>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 text-center">
                    <img height="220" width="220" src="' . $studentDetail->icsastudentdetails->avatar . '"
class="img-circle img-responsive" alt="Placeholder image">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Enrollment No:</strong>' . $studentDetail->enrollment_id . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Civil Id:</strong>' . $studentDetail->civil_id . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Birth Date:</strong>' . date('d M Y', strtotime($studentDetail->icsastudentdetails->birthdate)) . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Name:</strong>' . $studentDetail->icsastudentdetails->fullname . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <div class="form-group">
                                <strong>Course Name:</strong>' . $studentDetail->icsacourse->name . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Gender:</strong>' . $studentDetail->icsastudentdetails->gender . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Email:</strong>' . $studentDetail->icsastudentdetails->email . '
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Mobile:</strong>' . $studentDetail->icsastudentdetails->mobile . '
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        return $studentHtml;
    }
}
