<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Models\Icsabatch;
use App\Models\Icsacourse;
use Illuminate\Http\Request;

class IcsabatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icsabatchs = Icsabatch::orderBy('id', 'DESC')->get();

        return view('pages.icsabatch.index', compact('icsabatchs'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_option = array(
            ''=>'Select Option'
        );

        $icsacourses = Icsacourse::where('status', 'active')->orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $icsacourses = $empty_option + $icsacourses;
        $branches = Branches::where('status', 'active')->where('id', '!=', '3')->orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $branches = $empty_option + $branches;

        return view('pages.icsabatch.create', compact('icsacourses', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'course_id' => 'required',
            'branch_id' => 'required',
        ]);

        $icsacourse = Icsabatch::create($request->all());

        return redirect()->route('icsabatch.index')
            ->with('success', 'ICSA Batch created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsabatch  $icsabatch
     * @return \Illuminate\Http\Response
     */
    public function show(Icsabatch $icsabatch)
    {
        return view('pages.icsabatch.show', compact('icsabatch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsabatch  $icsabatch
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsabatch $icsabatch)
    {
        $empty_option = array(
            ''=>'Select Option'
        );

        $icsacourses = Icsacourse::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $icsacourses = $empty_option + $icsacourses;
        $branches = Branches::orderBy('id', 'ASC')->pluck('name', 'id')->all();
        $branches = $empty_option + $branches;

        return view('pages.icsabatch.edit', compact('icsacourses', 'branches', 'icsabatch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Icsabatch  $icsabatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsabatch $icsabatch)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $icsabatch->update($request->all());

        return redirect()->route('icsabatch.index')
            ->with('success', 'ICSA Batch Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsabatch  $icsabatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsabatch $icsabatch)
    {
        if ($icsabatch->status == 'active') {
            $icsabatch->status = 'disabled';
        } else {
            $icsabatch->status = 'active';
        }
        $icsabatch->update();
        return redirect()->route('icsabatch.index')
            ->with($icsabatch->status =='active' ? 'success' : 'error', 'ICSA Batch ' . ucfirst($icsabatch->status) . ' successfully');
    }
}
