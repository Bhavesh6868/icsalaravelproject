<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Icsastudent;
use App\Models\RoleBranch;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Spatie\Permission\Models\Permission;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.newlogin');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        /*$credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }*/

        $userdata = array('email' => $request->get('email'), 'password' => $request->get('password'));

        /*$this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);*/
        $validator = Validator::make($userdata, [
            'email' => 'required|max:255|email',
            'password' => 'required|confirmed',
        ]);

        if (Auth::attempt($userdata)) {
            if (auth()->user()->status != 'active') {
                $errors = new MessageBag(['email' => ['Email and/or password invalid.']]);
                auth()->logout();
                return Redirect::back()->withErrors($errors)->withInput();
            } else {
                return redirect()->intended($this->redirectTo());
            }
        }

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function redirectTo()
    {

        if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            $user->currentRole = Auth::user()->roles()->first()->id;

            $branches = RoleBranch::where('role_id', $user->currentRole)->orderBy('branch_id', 'Asc')->get();

            if(is_countable($branches)) {
                $user->currentBranch = $branches->first()->branch_id;
            }
            $user->update();

            $rolePermissions = [];
            foreach (Auth::user()->roles->pluck('id') as $roleId) {
                if (Auth::user()->roles()->first()->id == $roleId) {
                    $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
                        ->where("role_has_permissions.role_id", $roleId)
                        ->get();
                }
            }

            $permissionArray = [];
            foreach ($rolePermissions as $permissions) {
                $permissionArray[] = ($permissions->name);
            }
            Session::put('permission_array', $permissionArray);
        }

        if (Auth::check() && Auth::user()->roles->contains('name', 'Admin')) {
            return '/dashboard';

        } else if (Auth::check() && Auth::user()->roles->contains('name', 'Manager')) {
            return '/dashboard';
        }

        // Check user role
        /*switch ($role) {
            case 'Manager':
                return '/dashboard';
                break;
            case 'Admin':
                return '/manager';
                break;
            default:
                return '/login';
                break;
        }*/
    }

    public function impersonate($user_id)
    {
        if ($user_id != ' ') {
            $user = User::find($user_id);
            Auth::user()->impersonate($user);
            return redirect('/dashboard');
        }
        return redirect()->back();
    }

    public function impersonate_leave()
    {
        Auth::user()->leaveImpersonation();
        return redirect('/dashboard');
    }
}
