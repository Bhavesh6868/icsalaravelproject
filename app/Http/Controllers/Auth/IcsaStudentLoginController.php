<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class IcsaStudentLoginController extends Controller
{
    protected $guard = 'student';

    public function __construct()
    {
        $this->middleware('guest:student')->except('logout');
    }
    public function showLoginForm()
    {
        return view('auth.login.employee');
    }
    public function username()
    {
        return 'employee_id';
    }
   /* protected function guard()
    {
        return Auth::guard('student');
    }*/

    public function login(Request $request) {
        $userdata = array('enrollment_id' => $request->get('enrollment'), 'password' => $request->get('mobile'));

        /*$this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);*/
        $validator = Validator::make($userdata, [
            'enrollment_id' => 'required|max:255',
            'password' => 'required|confirmed',
        ]);
/*
        var_dump($userdata);
        echo Auth::guard('student')->attempt($userdata);
        die;*/
        if (Auth::guard('student')->attempt($userdata)) {
                return redirect()->intended($this->redirectTo());
        } else {
            $errors = new MessageBag(['enrollment' => ['Enrollment and/or password invalid.']]);
            auth()->logout();
            return Redirect::back()->withErrors($errors)->withInput();
        }
    }

    public function redirectTo()
    {
        return '/icsastudent';
    }
}
