<?php

namespace App\Http\Controllers;

use App\Models\Icsapaymentdetail;
use App\Models\Icsapaymentrefund;
use App\Models\Icsastudent;
use App\Models\Paymentmethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IcsapaymentrefundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function refundpayment(Request $request)
    {
        $icsastudent = [];
        return view('pages.icsapayment.refundpayment', compact('icsastudent'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required',
            'amount' => 'required',
        ]);

        if ($request->get('amount') <> 0) {
            $refunddata['enrollment_id'] = $request->get('id');
            $refunddata['amount'] = $request->get('amount');
            $refunddata['reason'] = $request->get('reason');
            $refunddata['pay_branch'] = auth()->user()->currentBranch;
            $refunddata['process_by'] = auth()->user()->username;
            Icsapaymentrefund::create($refunddata);

        }

        return redirect()->route('icsapayment.icsarefundpayment', ['id' => $request->get('id')])
        ->with('success', 'Payment Refunde Successfully')
        ->with('enroll', $request->get('enrollment_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsapaymentrefund  $icsapaymentrefund
     * @return \Illuminate\Http\Response
     */
    public function show(Icsapaymentrefund $icsapaymentrefund)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsapaymentrefund  $icsapaymentrefund
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsapaymentrefund $icsapaymentrefund)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Icsapaymentrefund  $icsapaymentrefund
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsapaymentrefund $icsapaymentrefund)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsapaymentrefund  $icsapaymentrefund
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsapaymentrefund $icsapaymentrefund)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsapaymentdetail $icsapaymentdetail
     * @return \Illuminate\Http\Response
     */
    public function icsarefundpayment(Request $request, $id)
    {
        $icsastudent = Icsastudent::find($id);
        /*echo $id;
        die;*/
        /*$empty_option = array(
            '' => 'Select Option'
        );*/
        //$paymentmethod = $empty_option + Paymentmethod::where('status', 'active')->pluck('name', 'id')->all();

        $paidAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['NEW STUDENT', 'BALANCE PAYMENT'])->value(DB::raw("SUM(amount)"));

        $discountAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['DISCOUNT'])->value(DB::raw("SUM(amount)"));

        $additionalAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['ADDITIONAL'])->value(DB::raw("SUM(amount)"));

        $refundAmtSum = Icsapaymentrefund::where('enrollment_id', $id)->value(DB::raw("SUM(amount)"));
        $refundAmt = Icsapaymentrefund::where('enrollment_id', $id)->get();

        return view('pages.icsapayment.refundpayment', compact('icsastudent', 'refundAmt', 'refundAmtSum','additionalAmt', 'paidAmt', 'discountAmt'));
    }
}
