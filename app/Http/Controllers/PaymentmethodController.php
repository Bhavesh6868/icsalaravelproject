<?php

namespace App\Http\Controllers;

use App\Models\Paymentmethod;
use Illuminate\Http\Request;

class PaymentmethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentmethod = Paymentmethod::orderBy('id', 'DESC')->get();
        return view('pages.paymentmethod.index', compact('paymentmethod'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.paymentmethod.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $icsacourse = Paymentmethod::create($request->all());

        return redirect()->route('paymentmethod.index')
            ->with('success', 'Payment method created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Paymentmethod  $paymentmethod
     * @return \Illuminate\Http\Response
     */
    public function show(Paymentmethod $paymentmethod)
    {
        return view('pages.paymentmethod.show', compact('paymentmethod'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Paymentmethod  $paymentmethod
     * @return \Illuminate\Http\Response
     */
    public function edit(Paymentmethod $paymentmethod)
    {
        return view('pages.paymentmethod.edit', compact('paymentmethod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Paymentmethod  $paymentmethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paymentmethod $paymentmethod)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $paymentmethod->update($request->all());

        return redirect()->route('paymentmethod.index')
            ->with('success', 'Payment Method Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Paymentmethod  $paymentmethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paymentmethod $paymentmethod)
    {
        if ($paymentmethod->status == 'active') {
            $paymentmethod->status = 'disabled';
        } else {
            $paymentmethod->status = 'active';
        }
        $paymentmethod->update();
        return redirect()->route('paymentmethod.index')
            ->with($paymentmethod->status =='active' ? 'success' : 'error', 'ICSA Course  ' . ucfirst($paymentmethod->status) . ' successfully');
    }
}
