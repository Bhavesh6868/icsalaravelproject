<?php

namespace App\Http\Controllers;

use App\Jobs\icsaRemittanceJob;
use App\Models\Branches;
use App\Models\Icsacourse;
use App\Models\Icsapaymentdetail;
use App\Models\Icsapaymentrecept;
use App\Models\Icsapaymentrefund;
use App\Models\Icsastudent;
use App\Models\Paymentmethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Mail;

class IcsapaymentdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');
        $course_id = '';
        $paymentod_id = '';

        /*$icsastudents = Icsastudent::orderBy('id', 'DESC')->get();//->paginate(5);*/
        $icsastudents = Icsapaymentdetail::query();

        $icsastudents->when(auth()->user()->currentBranch != '3', function ($q) {
            return $q->whereHas('icsastudent', function($q)
            {
                $q->where('enrollment_id', 'Like', auth()->user()->currentBranch.'%');

            });
        });
        /*$icsastudents->when(request('filter_by') == 'date', function ($q) {
            return $q->orderBy('id', 'desc');
        });*/

        $icsastudents = $icsastudents->whereBetween('date_paid', array($start_date, $end_date));
        $icsastudents = $icsastudents->orderBy('id', 'DESC')->get();


        $empty_option = array(
            '' => 'Select Course Option'
        );
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $empty_option = array(
            '' => 'Select Payment Option'
        );
        $paymentod = Paymentmethod::where('status', 'active')->pluck('name', 'id')->all();
        $paymentod = $empty_option + $paymentod;

        return view('pages.reports.icsapaymentreport', compact('icsastudents', 'start_date', 'end_date', 'courses', 'course_id', 'paymentod', 'paymentod_id'))->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function searchenroll(Request $request) {
        if((isset($request->start_date) && isset($request->end_date)) || isset($request->course_id) || isset($request->paymentod_id)) {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $course_id = $request->course_id;
            $paymentod_id = $request->paymentod_id;
        } else {
            $start_date = date('Y-m-01');
            $end_date = date('Y-m-t');
            $course_id = '';
            $paymentod_id = '';
        }

        /*$icsastudents = Icsastudent::orderBy('id', 'DESC')->get();//->paginate(5);*/
        $icsastudents = Icsapaymentdetail::query();

        $icsastudents->when($course_id != '', function($q) use ($course_id) {
            return $q->whereHas('icsastudent', function($q) use ($course_id){
                $q->where('course_id', '=', $course_id);
            });
        });

        $icsastudents->when(auth()->user()->currentBranch != '3', function ($q) {
            return $q->whereHas('icsastudent', function($q)
            {
                $q->where('enrollment_id', 'Like', auth()->user()->currentBranch.'%');

            });
        });

        $icsastudents->when($paymentod_id != '', function($q) use ($paymentod_id) {
            return $q->where('payment_bank', '=', $paymentod_id);
        });

        /*$icsastudents->when(request('filter_by') == 'date', function ($q) {
            return $q->orderBy('id', 'desc');
        });*/

        $icsastudents = $icsastudents->whereBetween('date_paid', array($start_date, $end_date));
        $icsastudents = $icsastudents->orderBy('id', 'DESC')->get();

        $empty_option = array(
            '' => 'Select Course Option'
        );
        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;


        $empty_option = array(
            '' => 'Select Payment Option'
        );
        $paymentod = Paymentmethod::where('status', 'active')->pluck('name', 'id')->all();
        $paymentod = $empty_option + $paymentod;

        return view('pages.reports.icsapaymentreport', compact('icsastudents', 'start_date', 'end_date', 'courses', 'course_id', 'paymentod', 'paymentod_id'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function addpayment(Request $request)
    {
        $icsastudent = [];
        return view('pages.icsapayment.addpayment', compact('icsastudent'));
    }

    public function getenrolldetails(Request $request)
    {

        $this->validate($request, [
            'enrollment_id' => 'required',
        ]);

        $checkEnroll = Icsastudent::where('enrollment_id', $request->get('enrollment_id'))->first();

        //if(auth()->user()->currentBranch )
        if ((substr($request->get('enrollment_id'), 0, 1) == auth()->user()->currentBranch) || auth()->user()->currentBranch == 3) {
            if (!is_null($checkEnroll)) {
                if ($request->fromtype == 'printinvoice') {
                    return redirect()->route('icsapayment.printinvoice', $checkEnroll->id);
                } else if ($request->fromtype == 'addpayment') {
                    return redirect()->route('icsapayment.icsaaddpayment', $checkEnroll->id);
                } else if ($request->fromtype == 'refundpayment') {
                    return redirect()->route('icsapayment.icsarefundpayment', $checkEnroll->id);
                }
                //->with('success', 'Student created successfully');
            } else {
                if ($request->fromtype == 'printinvoice') {
                    return redirect()->route('icsapayment.showinvoice')
                        ->with('error', 'Student Information Not Found')
                        ->with('enroll', $request->get('enrollment_id'));
                } else if ($request->fromtype == 'addpayment') {
                    return redirect()->route('icsapayment.addpayment')
                        ->with('error', 'Student Information Not Found')
                        ->with('enroll', $request->get('enrollment_id'));
                } else if ($request->fromtype == 'refundpayment') {
                    return redirect()->route('icsapayment.refundpayment')
                        ->with('error', 'Student Information Not Found')
                        ->with('enroll', $request->get('enrollment_id'));
                }
            }
        } else {
            if ($request->fromtype == 'printinvoice') {
                return redirect()->route('icsapayment.showinvoice')
                    ->with('error', 'Error: Searching of other branch accounts is not allowed, please use admin account.')
                    ->with('enroll', $request->get('enrollment_id'));
            } else if ($request->fromtype == 'addpayment') {
                return redirect()->route('icsapayment.addpayment')
                    ->with('error', 'Error: Searching of other branch accounts is not allowed, please use admin account.')
                    ->with('enroll', $request->get('enrollment_id'));
            } else if ($request->fromtype == 'refundpayment') {
                return redirect()->route('icsapayment.refundpayment')
                    ->with('error', 'Error: Searching of other branch accounts is not allowed, please use admin account.')
                    ->with('enroll', $request->get('enrollment_id'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paymentmethod = Paymentmethod::where('status', 'active')->get();
        return view('pages.icsapayment.create', compact('paymentmethod'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->get('paymentmethod') == 1 || $request->get('paymentmethod') == 2) {
            $this->validate($request, [
                'paymentmethod' => 'required',
                'amount' => 'required',
            ]);
        } else if ($request->get('paymentmethod') == 3) {
            $this->validate($request, [
                'paymentmethod' => 'required',
                'account_no' => 'required',
                'amount' => 'required',
            ]);
        } else {
            $this->validate($request, [
                'paymentmethod' => 'required',
                'amount' => 'required',
                'philippine_peso' => 'required',
                'transaction_no' => 'required',
                'transaction_date' => 'required',
            ]);
        }

        if ($request->get('paymentmethod') == 3) {
            $icsastudent = Icsastudent::where('enrollment_id', $request->get('account_no'))->first();
            if (is_null($icsastudent)) {
                $errors = new MessageBag(['email' => ['Transfer from account nummber not found.']]);
                return Redirect::back()->withErrors($errors)->withInput();
            } else {
                /*$fees = $icsastudent->icsacourse->course_fee;*/

                $paidAmt = Icsapaymentdetail::where('enrollment_id', $icsastudent->id)->whereIn('pay_detail', ['NEW STUDENT', 'BALANCE PAYMENT'])->value(DB::raw("SUM(amount)"));

               /* $discountAmt = Icsapaymentdetail::where('enrollment_id', $icsastudent->id)->whereIn('pay_detail', ['DISCOUNT'])->value(DB::raw("SUM(amount)"));*/

                $additionalAmt = Icsapaymentdetail::where('enrollment_id', $icsastudent->id)->whereIn('pay_detail', ['ADDITIONAL'])->value(DB::raw("SUM(amount)"));

                $refundAmtSum = Icsapaymentrefund::where('enrollment_id', $icsastudent->id)->value(DB::raw("SUM(amount)"));

                $balance = ($paidAmt+$additionalAmt)-$refundAmtSum;

                if($balance < $request->get('amount') && $request->get('amount') <> 0) {
                    $errors = new MessageBag(['email' => ['Can not possible because paid amount of '.$request->get('account_no').' is less then current student requested amount.']]);
                    return Redirect::back()->withErrors($errors)->withInput();
                } else if ($request->get('amount') <> 0) {
                    $reason = 'Amount Refund and transfer to account no: ' . $request->get('enrollment_id');
                    $refunddata['enrollment_id'] = $icsastudent->id;
                    $refunddata['amount'] = $request->get('amount');
                    $refunddata['reason'] = $reason;
                    $refunddata['pay_branch'] = auth()->user()->currentBranch;
                    $refunddata['process_by'] = auth()->user()->username;
                    Icsapaymentrefund::create($refunddata);

                }
            }
        }

        $reason = ($request->get('account_no')) ? 'Transfer form account no: ' . $request->get('account_no') : $request->get('transfer_note');

        //get enrollment details
        $icsastudentdetails = Icsastudent::where('id', $request->get('id'))->first();
        $paidamount = 0;
        if (!is_null($icsastudentdetails)) {
            $paidfees = Icsapaymentdetail::where('enrollment_id', $request->get('id'))->get();
            if (is_countable($paidfees) && count($paidfees) > 0) {
                foreach ($paidfees as $key => $val) {
                    $paidamount = $paidamount + ($val['amount']);
                }
            }
            /*$icsacoursedetails = Icsacourse::where('id', $icsastudentdetails->course_id)->first();
            $courseFee = $icsacoursedetails->course_fee;*/
        }

        $method = '';
        if ($paidamount == 0) {
            $method = 'NEW STUDENT';
        } elseif ($paidamount > 0) {
            $method = 'BALANCE PAYMENT';
        }

        if (($request->get('paymentmethod') == 1 || $request->get('paymentmethod') == 2 || $request->get('paymentmethod') == 3) && $request->get('amount') <> 0) {
            $insertpayment['enrollment_id'] = $request->get('id');
            $insertpayment['amount'] = $request->get('amount');
            $insertpayment['pay_branch'] = auth()->user()->currentBranch;
            $insertpayment['process_by'] = auth()->user()->username;
            $insertpayment['transfer_note'] = ($request->get('discount') <> 0 || $request->get('additional') <> 0) ? null : $reason;;
            $insertpayment['pay_detail'] = $method;
            $insertpayment['payment_bank'] = $request->get('paymentmethod');
            $payment = Icsapaymentdetail::create($insertpayment);
        } else if ($request->get('amount') <> 0) {
            $insertpayment['enrollment_id'] = $request->get('id');
            $insertpayment['amount'] = $request->get('amount');
            $insertpayment['philippine_peso'] = $request->get('philippine_peso');
            $insertpayment['transaction_no'] = $request->get('transaction_no');
            $insertpayment['transaction_date'] = $request->get('transaction_date');
            $insertpayment['transfer_note'] = ($request->get('discount') <> 0 || $request->get('additional') <> 0) ? null : $reason;
            $insertpayment['pay_detail'] = $method;
            $insertpayment['payment_bank'] = $request->get('paymentmethod');
            $insertpayment['pay_branch'] = auth()->user()->currentBranch;
            $insertpayment['process_by'] = auth()->user()->username;
            $payment = Icsapaymentdetail::create($insertpayment);
        }
        if (isset($request->discount) && $request->get('discount') <> 0) {
            $method = 'DISCOUNT';
            $insertpayment['enrollment_id'] = $request->get('id');
            $insertpayment['amount'] = $request->get('discount');
            $insertpayment['pay_branch'] = auth()->user()->currentBranch;
            $insertpayment['process_by'] = auth()->user()->username;
            $insertpayment['transfer_note'] = $reason;
            $insertpayment['pay_detail'] = $method;
            $insertpayment['payment_bank'] = $request->get('paymentmethod');
            Icsapaymentdetail::create($insertpayment);
        }
        if (isset($request->additional) && $request->get('additional') <> 0) {
            $method = 'ADDITIONAL';
            $insertpayment['enrollment_id'] = $request->get('id');
            $insertpayment['amount'] = $request->get('additional');
            $insertpayment['pay_branch'] = auth()->user()->currentBranch;
            $insertpayment['process_by'] = auth()->user()->username;
            $insertpayment['transfer_note'] = $reason;
            $insertpayment['pay_detail'] = $method;
            $insertpayment['payment_bank'] = $request->get('paymentmethod');
            Icsapaymentdetail::create($insertpayment);
        }

        if (($request->get('paymentmethod') == 1 || $request->get('paymentmethod') == 2 || $request->get('paymentmethod') == 3) && $request->get('amount') <> 0) {
            $file = $request->myimage;
        } else {
            $file = $request->myimage1;
        }
        if ($file) {
            //if (file_exists(public_path() . '/Images/crop_image/' . $request->username . '.png')) {
            if (!is_dir(public_path('/Images/icsaremittance'))) {
                mkdir(public_path('/Images/icsaremittance/'));
                chmod(public_path('/Images/icsaremittance'), 0777);
            }

            $fileName = $request->get('enrollment_id') . '-' . time() . '.' . $file->extension();

            $file->move(public_path('/Images/icsaremittance'), $fileName);

            $insertpaymentrecept['enrollment_id'] = $request->get('id');
            $insertpaymentrecept['payment_id'] = $payment->id;
            $insertpaymentrecept['amount'] = 0;
            $insertpaymentrecept['image'] = $fileName;

            Icsapaymentrecept::create($insertpaymentrecept);
            $details['enrollment_id'] = $request->get('id');
            $details['images'] = $fileName;
            $details['currentBranch'] = auth()->user()->currentBranch;
            dispatch(new icsaRemittanceJob($details));
        }
        /*return redirect()->route('icsapayment.addpayment')
            ->with('error', 'Payment Created')
            ->with('enroll', $request->get('enrollment_id'));*/

        return redirect()->route('icsapayment.printinvoice', ['id' => $request->get('id')]);
        /*->with('error', 'Payment Created')
        ->with('enroll', $request->get('enrollment_id'));*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsapaymentdetail $icsapaymentdetail
     * @return \Illuminate\Http\Response
     */
    public function show(Icsapaymentdetail $icsapaymentdetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsapaymentdetail $icsapaymentdetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsapaymentdetail $icsapaymentdetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Icsapaymentdetail $icsapaymentdetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsapaymentdetail $icsapaymentdetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsapaymentdetail $icsapaymentdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsapaymentdetail $icsapaymentdetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsapaymentdetail $icsapaymentdetail
     * @return \Illuminate\Http\Response
     */
    public function icsaaddpayment(Request $request, $id)
    {
        $icsastudent = Icsastudent::find($id);
        /*echo $id;
        die;*/
        $empty_option = array(
            '' => 'Select Option'
        );
        $paymentmethod = $empty_option + Paymentmethod::where('status', 'active')->pluck('name', 'id')->all();

        $paidAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['NEW STUDENT', 'BALANCE PAYMENT'])->value(DB::raw("SUM(amount)"));

        $discountAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['DISCOUNT'])->value(DB::raw("SUM(amount)"));

        $additionalAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['ADDITIONAL'])->value(DB::raw("SUM(amount)"));

        $refundAmtSum = Icsapaymentrefund::where('enrollment_id', $id)->value(DB::raw("SUM(amount)"));
        $refundAmt = Icsapaymentrefund::where('enrollment_id', $id)->get();

        return view('pages.icsapayment.addpayment', compact('icsastudent', 'paymentmethod', 'paidAmt', 'discountAmt', 'additionalAmt', 'refundAmtSum', 'refundAmt'));
    }

    public function showinvoice(Request $request)
    {
        $icsastudent = [];
        return view('pages.icsapayment.printinvoice', compact('icsastudent'));
    }

    public function printinvoice(Request $request, $id)
    {
        $icsastudent = Icsastudent::find($id);
        /*echo $id;
        die;*/
        $empty_option = array(
            '' => 'Select Option'
        );
        $paymentmethod = $empty_option + Paymentmethod::where('status', 'active')->pluck('name', 'id')->all();

        $paidAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['NEW STUDENT', 'BALANCE PAYMENT'])->value(DB::raw("SUM(amount)"));

        $discountAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['DISCOUNT'])->value(DB::raw("SUM(amount)"));

        $additionalAmt = Icsapaymentdetail::where('enrollment_id', $id)->whereIn('pay_detail', ['ADDITIONAL'])->value(DB::raw("SUM(amount)"));

        $refundAmtSum = Icsapaymentrefund::where('enrollment_id', $id)->value(DB::raw("SUM(amount)"));
        $refundAmt = Icsapaymentrefund::where('enrollment_id', $id)->get();


        return view('pages.icsapayment.printinvoice', compact('icsastudent', 'paymentmethod', 'paidAmt', 'discountAmt', 'additionalAmt', 'refundAmtSum', 'refundAmt'));
    }
}
