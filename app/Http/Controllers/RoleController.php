<?php

namespace App\Http\Controllers;


use App\Models\Branches;
use App\Models\RoleBranch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id','DESC')->get();//->paginate(5);
        return view('pages.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get();
        $permiarry = [];
        foreach ($permission as $key => $val) {
            $permi = explode("-", $val['name']);
            if(!in_array($permi[0], $permiarry)) {
                $getPermission = Permission::where('name', 'like', '' .$permi[0].'%')->get();
                $permiarry[$permi[0]][] = $getPermission;
            } else if(in_array($permi[0], $permiarry)) {
                $getPermission = Permission::where('name', 'like', '' .$permi[0].'%')->get();
                $permiarry[$permi[0]][] = $getPermission;
            }
        }
        $branches = Branches::pluck('name', 'id')->all();

        return view('pages.roles.create',compact('permiarry', 'branches'))->with('i');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
            //'branch' => 'required',
        ]);

        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));

        /**
         * Added for branch role wise
         */
        if (is_countable($request->input('branch')) && count($request->input('branch')) > 0) {
            //DB::table('user_branch')->where('user_id', $id)->delete();
            foreach ($request->input('branch') as $branch) {
                $rolebranch = new RoleBranch();
                $rolebranch->role_id = $role->id;
                $rolebranch->branch_id = $branch;
                $rolebranch->save();
            }
        }
        /***
         * End added for branch role wise
         */

        return redirect()->route('roles.index')
            ->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();

        $branches = RoleBranch::where('role_id', $id)->get();
        return view('pages.roles.show',compact('role','rolePermissions', 'branches'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $permiarry = [];
        foreach ($permission as $key => $val) {
            $permi = explode("-", $val['name']);
            if(!in_array($permi[0], $permiarry)) {
                $getPermission = Permission::where('name', 'like', '' .$permi[0].'%')->get();
                $permiarry[$permi[0]][] = $getPermission;
            } else if(in_array($permi[0], $permiarry)) {
                $getPermission = Permission::where('name', 'like', '' .$permi[0].'%')->get();
                $permiarry[$permi[0]][] = $getPermission;
            }
        }
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        /**
         * Added branch role wise
         */
        $branches = Branches::pluck('name', 'id')->all();

        $roleBranch = DB::table('branches AS pr')
            ->select(DB::raw('pr.name, pr.id'))
            ->join('role_branches AS pu', 'pu.branch_id', '=', 'pr.id')
            ->join('roles AS r', 'r.id', '=', 'pu.role_id')
            ->where('r.id', '=', $id)
            ->pluck('id')->all();

        /**
         * End Added branch role wise
         */
        return view('pages.roles.edit',compact('role','permiarry','rolePermissions', 'branches', 'roleBranch'))->with('i');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
            //'branch' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        /**
         * Added for branch role wise
        */
        if (is_countable($request->input('branch')) && count($request->input('branch')) > 0) {
            DB::table('role_branches')->where('role_id', $id)->delete();
            foreach ($request->input('branch') as $branch) {
                /*var_dump($branch);
                die;*/
                $rolebranch = new RoleBranch();
                $rolebranch->role_id = $id;
                $rolebranch->branch_id = $branch;
                $rolebranch->save();
            }
        }
        /**
         * End Added for branch role wise
         */

        return redirect()->route('roles.index')
            ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
            ->with('success','Role deleted successfully');*/
        $role = Role::find($id);
        if ($role->status == 'active') {
            $role->status = 'disabled';
        } else {
            $role->status = 'active';
        }
        $role->update();
        //DB::table("branches")->where('id',$id)->delete();
        return redirect()->route('roles.index')
            ->with($role->status =='active' ? 'success' : 'error', 'Role ' . ucfirst($role->status) . ' successfully');
    }
}