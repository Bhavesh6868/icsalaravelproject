<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BranchController extends Controller
{
    /***
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request) {
        $branches = Branches::orderBy('id','DESC')->get();//->paginate(5);
        return view('pages.branch.index',compact('branches'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /***
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('pages.branch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:branches,name',
        ]);

        $branch = Branches::create(['name' => $request->input('name')]);

        return redirect()->route('branch.index')
            ->with('success','Branch created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branch = Branches::find($id);

        return view('pages.branch.show',compact('branch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branches::find($id);
        return view('pages.branch.edit',compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $role = Branches::find($id);
        $role->name = $request->input('name');
        $role->save();

        return redirect()->route('branch.index')
            ->with('success','Branch updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branches::find($id);
        if ($branch->status == 'active') {
            $branch->status = 'disabled';
        } else {
            $branch->status = 'active';
        }
        $branch->update();
        //DB::table("branches")->where('id',$id)->delete();
        return redirect()->route('branch.index')
            ->with($branch->status =='active' ? 'success' : 'error', 'Branch ' . ucfirst($branch->status) . ' successfully');
    }
}
