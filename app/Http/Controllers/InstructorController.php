<?php

namespace App\Http\Controllers;

use App\Models\Icsacourse;
use App\Models\Instructor;
use App\Models\Instructorcourse;
use App\Models\User;
use Illuminate\Http\Request;
use Maher\Counters\Facades\Counters;
use Maher\Counters\Models\Counter;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructor = Instructor::orderBy('id', 'DESC')->get();//->paginate(5);
        return view('pages.instructor.index', compact('instructor'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::doesntHave('instructor')->pluck('name', 'id')->toArray();//->paginate(5);
        $empty_option = array(
            ''=>'Select Option'
        );
        $users = $empty_option + $users;

        $course = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $course = $empty_option + $course;
        //$course = array_merge(['0' => 'Select Courses'], $course);
        return view('pages.instructor.create', compact('users', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user' => 'required|not_in:0',
            'course' => 'required',
            'basic_salary' => 'required',
        ]);
        $user = User::with('instructor')->find($request->user);

        if ($user) {
            if ($user->instructor()->exists()) {
                $ins['name'] = $user->name;
                $ins['speaking_name'] = $user->username;

                if (is_countable($request->input('course')) && count($request->input('course')) > 0) {
                    //DB::table('user_branch')->where('user_id', $id)->delete();
                    foreach ($request->input('course') as $course) {
                        $instructorcourse = new Instructorcourse();
                        $instructorcourse->instructor_id = $user->instructor()->instructor_id;
                        $instructorcourse->course_id = $course;
                        $instructorcourse->save();
                    }
                }

                //$ins['user_id'] = $user->civil_id;
                $user->instructor()->update($ins);

            } else {
                $checkCounter = \DB::select(\DB::raw("select * from counters where `key` = 'INST'"));
                if (count($checkCounter) > 0) {
                    $count = Counters::increment('INST');
                    $counter = Counters::getValue('INST');
                } else {
                    Counter::create([
                        'key' => 'INST',
                        'name' => 'instructor_counter',
                        'initial_value' => 0,
                        'step' => 1
                    ]);
                    $count = Counters::increment('INST');
                    $counter = Counters::getValue('INST');
                }
                $instructor = new Instructor();
                $instructor->instructor_id = 'INST' . sprintf("%04d", $counter);;
                $instructor->name = $user->name;
                $instructor->speaking_name = $user->username;
                //$instructor->user_id = $user->id;
                $user->instructor()->save($instructor);

                if (is_countable($request->input('course')) && count($request->input('course')) > 0) {
                    //DB::table('user_branch')->where('user_id', $id)->delete();
                    foreach ($request->input('course') as $course) {
                        $instructorcourse = new Instructorcourse();
                        $instructorcourse->instructor_id = $instructor->id;
                        $instructorcourse->course_id = $course;
                        $instructorcourse->save();
                    }
                }
            }
        }
        return redirect()->route('instructor.index')
            ->with('success', 'Instructor created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Instructor $instructor
     * @return \Illuminate\Http\Response
     */
    public function show(Instructor $instructor)
    {
        return view('pages.instructor.show', compact('instructor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Instructor $instructor
     * @return \Illuminate\Http\Response
     */
    public function edit(Instructor $instructor)
    {
        $user = User::pluck('name', 'id')->all();//->paginate(5);
        $empty_option = array(
            ''=>'Select Option'
        );
        //$users = $empty_option + $users;

        $course = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $course = $empty_option + $course;

        $oldCourse = $instructor->instructorcourse->pluck('course_id')->all();
        return view('pages.instructor.edit', compact('instructor', 'user', 'course', 'oldCourse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Instructor $instructor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instructor $instructor)
    {
        $this->validate($request, [
            //'user' => 'required|not_in:0',
            'course' => 'required',
            'basic_salary' => 'required',
        ]);

        if($instructor->update($request->all())) {
            if (is_countable($request->input('course')) && count($request->input('course')) > 0) {
                Instructorcourse::where('instructor_id', $instructor->id)->delete();
                foreach ($request->input('course') as $branch) {
                    /*var_dump($branch);
                    die;*/
                    $userbranch = new Instructorcourse();
                    $userbranch->instructor_id = $instructor->id;
                    $userbranch->course_id = $branch;
                    $userbranch->save();
                }
            }
        }

        return redirect()->route('instructor.index')
            ->with('success', 'Instructor Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Instructor $instructor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instructor $instructor)
    {
        if ($instructor->status == 'active') {
            $instructor->status = 'disabled';
        } else {
            $instructor->status = 'active';
        }
        $instructor->update();
        return redirect()->route('instructor.index')
            ->with($instructor->status =='active' ? 'success' : 'error', 'Instructor ' . ucfirst($instructor->status) . ' successfully');
    }
}
