<?php

namespace App\Http\Controllers;

use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Models\Icsacourse;
use Illuminate\Http\Request;

/**
 * Class For Email Template & Template Content
 * Created By : Bhavesh Darji
 * Date : 07-11-2021
 *
*/
class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:emailtemplate-list|emailtemplate-create|emailtemplate-edit|emailtemplate-delete', ['only' => ['index','store']]);
        $this->middleware('permission:emailtemplate-create', ['only' => ['create','store']]);
        $this->middleware('permission:emailtemplate-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:emailtemplate-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailtemplates = EmailTemplate::orderBy('id', 'DESC')->get();

        return view('pages.emailtemplate.index', compact('emailtemplates'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_option = array(
            ''=>'Select Option'
        );

        return view('pages.emailtemplate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $icsacourse = EmailTemplate::create($request->all());

        return redirect()->route('emailtemplate.index')
            ->with('success', 'Email Template created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplate $emailTemplate, $id)
    {
        $emailTemplate = EmailTemplate::find($id);
        return view('pages.emailtemplate.show', compact('emailTemplate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailTemplate $emailTemplate, $id)
    {
        $emailTemplate = EmailTemplate::find($id);
        return view('pages.emailtemplate.edit', compact('emailTemplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailTemplate $emailTemplate, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $emailTemplate = EmailTemplate::find($id);
        $emailTemplate->update($request->all());

        return redirect()->route('emailtemplate.index')
            ->with('success', 'Email Template Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplate $emailTemplate, $id)
    {
        $emailTemplate = EmailTemplate::find($id);
        if ($emailTemplate->status == 'active') {
            $emailTemplate->status = 'disabled';
        } else {
            $emailTemplate->status = 'active';
        }
        $emailTemplate->update();
        return redirect()->route('emailtemplate.index')
            ->with($emailTemplate->status =='active' ? 'success' : 'error', 'Email Template ' . ucfirst($emailTemplate->status) . ' successfully');
    }

    /**
     * Code For Template Content
     * Created By Bhavesh Darji
     *
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contentlist(Request $request, $id)
    {
        $emailtemplates = EmailTemplateContent::where('template_id', $id)->orderBy('id', 'DESC')->get();

        return view('pages.emailtemplate.indexcontent', compact('emailtemplates', 'id'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contentcreate(Request $request, $id)
    {

        $empty_option = array(
            ''=>'Select Option'
        );

        $course = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $course = $empty_option + $course;
        return view('pages.emailtemplate.createcontent', compact('course', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contentstore(Request $request, $id)
    {
        //$request->template_id = $id;
        $request->request->add(['template_id' => $id]);
        $this->validate($request, [
            //'template_id' => 'required',
            'course_id' => 'required',
            'content' => 'required',
        ]);
        //var_dump($request->all());

        $emailTemplateContent = EmailTemplateContent::create($request->all());

        foreach ($request->input('document', []) as $file) {
                $emailTemplateContent->addMedia(public_path('/Images/EmailContent/' . $file))->preservingOriginal()->toMediaCollection('document');
        }

        return redirect()->route('emailtemplate.attach.content', $id)
            ->with('success', 'Email Template Content created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function contentshow(EmailTemplateContent $emailTemplateContent, $template_id, $id)
    {
        $emailTemplateContent = EmailTemplateContent::find($template_id);
        $emailTemplateContent->document = $emailTemplateContent->getMedia('document');
        return view('pages.emailtemplate.showcontent', compact('emailTemplateContent', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function contentedit(EmailTemplateContent $emailTemplateContent, $id, $template_id)
    {
        $empty_option = array(
            ''=>'Select Option'
        );

        $course = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $course = $empty_option + $course;

        $emailTemplateContent = EmailTemplateContent::find($id);
        $emailTemplateContent->document = $emailTemplateContent->getMedia('document');

        return view('pages.emailtemplate.editcontent', compact('emailTemplateContent', 'course', 'template_id', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function updatecontent(Request $request, EmailTemplateContent $emailTemplateContent, $id, $template_id)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'content' => 'required',
        ]);

        $emailTemplateContent = EmailTemplateContent::find($id);
        $emailTemplateContent->update($request->all());
        //$emailTemplateContent->document = $emailTemplateContent->getMedia('document');

        if (count($emailTemplateContent->getMedia('document')) > 0) {
            foreach ($emailTemplateContent->getMedia('document') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    unlink(public_path('/Images/EmailContent/' . $media->file_name));
                    $media->delete();
                }
            }
        }

        $media = $emailTemplateContent->getMedia('document')->pluck('file_name')->toArray();

        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $emailTemplateContent->addMedia(public_path('/Images/EmailContent/' . $file))->preservingOriginal()->toMediaCollection('document');
            }
        }

        return redirect()->route('emailtemplate.attach.content', $template_id)
            ->with('success', 'Email Template Content Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function destroycontent(EmailTemplateContent $emailTemplateContent, $id, $template_id)
    {
        $emailTemplateContent = EmailTemplateContent::find($id);
        if ($emailTemplateContent->status == 'active') {
            $emailTemplateContent->status = 'disabled';
        } else {
            $emailTemplateContent->status = 'active';
        }
        $emailTemplateContent->update();
        return redirect()->route('emailtemplate.attach.content', $template_id)
            ->with($emailTemplateContent->status =='active' ? 'success' : 'error', 'Email Template Content ' . ucfirst($emailTemplateContent->status) . ' successfully');
    }

    /**
     * Image Upload Code
     *
     * @return void
     */
    public function dropzoneStore(Request $request, $val = null)
    {
        /*echo public_path('images');
        die;*/
        /*$image = $request->file('file');

        $imageName = rand().'.'.$image->extension();
        $image->move(public_path('Images'),$imageName);

        return response()->json(['success'=>$imageName]);*/

        if(isset($val) && $val == 'emailcontent') {
            $path = public_path('Images/EmailContent');
        } else if(isset($val) && $val == 'adscourse') {
            $path = public_path('Images/AdsCourse');
        }
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
}
