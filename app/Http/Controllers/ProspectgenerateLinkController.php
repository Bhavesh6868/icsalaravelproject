<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Models\Icsacourse;
use Illuminate\Http\Request;

class ProspectgenerateLinkController extends Controller
{
    function index()
    {
        return view('image_crop');
    }

    public function generateLinkPage(Request $request) {
        $empty_option = array(
            ''=>'Select Option'
        );

        $course_id = 'null';
        $branch_id = 'null';
        $frequency= '';
        $courses = Icsacourse::where('status', 'active')->where('show_in_register_form', '1')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $empty_option = array(
            ''=>'Select Option'
        );
        $branches = Branches::where('status', 'active')->where('id', '!=', '3')->pluck('name', 'id')->all();
        $branches = $empty_option + $branches;


        return view('pages.prospect.generatelink', compact( 'courses', 'course_id', 'branches', 'branch_id', 'frequency'));
    }

    public function generateLink(Request $request) {

        $empty_option = array(
            ''=>'Select Option'
        );

        $course_id = $request->course_id;
        $branch_id = $request->branch_id;
        $frequency = $request->frequency;

        $courses = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $courses = $empty_option + $courses;

        $empty_option = array(
            ''=>'Select Option'
        );
        $branches = Branches::where('status', 'active')->where('id', '!=', '3')->pluck('name', 'id')->all();
        $branches = $empty_option + $branches;


        return view('pages.prospect.generatelink', compact( 'courses', 'course_id', 'branches', 'branch_id', 'frequency'));
    }

    public function referLink(Request $request) {
        
    }
}