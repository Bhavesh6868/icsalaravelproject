<?php

namespace App\Http\Controllers;

use App\Models\Icsastudentdetails;
use Illuminate\Http\Request;

class IcsastudentdetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Icsastudentdetails  $icsastudentdetails
     * @return \Illuminate\Http\Response
     */
    public function show(Icsastudentdetails $icsastudentdetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Icsastudentdetails  $icsastudentdetails
     * @return \Illuminate\Http\Response
     */
    public function edit(Icsastudentdetails $icsastudentdetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Icsastudentdetails  $icsastudentdetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icsastudentdetails $icsastudentdetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Icsastudentdetails  $icsastudentdetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icsastudentdetails $icsastudentdetails)
    {
        //
    }
}
