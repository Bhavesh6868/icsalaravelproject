<?php

namespace App\Http\Controllers;

use App\Models\Adscourse;
use App\Models\Icsacourse;
use Illuminate\Http\Request;

class AdscourseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:adscourse-list|adscourse-create|adscourse-edit|adscourse-delete', ['only' => ['index','store']]);
        $this->middleware('permission:adscourse-create', ['only' => ['create','store']]);
        $this->middleware('permission:adscourse-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:adscourse-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adscourses = Adscourse::orderBy('id', 'DESC')->get();

        return view('pages.adscourse.index', compact('adscourses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_option = array(
            ''=>'Select Option'
        );
        $icsacourse = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();

        $icsacourse = $empty_option + $icsacourse;

        return view('pages.adscourse.create', compact('icsacourse'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'course_details' => 'required',
            'course_schedule' => 'required',
            //'document' => 'required',
        ]);

        $checkadscourse = Adscourse::where('course_id', $request->get('course_id'))->get();
        if(count($checkadscourse) > 0) {

            return redirect()->route('adscourse.index')
                ->with('success', 'Ads for Course '.$checkadscourse[0]->icsacourse->name.' already created');
        } else {
            $adscourse = Adscourse::create($request->all());

            foreach ($request->input('document', []) as $file) {
                $adscourse->addMedia(public_path('/Images/AdsCourse/' . $file))->preservingOriginal()->toMediaCollection('adsdocument');
            }

            return redirect()->route('adscourse.index')
                ->with('success', 'Ads Course created successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Adscourse  $adscourse
     * @return \Illuminate\Http\Response
     */
    public function show(Adscourse $adscourse)
    {
        $adscourse->adsdocument = $adscourse->getMedia('adsdocument');
        return view('pages.adscourse.show', compact('adscourse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Adscourse  $adscourse
     * @return \Illuminate\Http\Response
     */
    public function edit(Adscourse $adscourse)
    {
        $empty_option = array(
            ''=>'Select Option'
        );

        $icsacourse = Icsacourse::where('status', 'active')->pluck('name', 'id')->all();
        $icsacourse = $empty_option + $icsacourse;

        //$emailTemplateContent = Adscourse::find($id);
        $adscourse->adsdocument = $adscourse->getMedia('adsdocument');

        return view('pages.adscourse.edit', compact('adscourse', 'icsacourse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Adscourse  $adscourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Adscourse $adscourse)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'course_details' => 'required',
            'course_schedule' => 'required',
            //'document' => 'required',
        ]);

        $adscourse->update($request->all());

        if (count($adscourse->getMedia('adsdocument')) > 0) {
            foreach ($adscourse->getMedia('adsdocument') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    unlink(public_path('/Images/AdsCourse/' . $media->file_name));
                    $media->delete();
                }
            }
        }

        $media = $adscourse->getMedia('adsdocument')->pluck('file_name')->toArray();

        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $adscourse->addMedia(public_path('/Images/AdsCourse/' . $file))->preservingOriginal()->toMediaCollection('adsdocument');
            }
        }

        return redirect()->route('adscourse.index')
            ->with('success', 'Ads Course Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Adscourse  $adscourse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adscourse $adscourse)
    {
        if ($adscourse->status == 'active') {
            $adscourse->status = 'disabled';
        } else {
            $adscourse->status = 'active';
        }
        $adscourse->update();
        return redirect()->route('adscourse.index')
            ->with($adscourse->status =='active' ? 'success' : 'error', 'Ads Course ' . ucfirst($adscourse->status) . ' successfully');
    }
}
