<?php

namespace App\Http\Controllers;

use App\Models\EmailTemplateContent;
use Illuminate\Http\Request;

class EmailTemplateContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailtemplates = EmailTemplate::orderBy('id', 'DESC')->get();

        return view('pages.emailtemplate.indexcontent', compact('emailtemplates'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empty_option = array(
            ''=>'Select Option'
        );

        return view('pages.emailtemplate.createcontent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplateContent $emailTemplateContent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailTemplateContent $emailTemplateContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailTemplateContent $emailTemplateContent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailTemplateContent  $emailTemplateContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplateContent $emailTemplateContent)
    {
        //
    }
}
