<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Traits\HasRoles;
use Lab404\Impersonate\Models\Impersonate;
use Maher\Counters\Traits\HasCounter;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasRoles;
    use Impersonate;
    use HasCounter;

    protected $appends = ['permission_array'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid', 'name', 'username', 'email', 'password', 'mobile', 'civil_id', 'position', 'gender', 'birthdate', 'nationality', 'address', 'avatar', 'isInstructor'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function canAccess($permission) {
        $permissions = Session::get('permission_array');
        return in_array($permission, $permissions) ? true : false;
    }

    public function userbranches()
    {
        return $this->hasMany(UserBranch::class);
    }

    public function rolebranches()
    {
        return $this->hasMany(RoleBranch::class, 'user_id')->where('role_id', auth()->user()->currentRole);
    }

  /*  public function rolebranches()
    {
        return $this->hasManyThrough(RoleBranch::class, User::class, 'id', 'role_id', 'currentRole', 'currentRole');
    }*/

    public function instructor(){
        return $this->hasOne(Instructor::class);
    }
}
