<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsacourse extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'course_fee',
        'duration_month',
        'duration_hours',
        'is_group_course',
        'show_in_register_form'
    ];

    public function instructorcourse()
    {
        return $this->hasOne(Instructorcourse::class);
    }

    public function icsabatch()
    {
        return $this->hasOne(Icsabatch::class);
    }

    public function icsastudent()
    {
        return $this->hasOne(Icsastudent::class);
    }
}
