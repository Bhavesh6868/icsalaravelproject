<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsapaymentrecept extends Model
{
    use HasFactory;

    protected $fillable = [
      'enrollment_id',
      'payment_id',
      'amount',
      'image',
      'date_paid',
      'is_approved',
      'manualcomment',
      'resonnotcredit'
    ];

    public function icsastudent()
    {
        return $this->hasMany(Icsastudent::class); // assuming user_id and task_id as fk
    }
}
