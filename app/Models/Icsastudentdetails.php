<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsastudentdetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'civil_id',
        'fullname',
        'avatar',
        'gender',
        'birthdate',
        'isd_cdoe',
        'mobile',
        'currently_residing',
        'nationality',
        'occupation',
        'address',
        'email',
        'facebook',
        'skype_isd_cdoe',
        'skype'
    ];

    public function icsastudent()
    {
        return $this->hasOne(Icsastudent::class, 'civil_id', 'id');

    }

    public function countries()
    {
        return $this->belongsTo(Countrie::class, 'nationality');
    }

    public function currentlyresiding()
    {
        return $this->belongsTo(Countrie::class, 'currently_residing');

    }
}
