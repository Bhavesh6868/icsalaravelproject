<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsapaymentrefund extends Model
{
    use HasFactory;

    protected $fillable = [
        'enrollment_id',
        'amount',
        'reason',
        'pay_branch',
        'process_by'
    ];
}
