<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branches extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function userbranches()
    {
        return $this->hasMany(UserBranch::class); // assuming user_id and task_id as fk
    }

    public function icsabatch()
    {
        return $this->hasOne(Icsabatch::class);
    }
    //UserBranch::class
}
