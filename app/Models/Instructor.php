<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    use HasFactory;
    protected $fillable = [
        'instructor_id', 'name', 'speaking_name', 'basic_salary'
    ];

    /**
     * Get the User that owns the Purchase.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function instructorcourse()
    {
        return $this->hasMany(Instructorcourse::class);
    }

    public function icsastudent()
    {
        return $this->hasOne(Icsastudent::class);
    }
}
