<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class EmailTemplateContent extends Model implements HasMedia
{
    use HasMediaTrait;
    use HasFactory;

    protected  $table = 'email_template_content';
    protected $fillable = [
        'template_id',
        'course_id',
        'content'
    ];

    public function icsacourse()
    {
        return $this->belongsTo(Icsacourse::class, 'course_id');

    }

    public function emailcontent()
    {
        return $this->belongsTo(EmailTemplate::class, 'template_id');

    }
}
