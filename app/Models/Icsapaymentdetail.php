<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsapaymentdetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'enrollment_id',
        'amount',
        'transfer_note',
        'pay_branch',
        'process_by',
        'pay_detail',
        'payment_bank',
        'philippine_peso',
        'transaction_no',
        'transaction_date'
    ];

    public function paymentmethods()
    {
        return $this->belongsTo(Paymentmethod::class, 'payment_bank');

    }

    public function icsastudent()
    {
        return $this->belongsTo(Icsastudent::class, 'enrollment_id');
    }
}
