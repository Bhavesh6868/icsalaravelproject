<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsabatch extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'course_id',
        'branch_id',
    ];

    public function icsacourse()
    {
        return $this->belongsTo(Icsacourse::class, 'course_id');

    }

    public function branchs()
    {
        return $this->belongsTo(Branches::class, 'branch_id');

    }
}
