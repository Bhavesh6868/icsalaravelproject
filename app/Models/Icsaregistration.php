<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsaregistration extends Model
{
    use HasFactory;
//, 'facebook', 'skype'
    protected $fillable = [
        'registration_id', 'fullname', 'gender', 'birthdate', 'isd_code', 'mobile', 'civil_id', 'currently_residing', 'nationality', 'occupation', 'address', 'email', 'course_id', 'frequency', 'schedule', 'time', 'branch', 'facebook', 'skype_isd_code', 'skype'
    ];
}
