<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleBranch extends Model
{
    use HasFactory;

    /**
     * Get the User that owns the Purchase.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'currentRole', 'id');
    }

    /**
     * Get the Product that owns the Purchase.
     */
    public function branch()
    {
        return $this->belongsTo(Branches::class);
    }
}
