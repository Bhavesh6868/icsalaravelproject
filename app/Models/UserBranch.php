<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBranch extends Model
{
    use HasFactory;
    protected $table = 'user_branch';

    /**
     * Get the User that owns the Purchase.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Product that owns the Purchase.
     */
    public function branch()
    {
        return $this->belongsTo(Branches::class);
    }
}
