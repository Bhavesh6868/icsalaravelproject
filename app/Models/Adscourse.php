<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Adscourse extends Model implements HasMedia
{
    use HasMediaTrait;
    use HasFactory;

    protected $fillable = [
        'course_id',
        'course_details',
        'course_schedule',
    ];


    public function icsacourse()
    {
        return $this->belongsTo(Icsacourse::class, 'course_id');

    }
}
