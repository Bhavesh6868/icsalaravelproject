<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icsastudent extends Model
{
    use HasFactory;

    protected $fillable = [
        'enrollment_id',
        'civil_id',
        'course_id',
        'instructor_id',
        'note',
        'status',
        'frequency',
        'ielts_type',
        'schedule',
        'time',
        'setHours',
        'batch_id',
        'source_id',
        'oldenrollment_no',
        'refer_enrollment',
        'join_date'
    ];

    public function icsastudentdetails()
    {
        return $this->belongsTo(Icsastudentdetails::class, 'civil_id', 'civil_id');
    }

    public function icsacourse()
    {
        return $this->belongsTo(Icsacourse::class, 'course_id');

    }

    public function instructor()
    {
        return $this->belongsTo(Instructor::class, 'instructor_id');

    }

    public function icsapaymentdetail()
    {
        return $this->hasMany(Icsapaymentdetail::class, 'enrollment_id');
    }

    public function icsapaymentrefund()
    {
        return $this->hasMany(Icsapaymentrefund::class, 'enrollment_id');
    }
    /**
     * Get the User that owns the Purchase.
     */
    public function icsapaymentrecept()
    {
        return $this->belongsTo(Icsapaymentrecept::class);
    }
}
