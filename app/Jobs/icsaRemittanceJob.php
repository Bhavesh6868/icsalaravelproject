<?php

namespace App\Jobs;

use App\Mail\icsaRemittanceMail;
use App\Models\Branches;
use App\Models\Icsastudent;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class icsaRemittanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * Get Enrollment Details;
         */
        $getIcsaStudent = Icsastudent::find($this->details['enrollment_id']);

        /**
         * Get Payment Details
         */
        $getpayment = $getIcsaStudent->icsapaymentdetail->where('created_at', '>=', Carbon::today()->toDateString().' 00:00:00')->whereIn('pay_detail', ['NEW STUDENT', 'BALANCE PAYMENT']);

        /**
         * Get Course Details
         */
        $getCourse = $getIcsaStudent->icsacourse;

        /**
         * Get Current Branch name
         */
        $currentBranch = Branches::find($this->details['currentBranch']);

        $amountPaid= 0;
        $paymentmethods = $transaction_no = $transaction_date = $philippine_peso = null;
        foreach ($getpayment as $pay) {
            $paymentmethods = ($pay->paymentmethods->name);
            $amountPaid = $amountPaid + ($pay->amount);
            $philippine_peso = $pay->philippine_peso;
            $transaction_date = $pay->transaction_date;
            $transaction_no = $pay->transaction_no;
        }

        $detailsArray['enrollment_id'] = $getIcsaStudent->enrollment_id;
        $detailsArray['fullname'] = $getIcsaStudent->icsastudentdetails->fullname;
        $detailsArray['course'] = $getCourse->name;
        $detailsArray['location'] = $currentBranch->name;
        $detailsArray['paymentmethods'] = $paymentmethods;
        $detailsArray['amount'] = $amountPaid;
        $detailsArray['transaction_no'] = $transaction_no;
        $detailsArray['transaction_date'] = $transaction_date;
        $detailsArray['philippine_peso'] = $philippine_peso;
        $detailsArray['enrollment_date'] = $getIcsaStudent->enrollment_date;
        $this->details['paymentdetails'][] = $detailsArray;


        $email = new icsaRemittanceMail($this->details);
        Mail::to($getIcsaStudent->icsastudentdetails['email'])->send($email);
    }
}
