@extends('layouts.app')

@section('content')
    <style>
        body {
            background: #7ca5eb;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 m-5">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <ul class="nav nav-tabs row" data-tabs="tabs">
                                    <li class="nav-item col-sm-4">
                                        <a class="nav-link {{ (old('type') == 'user' ? 'active': ( old('type') == 'icsastudent' ?  '' : 'active')) }}" href="#" data-toggle="tab" id="profile">
                                            <i class="material-icons"><span class="material-icons">
account_circle</span></i> User Login
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    <li class="nav-item col-sm-4">
                                        <a class="nav-link {{ (old('type') == 'icsastudent' ? 'active' : ( old('type') == 'user' ?  '' : '')) }}" href="#" data-toggle="tab" id="messages">
                                            <i class="material-icons"><span class="material-icons">
person</span></i> ICSA Student Login
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                    <li class="nav-item col-sm-4">
                                        <a class="nav-link {{ (old('type') == 'capstudent' ? 'active' : ( old('type') == 'user' ?  '' : '')) }}" href="#" data-toggle="tab" id="caplogin">
                                            <i class="material-icons"><span class="material-icons">
person</span></i> CAP Student Login
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane {{ (old('type') == 'user' ? 'active': ( old('type') == 'icsastudent' ?  '' : 'active')) }}" id="login-form">
                                <div class="">
                                    <div class="card-header text-center"><h2>{{ __('Welcome Back To User Login') }}</h2></div>
                                    <div class="card-body">
                                        @if(session()->has('error'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('error') }}
                                            </div>
                                        @endif
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="new-email" autofocus>

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-6 offset-md-4">
                                                    <div class="form-check">
                                                        <input class="" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label" for="remember">
                                                            {{ __('Remember Me') }}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <input type="hidden" name="type" value="user">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Login') }}
                                                    </button>

                                                    @if (Route::has('password.request'))
                                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                                            {{ __('Forgot Your Password?') }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane {{ (old('type') == 'icsastudent' ? 'active': ( old('type') == 'user' ?  '' : '')) }}" id="register-form">
                                <div class="">
                                    <div class="card-header text-center"><h2>{{ __('Welcome Back To ICSA Login') }}</h2></div>
                                    <div class="card-body">
                                        @if(session()->has('error'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('error') }}
                                            </div>
                                        @endif
                                            @if($errors->has('wrong'))
                                                <div class="alert alert-danger">
                                                {{ implode('', $errors->all(':message')) }}
                                                </div>
                                            @endif
                                        <form method="POST" action="{{ route('employee.login') }}">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="enrollment" class="col-md-4 col-form-label text-md-right">{{ __('Enrollment Number') }}</label>

                                                <div class="col-md-6">
                                                    <input id="enrollment" type="number" class="form-control @error('enrollment') is-invalid @enderror" name="enrollment" value="{{ old('enrollment') }}" required autocomplete="new-email" autofocus>

                                                    @error('enrollment')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mobile" type="password" class="form-control @error('wrong') is-invalid @enderror" name="mobile" required autocomplete="new-password">

                                                    @error('wrong')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <input type="hidden" name="type" value="icsastudent">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Login') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane {{ (old('type') == 'capstudent' ? 'active': ( old('type') == 'user' ?  '' : '')) }}" id="cap-register-form">
                                <div class="">
                                    <div class="card-header text-center"><h2>{{ __('Welcome Back To CAP Login') }}</h2></div>
                                    <div class="card-body">
                                        @if(session()->has('error'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('error') }}
                                            </div>
                                        @endif
                                        @if($errors->has('wrong'))
                                            <div class="alert alert-danger">
                                                {{ implode('', $errors->all(':message')) }}
                                            </div>
                                        @endif
                                        <form method="POST" action="{{ route('employee.login') }}">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="enrollment" class="col-md-4 col-form-label text-md-right">{{ __('Enrollment Number') }}</label>

                                                <div class="col-md-6">
                                                    <input id="enrollment" type="number" class="form-control @error('enrollment') is-invalid @enderror" name="enrollment" value="{{ old('enrollment') }}" required autocomplete="new-email" autofocus>

                                                    @error('enrollment')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>

                                                <div class="col-md-6">
                                                    <input id="mobile" type="password" class="form-control @error('wrong') is-invalid @enderror" name="mobile" required autocomplete="new-password">

                                                    @error('wrong')
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-8 offset-md-4">
                                                    <input type="hidden" name="type" value="icsastudent">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Login') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/core/jquery.min.js') }} "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.8/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>

    <script>
        $('#profile').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $("#cap-register-form").fadeOut(100);
            $('#messages').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#messages').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $("#cap-register-form").fadeOut(100);
            $('#profile').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#caplogin').click(function (e) {
            $("#cap-register-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $("#login-form").fadeOut(100);
            $('#profile').removeClass('active');
            $('#messages').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
    </script>
@endsection
