@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit Email Template Data</h4>
                                <p class="card-category"> Edit Email Template.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning text-dark" href="{{ route('emailtemplate.index') }}"> Back To List</a>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            {!! Form::model($emailTemplate, ['method' => 'PATCH','route' => ['emailtemplate.update', $emailTemplate->id]]) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {!! Form::text('title', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Content:</strong>
                                        {!! Form::textarea('content', null, array('placeholder' => 'Content','class' => 'form-control content', 'id=mytextarea')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <script src="https://cdn.tiny.cloud/1/j1fzudbuv848vsykl4lugt9yfs8sqbvgub5bq9p515j9ssyz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            tinymce.init({
                selector: '#mytextarea',
                //theme: 'modern',
                height: "280",
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime table contextmenu paste code'
                    , 'textcolor'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | bullist numlist outdent indent | fontsizeselect | forecolor backcolor',
                setup: function (editor) {
                    editor.on('init', function () {
                    })
                }
            });
        })
    </script>
@stop