@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit Email Template Content Data</h4>
                                <p class="card-category"> Edit Email Template Content.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning text-dark"
                                   href="{{ route('emailtemplate.attach.content', $template_id) }}"> Back To Email Template
                                    List</a>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            {!! Form::model($emailTemplateContent, ['method' => 'PATCH','route' => ['emailtemplate.attach.update', [$emailTemplateContent->id, $template_id]]]) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {!! Form::select('course_id', $course, null, array('class' => 'form-control course_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'course-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Content:</strong>
                                        {!! Form::textarea('content', null, array('placeholder' => 'Content','class' => 'form-control content', 'id=mytextarea')) !!}
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="document">Documents</label>
                                        <div class="needsclick dropzone" id="document-dropzone">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <script src="https://cdn.tiny.cloud/1/j1fzudbuv848vsykl4lugt9yfs8sqbvgub5bq9p515j9ssyz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <script type="text/javascript">

        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('dropzone.store', 'emailcontent') }}',
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                //console.log(file)
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = file.name;//uploadedDocumentMap[file.name]
                }
                //console.log(file.name);
                $('form').find('input[name="document[]"][value="' + name + '"]').remove()
            },
            init: function () {
                myDropzone = this;
                        @if(isset($emailTemplateContent))
                var filespath =
                        {!! json_encode(asset('Images/EmailContent/')) !!}
                        @endif
                        @if(isset($emailTemplateContent) && $emailTemplateContent->document)
                var files =
                {!! json_encode($emailTemplateContent->document) !!}
                    for (var i in files) {
                    var file = files[i]

                    var mockFile = {name: file.file_name, size: file.size};
                    myDropzone.emit("addedfile", mockFile, file.file_name);
                    myDropzone.emit("thumbnail", mockFile, filespath+'/'+file.file_name);
                   // myDropzone.emit("complete", mockFile);

                    /*var mydropzone = this;
                    mydropzone.emit("thumbnail", file, "Images/x.jpg");*/

                   // var mockFile = {name: file.file_name, size: file.size};

                    //this.options.addedfile.call(this, file);
                    /*this.options.thumbnail.call(
                        this,
                        mockFile,
                        file.file_name
                    );*/
                    mockFile.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
                }
                @endif
            }
        }

        $(document).ready(function () {
            tinymce.init({
                selector: '#mytextarea',
                //theme: 'modern',
                height: "280",
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime table contextmenu paste code'
                    , 'textcolor'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | bullist numlist outdent indent | fontsizeselect | forecolor backcolor',
                setup: function (editor) {
                    editor.on('init', function () {
                    })
                }
            });
        })
    </script>
    <style>
        .dz-message{
            text-align: center;
            font-size: 28px;
        }
        .dropzone .dz-preview .dz-image {
            width: 200px;
            height: 200px;
            display: inline-block; /* makes it fit in like an <img> */
            background-size: cover; /* or contain */
            background-position: center center;
            background-repeat: no-repeat;
        }

        .dropzone .dz-preview .dz-image img {
            width: 200px;
            height: 200px;
        }
    </style>
@stop