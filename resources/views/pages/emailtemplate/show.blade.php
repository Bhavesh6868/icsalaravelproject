@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Show Email Template Information</h4>
                                <p class="card-category"> Email Template Details</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning text-dark" href="{{ route('emailtemplate.index') }}"> Back To List</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Title:</strong>
                                        {{ $emailTemplate->title }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Content:</strong>
                                        {!! $emailTemplate->content !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Status:</strong>
                                        <span class="badge {{ $emailTemplate->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($emailTemplate->status) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop