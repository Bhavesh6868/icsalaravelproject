@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Generate Dynamic Email Template</h4>
                                <p class="card-category"> All Email Templated.</p>
                            </div>
                            <div class="pull-right">
                                @if(auth()->user()->canAccess('emailtemplate-create'))
                                    <a class="btn btn-success" href="{{ route('emailtemplate.create') }}"><i
                                                class="fa fa-plus"></i>&nbsp;&nbsp; Create New Email Template</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table id="dtBasicExample" class="table table-striped table-bordered table-sm"
                                       cellspacing="0" width="100%">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($emailtemplates as $key => $emailtemplate)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $emailtemplate->title }}</td>
                                            <td>
                                                <span class="badge {{ $emailtemplate->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($emailtemplate->status) }}</span>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle btn-sm"
                                                            data-toggle="dropdown">Action<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a class="" href="{{ route('emailtemplate.attach.content',$emailtemplate->id) }}"><i class="fa fa-plus"></i>&nbsp;Add Content</a></li>
                                                        <li><a class="" href="{{ route('emailtemplate.show',$emailtemplate->id) }}"><i class="fa fa-eye"></i>&nbsp;Show Template</a></li>
                                                        @if(auth()->user()->canAccess('emailtemplate-edit'))
                                                            <li>
                                                                <a class="" href="{{ route('emailtemplate.edit',$emailtemplate->id) }}"><i class="fa fa-pencil"></i>&nbsp; Edit Template </a></li>
                                                        @endif
                                                        @if(auth()->user()->canAccess('emailtemplate-delete'))
                                                            <li>
                                                                <a href="#"><i class="fa fa-crosshairs"></i> &nbsp;
                                                                {!! Form::open(['method' => 'DELETE','route' => ['emailtemplate.destroy', $emailtemplate->id],'style'=>'display:inline', 'class' => 'delete-source']) !!}
                                                                @csrf
                                                                {!! Form::submit($emailtemplate->status == 'active' ? 'Make Disabled' : 'Make Active', ['class' => $emailtemplate->status == 'active' ? 'btn btn-outline-danger btn-sm customBtn' : 'btn btn-outline-success btn-sm customBtn', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                                {!! Form::close() !!}
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <style>
        .customBtn {
            padding: 0 !important;
            box-shadow: none;
            border: 0px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
    <style>
        #dtBasicExample_filter {
            float: right !important;
        }
    </style>
@stop