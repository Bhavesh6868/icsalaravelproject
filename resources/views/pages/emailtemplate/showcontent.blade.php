@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Show Email Template Content Information</h4>
                                <p class="card-category"> Email Template Details</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning text-dark"
                                   href="{{ route('emailtemplate.attach.content', $id) }}"><i class="fa fa-list-ul"></i>&nbsp;
                                    Back To Email Template
                                    Content List</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <strong class="col-sm-2">Status:</strong>
                                        <div class="col-sm-8">
                                            <span class="badge {{ $emailTemplateContent->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($emailTemplateContent->status) }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <strong class="col-sm-2">Title:</strong>
                                        <div class="col-sm-8">
                                            {{ $emailTemplateContent->emailcontent->title }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <strong class="col-sm-2">Course:</strong>
                                        <div class="col-sm-8">
                                            {!! $emailTemplateContent->icsacourse->name !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <strong class="col-sm-2">Content:</strong>
                                        <div class="col-sm-8">
                                            {!! $emailTemplateContent->content !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group row">
                                        <strong class="col-sm-12">Images:</strong>
                                        <div class="col-sm-12">
                                            <div class="demo-gallery">
                                                <ul id="lightgallery" class="list-unstyled row">
                                                    @foreach($emailTemplateContent->document as $val)
                                                        {{--{{ $val['file_name'] }}--}}
                                                        <li class="col-xs-6 col-sm-4 col-md-4 col-lg-4" data-responsive="{{ asset('Images/EmailContent').'/'.$val['file_name'] }}" data-src="{{ asset('Images/EmailContent').'/'.$val['file_name'] }}" data-sub-html="<h4>Images</h4><p>Image for {{ $emailTemplateContent->icsacourse->name }} Email Content .</p>">
                                                            <a href="">
                                                                <img class="img-responsive" width="250" src="{{ asset('Images/EmailContent').'/'.$val['file_name'] }}">
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <link href="https://cdn.jsdelivr.net/lightgallery/1.3.9/css/lightgallery.min.css" rel="stylesheet">
    <style>
       /* body{
            background-color: #152836
        }
        h2{color:#fff;margin-bottom:40px;text-align:center;font-weight:100;}*/
        .demo-gallery > ul {
            margin-bottom: 0;
        }
        .demo-gallery > ul > li {
            float: left;
            margin-bottom: 5px;
            margin-right: 0px;
            width: 250px;
        }
        .demo-gallery > ul > li a {
            border: 3px solid #FFF;
            border-radius: 3px;
            display: block;
            overflow: hidden;
            position: relative;
            float: left;
        }
        .demo-gallery > ul > li a > img {
            -webkit-transition: -webkit-transform 0.15s ease 0s;
            -moz-transition: -moz-transform 0.15s ease 0s;
            -o-transition: -o-transform 0.15s ease 0s;
            transition: transform 0.15s ease 0s;
            -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
            height: 100%;
            width: 100%;
        }
        .demo-gallery > ul > li a:hover > img {
            -webkit-transform: scale3d(1.1, 1.1, 1.1);
            transform: scale3d(1.1, 1.1, 1.1);
        }
        .demo-gallery > ul > li a:hover .demo-gallery-poster > img {
            opacity: 1;
        }
        .demo-gallery > ul > li a .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.1);
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            -webkit-transition: background-color 0.15s ease 0s;
            -o-transition: background-color 0.15s ease 0s;
            transition: background-color 0.15s ease 0s;
        }
        .demo-gallery > ul > li a .demo-gallery-poster > img {
            left: 50%;
            margin-left: -10px;
            margin-top: -10px;
            opacity: 0;
            position: absolute;
            top: 50%;
            -webkit-transition: opacity 0.3s ease 0s;
            -o-transition: opacity 0.3s ease 0s;
            transition: opacity 0.3s ease 0s;
        }
        .demo-gallery > ul > li a:hover .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.5);
        }
        .demo-gallery .justified-gallery > a > img {
            -webkit-transition: -webkit-transform 0.15s ease 0s;
            -moz-transition: -moz-transform 0.15s ease 0s;
            -o-transition: -o-transform 0.15s ease 0s;
            transition: transform 0.15s ease 0s;
            -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
            height: 100%;
            width: 100%;
        }
        .demo-gallery .justified-gallery > a:hover > img {
            -webkit-transform: scale3d(1.1, 1.1, 1.1);
            transform: scale3d(1.1, 1.1, 1.1);
        }
        .demo-gallery .justified-gallery > a:hover .demo-gallery-poster > img {
            opacity: 1;
        }
        .demo-gallery .justified-gallery > a .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.1);
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            -webkit-transition: background-color 0.15s ease 0s;
            -o-transition: background-color 0.15s ease 0s;
            transition: background-color 0.15s ease 0s;
        }
        .demo-gallery .justified-gallery > a .demo-gallery-poster > img {
            left: 50%;
            margin-left: -10px;
            margin-top: -10px;
            opacity: 0;
            position: absolute;
            top: 50%;
            -webkit-transition: opacity 0.3s ease 0s;
            -o-transition: opacity 0.3s ease 0s;
            transition: opacity 0.3s ease 0s;
        }
        .demo-gallery .justified-gallery > a:hover .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.5);
        }
        .demo-gallery .video .demo-gallery-poster img {
            height: 48px;
            margin-left: -24px;
            margin-top: -24px;
            opacity: 0.8;
            width: 48px;
        }
        .demo-gallery.dark > ul > li a {
            border: 3px solid #04070a;
        }
        .home .demo-gallery {
            padding-bottom: 80px;
        }
    </style>
    <script src="{{asset('assets/js/gallerylightbox.js')}}"></script>
    <script>
        function fullscreen() {
            if (document.fullscreenElement === null) {
                this._fullscreen.openFullscreen();
            } else {
                this._fullscreen.closeFullscreen();
            }
        }
        $(document).ready(function () {
            $('#lightgallery').lightGallery();
        });
    </script>
@stop