@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit ICSA Batch</h4>
                                <p class="card-category"> Edit ICSA Batch name.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('icsabatch.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            {!! Form::model($icsabatch, ['method' => 'PATCH','route' => ['icsabatch.update', $icsabatch->id]]) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <strong>Course:</strong>
                                        {{--{{ dd($branches, $userBranch) }}--}}
                                        {!! Form::select('course_id', $icsacourses, null, array('class' => 'form-control icsacourses','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'course-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <strong>Branch:</strong>
                                        {!! Form::select('branch_id', $branches,null, array('class' => 'form-control','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#course-select').selectpicker();
            $('#branch-select').selectpicker();
        })
    </script>
@stop