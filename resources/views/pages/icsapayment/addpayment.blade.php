@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">ICSA Add Student Payment</h4>
                                <p class="card-category"> Search Student details and make payment.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('icsastudents.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (count($errors) > 0 && empty($icsastudent))
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif

                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif


                            {!! Form::open(array('route' => 'icsapayment.getenrolldetails', 'method'=>'POST')) !!}
                            <div class="row pull-center">
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center errormessage d-none">
                                    <div class="alert alert-danger">
                                        Sorry Information Not Found
                                    </div>
                                </div>
                            </div>
                            <div class="row pull-center">
                                <div class="col-xs-12 col-sm-12 col-md-2 text-center">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 pull-center">
                                    <div class="form-group row">
                                        {!! Form::number('enrollment_id', $icsastudent ? old('enrollment_id', $icsastudent->enrollment_id) : Session::get('enroll'), array('placeholder' => 'Enter Enrollment Number','class' => 'form-control registration_id')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 pull-center">
                                    <div class="form-group">
                                        {!! Form::hidden('fromtype', 'addpayment') !!}
                                        <button type="submit" class="btn btn-primary btn-md " style="margin: 0">Load
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            {{--{{ dd($icsastudent) }}--}}
            @if( !empty($icsastudent))
                {!! Form::model($icsastudent ? $icsastudent : null, ['id'=> 'defaultForm', 'enctype' => "multipart/form-data", 'method' => 'POST','route' => ['icsapayment.store', $icsastudent ? $icsastudent->id : null]]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="margin-top: 0">
                            <div class="card-body">
                                @if (count($errors) > 0 && !empty($icsastudent))
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif

                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif

                                    <div class="row" style="font-size: 16px">
                                        <div class="col-md-12">
                                            <h4 style="background: #9B34B2; color: white;"
                                                class="text-left border-bottom p-1">Student Details</h4>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-3">
                                            <div class="form-group">
                                                <strong>Enrollment:</strong>
                                                {{ $icsastudent->enrollment_id }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Name:</strong>
                                                {{ $icsastudent->icsastudentdetails->fullname }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-5">
                                            <div class="form-group">
                                                <strong>Course:</strong>
                                                {{ $icsastudent->icsacourse->name}}
                                            </div>
                                        </div>
                                        {{--<div class="col-xs-12 col-sm-12 col-md-3">
                                            <div class="form-group">
                                                <strong>Course Fees:</strong>
                                                {{ $icsastudent->icsacourse->course_fee }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Paid Amount:</strong>
                                                {{ $paidAmt }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-5">
                                            <div class="form-group">
                                                <strong>Discount:</strong>
                                                {{ $discountAmt }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-3">
                                            <div class="form-group">
                                                <strong>Balance:</strong>
                                                {{ $icsastudent->icsacourse->course_fee - ($paidAmt+$discountAmt) }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Refund:</strong>
                                                {{ $refundAmtSum }}
                                            </div>
                                        </div>--}}
                                    </div>
                                    <div class="row" style="font-size: 16px">
                                        <div class="col-md-12">
                                            <h4 style="background: #9B34B2; color: white;"
                                                class="text-left border-bottom p-1">Payment Details</h4>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Course Fees:</strong>
                                                {{ $icsastudent->icsacourse->course_fee }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Paid Amount:</strong>
                                                {{ $paidAmt }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Discount Amount:</strong>
                                                {{ $discountAmt }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Balance Amount:</strong>
                                                {{ $icsastudent->icsacourse->course_fee - ($paidAmt+$discountAmt) }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Additional Amount:</strong>
                                                {{ $additionalAmt }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Total Paid Includind Additional:</strong>
                                                {{ $paidAmt+ $additionalAmt }}
                                            </div>
                                        </div>
                                        @if($refundAmtSum > 0)
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Refund Amount:</strong>
                                                    {{ $refundAmtSum }}
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                   {{-- @if(count($refundAmt)>0)
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 style="background: #9B34B2; color: white;"
                                                    class="text-left border-bottom p-1">Refund Details</h4>
                                                <table class="table table-striped table-bordered table-sm"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <td>#</td>
                                                        <td>Amount</td>
                                                        <td>Reason</td>
                                                        <td>Date</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($refundAmt as $key => $refund)
                                                        <tr>
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ $refund->amount }}</td>
                                                            <td>{{ $refund->reason }}</td>
                                                            <td>{{ date('Y-m-d h:i A', strtotime($refund->date_refunded))}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif--}}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 style="background: #9B34B2; color: white;" class="text-left  p-1">Add Payment Details</h4>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-9">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <strong>Payment Method:</strong><i class="fa fa-asterisk"
                                                                                       style="color: #bd2130; font-size: 12px;"></i>
                                                    {!! Form::select('paymentmethod', $paymentmethod, null, array('class' => 'form-control paymentmethod','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'paymentmethod', 'required')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <strong>Amount:</strong><i class="fa fa-asterisk"
                                                                               style="color: #bd2130; font-size: 12px;"></i>
                                                    {!! Form::text('amount', null, array('placeholder' => 'Amount','class' => 'form-control amount', 'required')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 transfer_from_another" style="display: none">
                                                <div class="form-group">
                                                    <strong>Account No:</strong><i class="fa fa-asterisk"
                                                                               style="color: #bd2130; font-size: 12px;"></i>
                                                    {!! Form::text('account_no', null, array('placeholder' => 'Amount','class' => 'form-control account_no', 'required')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <strong>Discount:</strong>
                                                    {!! Form::text('discount', null, array('placeholder' => 'Discount','class' => 'form-control discount')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <strong>Additional:</strong>
                                                    {!! Form::text('additional', null, array('placeholder' => 'Additional Payment','class' => 'form-control additional')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <strong>Reason.:</strong>
                                                    {!! Form::textarea('transfer_note', null, array('placeholder' => 'Reason', 'rows=3', 'class' => 'form-control transfer_note')) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="card card-profile hideCash">
                                            <div class="row">
                                                <div class="col-sm-12" style="margin-top: 1rem;">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail"
                                                             data-trigger="fileinput"
                                                             style="width:200px; height:160px; border: 1px solid #d2d2d2"></div>
                                                        <div>
                            <span class="btn btn-default customBtn btn-file"><span class="fileinput-new">Select image Cash</span><span
                                        class="fileinput-exists">Change</span><input type="file"
                                                                                     accept="image/png, image/jpeg"
                                                                                     name="myimage"></span>
                                                            <a href="#"
                                                               class="btn btn-default customBtn fileinput-exists"
                                                               data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-profile hideCashRemitt" style="display: none">
                                            <div class="row">
                                                <div class="col-sm-12" style="margin-top: 1rem;">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail"
                                                             data-trigger="fileinput"
                                                             style="width:200px; height:150px;"></div>
                                                        <div>
                            <span class="btn btn-default customBtn btn-file"><span class="fileinput-new">Select image Remittance</span><span
                                        class="fileinput-exists">Change</span><input type="file"
                                                                                     accept="image/png, image/jpeg"
                                                                                     id="remittanceImage"
                                                                                     name="myimage1" required></span>
                                                            <a href="#"
                                                               class="btn btn-default customBtn fileinput-exists"
                                                               data-dismiss="fileinput">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hideCashRemitt" style="display: none">
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Philippine Peso:</strong><i class="fa fa-asterisk"
                                                                                style="color: #bd2130; font-size: 12px;"></i>
                                            {!! Form::text('philippine_peso', null, array('placeholder' => 'Philippine Peso','class' => 'form-control philippine_peso', 'required')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Transaction No:</strong><i class="fa fa-asterisk"
                                                                               style="color: #bd2130; font-size: 12px;"></i>
                                            {!! Form::text('transaction_no', null, array('placeholder' => 'Transaction No.','class' => 'form-control transaction_no', 'required')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Remittance Date:</strong><i class="fa fa-asterisk"
                                                                                style="color: #bd2130; font-size: 12px;"></i>
                                            {!! Form::date('transaction_date', null, array('placeholder' => 'Remittance Date.','class' => 'form-control transaction_date', 'required')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                        {!! Form::hidden('enrollment_id', $icsastudent ? $icsastudent->enrollment_id : null, array('placeholder' => 'Remittance Date.','class' => 'form-control transaction_date', 'required')) !!}
                                        {!! Form::hidden('id', $icsastudent ? $icsastudent->id : null, array('placeholder' => 'Remittance Date.','class' => 'form-control transaction_date', 'required')) !!}
                                        <button type="submit"
                                                class="btn btn-primary" {{ ($icsastudent) ? '' : 'disabled' }}>Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@stop
@section('custom-script')
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery-ui.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jasny-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrapValidator/bootstrapValidator.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('assets/bootstrapValidator/bootstrapValidator.min.js') }}"></script>
    <style>
        .btn.btn-default.customBtn {
            padding: 0.24rem 1.14rem;
        }

        .help-block {
            color: #bd2130;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#defaultForm').bootstrapValidator();
            if (typeof $('#paymentmethod').val() != "undefined" && $('#paymentmethod').val() != '') {
                $('#paymentmethod').trigger('click');
            }
        });
        $(document).delegate('#paymentmethod', 'click', function (event) {
            if (typeof $('#paymentmethod').val() != "undefined" && $('#paymentmethod').val() != '') {
                //alert($('#paymentmethod').val())
                if ($('#paymentmethod').val() == 1 || $('#paymentmethod').val() == 2 || $('#paymentmethod').val() == 3) {
                    $('.hideCash').show();
                    $('.hideCashRemitt').hide();
                    if($('#paymentmethod').val() == 3) {
                        $(".transfer_from_another").show();
                    } else {
                        $(".transfer_from_another").hide();
                    }
                    if(typeof $('.amount').val() != "undefined" && $('.amount').val() != '') {
                        $('button').attr('disabled', false)
                    }
                } else if ($('#paymentmethod').val() != 1 || $('#paymentmethod').val() != 2 || $('#paymentmethod').val() != 3) {
                    $('.hideCash').hide();
                    $('.hideCashRemitt').show();
                    $(".transfer_from_another").hide();
                    //$('#otherPayMethod').hide();
                }
            } else {
                $('.hideCash').show();
                $('.hideCashRemitt').hide();
            }
        });
    </script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>--}}
@stop