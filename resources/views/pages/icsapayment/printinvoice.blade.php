@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row printNot">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">ICSA Print Payment Invoice</h4>
                                <p class="card-category"> Search Enrollment and print invoice.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('icsastudents.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (count($errors) > 0 && empty($icsastudent))
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif

                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif


                            {!! Form::open(array('route' => 'icsapayment.getenrolldetails', 'method'=>'POST')) !!}
                            <div class="row pull-center">
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center errormessage d-none">
                                    <div class="alert alert-danger">
                                        Sorry Information Not Found
                                    </div>
                                </div>
                            </div>
                            <div class="row pull-center">
                                <div class="col-xs-12 col-sm-12 col-md-2 text-center">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6 pull-center">
                                    <div class="form-group row">
                                        {!! Form::number('enrollment_id', $icsastudent ? old('enrollment_id', $icsastudent->enrollment_id) : Session::get('enroll'), array('placeholder' => 'Enter Enrollment Number','class' => 'form-control registration_id')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 pull-center">
                                    <div class="form-group">
                                        {!! Form::hidden('fromtype', 'printinvoice') !!}
                                        <button type="submit" class="btn btn-primary btn-md " style="margin: 0">Load
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            {{--{{ dd($icsastudent) }}--}}
            @if( !empty($icsastudent))
                {!! Form::model($icsastudent ? $icsastudent : null, ['id'=> 'defaultForm', 'enctype' => "multipart/form-data", 'method' => 'POST','route' => ['icsapayment.store', $icsastudent ? $icsastudent->id : null]]) !!}
                <div class="row" id="html-content-holder">
                    <div class="col-md-12">
                        <div class="card" style="margin-top: 0">
                            <div class="card-body">
                                @if (count($errors) > 0 && !empty($icsastudent))
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif

                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif

                                <div class="row">
                                    <table class="border-bottom print_This" style="text-align:left; display: none">
                                        <tr>
                                            <td rowspan=4><img src="{{ asset('Images/logo128.png') }}"
                                                               style="align:left;width:90px;height:70px"></td>
                                            <td><h3>&nbsp;&nbsp;INTERNATIONAL INSTITUTE OF COMPUTER SCIENCE &
                                                    ADMINISTRATION</h3></td>
                                        </tr>
                                        <tr>
                                            <td><h3>&nbsp;&nbsp;8th Floor Panasonic Tower</h3></td>
                                        </tr>
                                        <tr>
                                            <td><h3>&nbsp;&nbsp;Maliya, Kuwait City</h3></td>
                                        </tr>
                                        <tr>
                                            <td><h3>&nbsp;&nbsp;Contact: 22467301/99302850 Email:admin@icsa.us</h3></td>
                                        </tr>
                                    </table>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <h4 style="background: #9B34B2; color: white;"
                                            class="text-left border-bottom p-1">Personal Details</h4>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Enrollment No:</strong>
                                                    {{ $icsastudent->enrollment_id }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Civil Id:</strong>
                                                    {{ $icsastudent->civil_id }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Birth Date:</strong>
                                                    {{ date('d M Y' , strtotime($icsastudent->icsastudentdetails->birthdate)) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Name:</strong>
                                                    {{ $icsastudent->icsastudentdetails->fullname }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-8">
                                                <div class="form-group">
                                                    <strong>Course Name:</strong>
                                                    {{ $icsastudent->icsacourse->name }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Gender:</strong>
                                                    {{ $icsastudent->icsastudentdetails->gender }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Email:</strong>
                                                    {{ $icsastudent->icsastudentdetails->email }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Mobile:</strong>
                                                    {{ $icsastudent->icsastudentdetails->mobile }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Join Date:</strong>
                                                    {{ date('d M y', strtotime($icsastudent->join_date)) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4">
                                                <div class="form-group">
                                                    <strong>Enrollment Date:</strong>
                                                    {{ date('d M y h:i:s A', strtotime($icsastudent->enrollment_date)) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="font-size: 16px">
                                    <div class="col-md-12 text-left">
                                        <h4 style="background: #9B34B2; color: white;"
                                            class="text-left border-bottom p-1">Payment Details</h4>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Course Fees:</strong>
                                            {{ $icsastudent->icsacourse->course_fee }}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Paid Amount:</strong>
                                            {{ $paidAmt }}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Discount Amount:</strong>
                                            {{ $discountAmt }}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Balance Amount:</strong>
                                            {{ $icsastudent->icsacourse->course_fee - ($paidAmt+$discountAmt) }}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Additional Amount:</strong>
                                            {{ $additionalAmt }}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Total Paid Includind Additional:</strong>
                                            {{ $paidAmt+ $additionalAmt }}
                                        </div>
                                    </div>
                                    @if($refundAmtSum > 0)
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Refund Amount:</strong>
                                                {{ $refundAmtSum }}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                @if(count($refundAmt)>0)
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive" style="font-weight: bold">
                                                <h4 style="background: #9B34B2; color: white;" class="border-bottom text-left  p-1">
                                                    Refund Details</h4>
                                                <table class="table table-striped table-bordered table-sm"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr style="background: #eec6f7; font-weight: bold">
                                                        <td>#</td>
                                                        <td>Amount</td>
                                                        <td>Reason</td>
                                                        <td>Date</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($refundAmt as $key => $refund)
                                                        <tr>
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ $refund->amount }}</td>
                                                            <td>{{ $refund->reason }}</td>
                                                            <td>{{ date('Y-m-d h:i A', strtotime($refund->date_refunded))}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive" style="font-weight: bold">
                                            <h4 style="background: #9B34B2; color: white;"
                                                class="text-left border-bottom p-1">Paid Details</h4>
                                            <table class="table table-striped table-bordered table-sm"
                                                   cellspacing="0" width="100%">
                                                <tr style="background: #eec6f7; font-weight: bold">
                                                    <td>#</td>
                                                    <td>Pay Mehtod</td>
                                                    <td>Date</td>
                                                    <td>Amount</td>
                                                    <td>Branch</td>
                                                    <td>Process By</td>
                                                    <td>Note</td>
                                                </tr>
                                                @if( count($icsastudent->icsapaymentdetail) > 0 )
                                                    @foreach($icsastudent->icsapaymentdetail as $key => $paydetail)
                                                        <tr>
                                                            <td>{{ ++$key }}</td>
                                                            <td>{{ $paydetail->pay_detail }}</td>
                                                            <td>{{ $paydetail->date_paid }}</td>
                                                            <td>{{ $paydetail->amount }}</td>
                                                            <td>{{ $paydetail->paymentmethods->name }}</td>
                                                            <td>{{ ucfirst($paydetail->process_by) }}</td>
                                                            <td>{{ ucfirst($paydetail->transfer_note) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="7">No Date</td>
                                                    </tr>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                        {{--<input style="padding:5px;" value="Print Receipt" class="btn btn-default" type="button" onclick="myFunction()"></input>--}}
                                        <a href="javascript:window.print()" target="_self" id="printNot"
                                           class="btn btn-primary printNot">
                                            <span class="glyphicon glyphicon-print"></span> PRINT</a>

                                        <a target="_blank"
                                           data-student="{{ $icsastudent->icsastudentdetails->fullname . '_' . $icsastudent->enrollment_id }}"
                                           class="btn btn-primary downloadImg" id="btn-Convert-Html2Image" href="#">
                                            <span class="glyphicon glyphicon-print"></span> Download
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="previewImage"></div>
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@stop
@section('custom-script')
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery-ui.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jasny-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrapValidator/bootstrapValidator.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('assets/bootstrapValidator/bootstrapValidator.min.js') }}"></script>
    <style>
        .btn.btn-default.customBtn {
            padding: 0.24rem 1.14rem;
        }

        .help-block {
            color: #bd2130;
        }

        .print_This {
            display: none;
            text-align: center;
        }

        @media print {
            table tr td {
                font-weight: bold;
            }

            html, body {
                color: yellow;
                font-size: 0.9em;
            }

            .printNot, .footer, .fixed-plugin, .downloadImg {
                display: none;
            }

            .print_This {
                display: block !important;
            }
        }
    </style>
    <script src="{{ asset('assets/htmltocanvas/html2canvas.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var element = $("#html-content-holder"); // global variable
            var getCanvas; // global variable
            html2canvas(element, {
                onrendered: function (canvas) {
                    //$("#previewImage").append(canvas);
                    getCanvas = canvas;
                },
                backgroundColor: "black",
            });
            $("#btn-Convert-Html2Image").on('click', function () {

                var name = $(this).data('student'); //.split(" ");
                var nameEnroll = name.replace(/ /g, '').split("_");
                var fileName = (nameEnroll[0] + "_" + nameEnroll[1]).toLowerCase();
                var imgageData = getCanvas.toDataURL('image/jpeg');
                // Now browser starts downloading it instead of just showing it
                var newData = imgageData.replace(/^data:image\/jpeg/, "data:application/octet-stream");
                $("#btn-Convert-Html2Image").attr("download", fileName + '.jpeg').attr("href", newData);
            });

            $('#defaultForm').bootstrapValidator();
            if (typeof $('#paymentmethod').val() != "undefined" && $('#paymentmethod').val() != '') {
                $('#paymentmethod').trigger('click');
            }
        });
        $(document).delegate('#paymentmethod', 'click', function (event) {
            if (typeof $('#paymentmethod').val() != "undefined" && $('#paymentmethod').val() != '') {
                //alert($('#paymentmethod').val())
                if ($('#paymentmethod').val() == 1 || $('#paymentmethod').val() == 2 || $('#paymentmethod').val() == 3) {
                    $('.hideCash').show();
                    $('.hideCashRemitt').hide();
                    if ($('#paymentmethod').val() == 3) {
                        $(".transfer_from_another").show();
                    } else {
                        $(".transfer_from_another").hide();
                    }
                    if (typeof $('.amount').val() != "undefined" && $('.amount').val() != '') {
                        $('button').attr('disabled', false)
                    }
                } else if ($('#paymentmethod').val() != 1 || $('#paymentmethod').val() != 2 || $('#paymentmethod').val() != 3) {
                    $('.hideCash').hide();
                    $('.hideCashRemitt').show();
                    $(".transfer_from_another").hide();
                    //$('#otherPayMethod').hide();
                }
            } else {
                $('.hideCash').show();
                $('.hideCashRemitt').hide();
            }
        });
    </script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>--}}
@stop