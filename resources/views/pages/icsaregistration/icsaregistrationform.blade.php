<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>ICSA Registration</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('Images/logo128.png') }}" rel="icon">
    <link href="{{ asset('Images/logo128.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('theevent/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('theevent/vendor/venobox/venobox.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('theevent/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('theevent/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('theevent/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('theevent/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet"/>
</head>

<body>
<div class="loader"></div>
<!-- ======= Header ======= -->
<header id="header">
    <div class="container">
        <div id="logo" class="pull-left">
            <a href="#" class="scrollto" style="color: #ffffff"><img src="{{ asset('Images/logo.png')  }}" alt=""
                                                                     title=""> &nbsp;Your Partnet In Success</a>
        </div>
    </div>
</header><!-- End Header -->

<!-- ======= Intro Section ======= -->
<section id="intro" style="height: 50vh !important;">
    <div class="intro-container" data-aos="zoom-in" data-aos-delay="100">
        <h1 class="mb-4 pb-0">ICSA<br><span>PRE-REGISTRATION</span> FORM</h1>
        <p class="mb-4 pb-0">To register now, fill the form below and take a note/photo of the registration details.
            Just present your Registration Code to the Reception In-charge on the day of your enrollment.</p>
    </div>
</section><!-- End Intro Section -->

<main id="main">
    <section id="contact" class="section-bg">
        <div class="container" data-aos="fade-up">

            {!! Form::open(array('route' => 'icsaregistration.store', ' enctype="multipart/form-data"', 'method'=>'POST')) !!}
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="section-header text-center" style="margin-bottom: 40px; padding-bottom: 2px;">
                        <h4 style="color: #0a6ebd">Personal Information</h4>
                        {{--<p>Nihil officia ut sint molestiae tenetur.</p>--}}
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Name: </strong><i class="fa fa-asterisk"
                                                          style="color: #bd2130; font-size: 12px;"></i>{!! Form::text('fullname', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Birth Date:</strong><i class="fa fa-asterisk"
                                                               style="color: #bd2130; font-size: 12px;"></i>{!! Form::date('birthdate', null, array('placeholder' => 'Birth date','class' => 'form-control' )) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Civil ID:</strong><i class="fa fa-asterisk"
                                                             style="color: #bd2130; font-size: 12px;"></i>{!! Form::text('civil_id', null, array('placeholder' => 'Civil ID','class' => 'form-control' )) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Gender:</strong><i class="fa fa-asterisk"
                                                           style="color: #bd2130; font-size: 12px;"></i>{!! Form::select('gender', ['' => 'select Gender', 'male' => 'Male', 'female'=>'Female'], null, array('class' => 'form-control','id' => 'gender-select')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Nationality:</strong><i class="fa fa-asterisk"
                                                                style="color: #bd2130; font-size: 12px;"></i>
                                {{--{!! Form::text('nationality', null, array('placeholder' => 'Nationality','class' => 'form-control' )) !!}--}}
                                {!! Form::select('nationality', $nationality,null, array('class' => 'form-control','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'nationality-select')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Currently Residing:</strong>
                                <i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                {{--{!! Form::text('currently_residing', null, array('placeholder' => 'Currently Residing','class' => 'form-control username')) !!}--}}
                                {!! Form::select('currently_residing', $nationality,null, array('class' => 'form-control','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'currently_residing-select')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8">
                            <div class="form-group">
                                <strong>Address:</strong>
                                {!! Form::textarea('address', null, array('placeholder' => 'Address','class' => 'form-control', 'rows' => "2" )) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Occupation:</strong>
                                {!! Form::text('occupation', null, array('placeholder' => 'Occupation','class' => 'form-control', 'autocomplete' => 'off' )) !!}
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="section-header text-center" style="margin-bottom: 40px; padding-bottom: 2px;">
                                <h4 style="color: #0a6ebd">Contact Details</h4>
                                {{--<p>Nihil officia ut sint molestiae tenetur.</p>--}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <strong>Email:</strong><i class="fa fa-asterisk"
                                                          style="color: #bd2130; font-size: 12px;"></i>{!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <strong>Mobile:</strong><i class="fa fa-asterisk"
                                                           style="color: #bd2130; font-size: 12px;"></i>
                                {{--{!! Form::number('mobile', null, array('placeholder' => 'mobile','class' => 'form-control mobile' )) !!}--}}
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <select class="form-control col-sm-4 mobileIsd" name="isd_code" data-live-search="true" required>
                                            @foreach($isd as $code)
                                                <option value="{{ $code->phonecode }}">+{{ $code->phonecode.' : '.$code->nicename }}</option>
                                            @endforeach
                                        </select>
                                        {!! Form::number('mobile', null, array('placeholder' => 'Mobile','class' => 'form-control col-sm-8 mobile' )) !!}
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <strong>(WhatsApp,IMO,Viber)#:</strong>
                               {{-- {!! Form::number('skype', null, array('placeholder' => 'mobile','class' => 'form-control skype' )) !!}--}}
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <select class="form-control col-sm-4 mobileIsd" name="skype_isd_code" data-live-search="true" required>
                                            @foreach($isd as $code)
                                                <option value="{{ $code->phonecode }}">+{{ $code->phonecode.' : '.$code->nicename }}</option>
                                            @endforeach
                                        </select>
                                        {!! Form::number('skype', null, array('placeholder' => 'WhatsApp,IMO,Viber','class' => 'form-control skype' )) !!}
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <strong>Facebook Name:</strong>
                                {!! Form::text('facebook', null, array('placeholder' => 'mobile','class' => 'form-control' )) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="section-header text-center" style="margin-bottom: 40px; padding-bottom: 2px;">
                                <h4 style="color: #0a6ebd">Course Information</h4>
                                {{--<p>Nihil officia ut sint molestiae tenetur.</p>--}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Branch:</strong><i class="fa fa-asterisk"
                                                           style="color: #bd2130; font-size: 12px;"></i>
                                {!! Form::select('branch', $branches,null, array('class' => 'form-control','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Course:</strong><i class="fa fa-asterisk"
                                                           style="color: #bd2130; font-size: 12px;"></i>
                                {{--{{ dd($branches, $userBranch) }}--}}
                                {!! Form::select('course_id', $courses, null, array('class' => 'form-control','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'course-select')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Batch:</strong>
                                {!! Form::select('batch', $icsabatch, null, array('class' => 'form-control','id' => 'batch-select')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Frequency:</strong><i class="fa fa-asterisk"
                                                              style="color: #bd2130; font-size: 12px;"></i>
                                {!! Form::select('frequency', [
                                '' => 'Select Frequency',
                                'Once a Week' => 'Once a Week',
                                'Twice a Week'=>'Twice a Week',
                                'Thrice a Week'=>'Thrice a Week',
                                'Once a Month'=>'Once a Month',
                                'Everyday'=>'Everyday',
                                ], null, array('class' => 'form-control','id' => 'gender-select')) !!}
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Timing:</strong><i class="fa fa-asterisk"
                                                           style="color: #bd2130; font-size: 12px;"></i>
                                {!! Form::text('time', null, array('placeholder' => 'Ex: 10AM-1PM','class' => 'form-control username')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="form-group">
                                <strong>Schedule:</strong>
                                {!! Form::select('schedule', [
                                        '' => 'Select Schedule',
                                        'Monday' => 'Monday',
                                        'Tuesday'=>'Tuesday',
                                        'Wednesday'=>'Wednesday',
                                        'Friday'=>'Friday',
                                        'Saturday'=>'Saturday',
                                        'Sunday'=>'Sunday',
                                        ], null, array('class' => 'form-control', 'multiple', 'id' => 'schedule-select', 'data-live-search="true"','data-style="btn-primary"',)) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

            {{--</div>--}}
            {!! Form::close() !!}
        </div>
        {{--</div>--}}
    </section><!-- End Contact Section -->

</main><!-- End #main -->
<div class="clearfix"></div>
<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-info" style="position: unset !important;">
                    <img class="img" src="{{ asset('Images/logo128.png') }}" alt="TheEvenet"> <span
                            style="font-weight: bold; color: #ffffff">Your Partnet In Success</span>
                    <p>In alias aperiam. Placeat tempore facere. Officiis voluptate ipsam vel eveniet est dolor et totam
                        porro. Perspiciatis ad omnis fugit molestiae recusandae possimus. Aut consectetur id quis. In
                        inventore consequatur ad voluptate cupiditate debitis accusamus repellat cumque.</p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Home</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Services</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Home</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Services</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Contact Us</h4>
                    <p>
                        A108 Adam Street <br>
                        New York, NY 535022<br>
                        United States <br>
                        <strong>Phone:</strong> +1 5589 55488 55<br>
                        <strong>Email:</strong> info@example.com<br>
                    </p>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            <strong>ICSA Your Partnet In Success <i class="fa fa-heart" style="color: red"></i></strong>
        </div>
        <div class="credits">
            Please Visit <a href="https://icsakuwait.com" target="_blank">ICSA Kuwait</a>
        </div>
    </div>
</footer><!-- End  Footer -->

<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<!-- Vendor JS Files -->
{{--<script src="{{ asset('theevent/vendor/jquery/jquery.min.js') }}"></script>--}}
<script src="{{ asset('assets/js/core/jquery.min.js') }} "></script>
<script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}"></script>
<script src="{{ asset('theevent/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('theevent/vendor/aos/aos.js') }}"></script>
<!-- Template Main JS File -->
<script src="{{ asset('theevent/js/main.js') }}"></script>
<script src="{{ asset('assets/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
<script>
    $(window).on('load', function () {
        $(".loader").fadeOut("slow");
    });
    $(document).ready(function () {
        $('#course-select').selectpicker();
        $('#branch-select').selectpicker();
        $('#schedule-select').selectpicker();
        $('#nationality-select').selectpicker();
        $('#currently_residing-select').selectpicker();

    })
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.mobileIsd').selectpicker();
        /* $('#sel_user').selectpicker();
         $('#sel_course').selectpicker();*/

        $('#branch-select').change(function () {
            $('#course-select').trigger('change');
        });

        $(document).delegate('#course-select', 'change', function (event) {
            var id = $(this).val();
            if (typeof id != "undefined" && id != '') {
                var branchid = $('#branch-select').val();
                var _token = $('input[name=_token]').val();
                $.ajax({
                    url: '{{ route("icsaregistration.getbatches") }}',
                    type: 'post',
                    data: {'courseid': id, 'branchid': branchid, _token: _token},
                    dataType: "json",
                    beforeSend: function () {
                        // Show image container
                        $(".loader").show();
                    },
                    success: function (data) {
                        $('#batch-select').html(data.icsabatch);
                    },
                    complete: function (data) {
                        // Hide image container
                        $(".loader").hide();
                    }
                });
            }
        })

        $(document).delegate('#currently_residing-select', 'change', function (event) {
            var countryid = $(this).val();
            if (typeof countryid != "undefined" && countryid != '') {
                var _token = $('input[name=_token]').val();
                $.ajax({
                    url: '{{ route("icsaregistration.getcountrycode") }}',
                    type: 'post',
                    data: {'countryid': countryid, _token: _token},
                    dataType: "json",
                    beforeSend: function () {
                        // Show image container
                        $(".loader").show();
                    },
                    success: function (data) {
                        /*$('.skype').val(data.country);
                        $('.mobile').val(data.country);*/

                        $('.mobileIsd').val(data.country);
                        $('.mobileIsd').selectpicker('refresh');

                    },
                    complete: function (data) {
                        // Hide image container
                        $(".loader").hide();
                    }
                });
            }
        })

    })
</script>
<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php  echo asset('Images/loader.gif') ?>') 50% 50% no-repeat rgb(249, 249, 249);
        opacity: .8;
    }
</style>
</body>

</html>