<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>ICSA Registration Success</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('Images/logo128.png') }}" rel="icon">
    <link href="{{ asset('Images/logo128.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('theevent/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('theevent/vendor/venobox/venobox.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('theevent/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('theevent/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('theevent/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('theevent/css/style.css') }}" rel="stylesheet">
</head>

<body>
<div class="loader"></div>
<!-- ======= Header ======= -->
<header id="header">
    <div class="container">
        <div id="logo" class="pull-left">
            <a href="#" class="scrollto" style="color: #ffffff"><img src="{{ asset('Images/logo.png')  }}" alt=""
                                                                     title=""> &nbsp;Your Partnet In Success</a>
        </div>
    </div>
</header><!-- End Header -->

<!-- ======= Intro Section ======= -->
<section id="intro" style="height: 50vh !important;">
    <div class="intro-container" data-aos="zoom-in" data-aos-delay="100">
        <h1 class="mb-4 pb-0">ICSA<br><span>PRE-REGISTRATION</span> FORM</h1>
        <p class="mb-4 pb-0">To register now, fill the form below and take a note/photo of the registration details.
            Just present your Registration Code to the Reception In-charge on the day of your enrollment.</p>
    </div>
</section><!-- End Intro Section -->

<main id="main">
    <section id="contact" class="section-bg">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">
                        <div class="card-header card-header-primary bg-success">
                            <div class="pull-center text-center">
                                <h4 class="card-title text-white">Success</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="" style="display: block;
  margin-left: auto;
  margin-right: auto;">
                                    <img class="img"
                                         src="{{ asset('Images/logo.png') }}" alt="" style="width: 150px">
                                </div>
                            </div>
                            <br>
                            <p class=""><b>Congratulations, you have successfully registered in ICSA. Please copy the following information or take a photo of it and present it to the reception during enrollment.</b></p>
                            <div class="row">
                                <div class="col-sm-4 col-md-4">Registration No.</div>
                                <div class="col-sm-8 col-md-8">
                                    <b>{{ $icsaregistration->registration_id }}</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-md-4">Name.</div>
                                <div class="col-sm-8 col-md-8">
                                    <b>{{ $icsaregistration->fullname }}</b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-md-4">Course Name.</div>
                                <div class="col-sm-8 col-md-8">
                                    <b>{{ $course[0]->name }}</b>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <a href="/icsaregistration" class="btn btn-primary">Back To Registration Form</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-info">
                    <img src="assets/img/logo.png" alt="TheEvenet">
                    <p>In alias aperiam. Placeat tempore facere. Officiis voluptate ipsam vel eveniet est dolor et totam
                        porro. Perspiciatis ad omnis fugit molestiae recusandae possimus. Aut consectetur id quis. In
                        inventore consequatur ad voluptate cupiditate debitis accusamus repellat cumque.</p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Home</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Services</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Home</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Services</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Contact Us</h4>
                    <p>
                        A108 Adam Street <br>
                        New York, NY 535022<br>
                        United States <br>
                        <strong>Phone:</strong> +1 5589 55488 55<br>
                        <strong>Email:</strong> info@example.com<br>
                    </p>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            <strong>ICSA Your Partnet In Success <i class="fa fa-heart" style="color: red"></i></strong>
        </div>
        <div class="credits">
            Please Visit <a href="https://icsakuwait.com" target="_blank">ICSA Kuwait</a>
        </div>
    </div>
</footer><!-- End  Footer -->

<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<!-- Vendor JS Files -->
{{--<script src="{{ asset('theevent/vendor/jquery/jquery.min.js') }}"></script>--}}
<script src="{{ asset('assets/js/core/jquery.min.js') }} "></script>
<script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/core/bootstrap-material-design.min.js') }}"></script>
<script src="{{ asset('theevent/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('theevent/vendor/aos/aos.js') }}"></script>
<!-- Template Main JS File -->
<script src="{{ asset('theevent/js/main.js') }}"></script>
</body>

</html>