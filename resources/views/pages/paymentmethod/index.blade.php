@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Payment Method Management</h4>
                                <p class="card-category"> All ICSA & CAP Payment Methods.</p>
                            </div>
                            <div class="pull-right">
                                @if(auth()->user()->canAccess('paymentmethod-create'))
                                    <a class="btn btn-success" href="{{ route('paymentmethod.create') }}"> Create New
                                        Branch</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table id="dtBasicExample" class="table table-striped table-bordered table-sm"
                                       cellspacing="0" width="100%">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($paymentmethod as $key => $method)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $method->name }}</td>
                                            <td>
                                                <span class="badge {{ $method->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($method->status) }}</span>
                                            </td>
                                            <td>
                                                <a class="btn btn-info btn-sm"
                                                   href="{{ route('paymentmethod.show',$method->id) }}"><i class="fa fa-eye"></i> </a>
                                                @if(auth()->user()->canAccess('paymentmethod-edit'))
                                                    <a class="btn btn-primary btn-sm"
                                                       href="{{ route('paymentmethod.edit',$method->id) }}"><i class="fa fa-pencil"></i> </a>
                                                @endif
                                                @if(auth()->user()->canAccess('paymentmethod-delete'))
                                                    {!! Form::open(['method' => 'DELETE','route' => ['paymentmethod.destroy', $method->id],'style'=>'display:inline', 'class' => 'delete-paymentmethod']) !!}
                                                    @csrf
                                                    {!! Form::submit($method->status == 'active' ? 'Disabled' : 'Active', ['class' => $method->status == 'active' ? 'btn btn-danger btn-sm' : 'btn btn-success btn-sm', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
    <style>
        #dtBasicExample_filter {
            float: right !important;
        }
    </style>
@stop