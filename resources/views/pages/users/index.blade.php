@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Users Management</h4>
                                <p class="card-category"> List Of all registered users</p>
                            </div>
                            <div class="pull-right">
                                @if(auth()->user()->canAccess('user-create'))
                                    <a class="btn btn-success" href="{{ route('users.create') }}"> Create New
                                        User</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table id="dtBasicExample" class="table">
                                    <thead class=" text-primary">
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Roles</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $key => $user)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td><span class="badge {{ $user->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($user->status) }}</span></td>
                                            <td>
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                        <label class="badge badge-success">{{ $v }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>

                                                <a title="View Details" class="btn btn-info btn-sm"
                                                   href="{{ route('users.show',$user->id) }}"><i class="fa fa-eye"></i>
                                                </a>
                                                @if(auth()->user()->canAccess('user-edit'))
                                                    <a title="Edit Detail" class="btn btn-primary btn-sm"
                                                       href="{{ route('users.edit',$user->id) }}"><i
                                                                class="fa fa-edit"></i></a>
                                                @endif
                                                @if(auth()->user()->canAccess('user-delete'))
                                                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline', 'class' => 'delete-user']) !!}
                                                    {!! Form::submit($user->status == 'active' ? 'Disabled' : 'Active', ['class' => $user->status == 'active' ? 'btn btn-danger  btn-sm' : 'btn btn-success  btn-sm', 'title' => 'Delete User', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                                @if(auth()->user()->canAccess('user-impersonate'))
                                                    @if(session('impersonated_by'))
                                                        @if(auth()->user()->name == $user->name)
                                                            @impersonating
                                                            <a title="Leave Impersonate" class="btn btn-dark btn-sm"
                                                               href="{{ route('impersonate.leave') }}"><i
                                                                        class="material-icons">keyboard_return</i></a>
                                                            @endImpersonating
                                                        @endif
                                                    @else
                                                        @canBeImpersonated($user)
                                                        <a title="Impersonate User" class="btn btn-warning btn-sm"
                                                           href="{{ route('impersonate', $user->id) }}"><i
                                                                    class="material-icons">keyboard_tab</i></a>
                                                        @endCanBeImpersonated
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
        /*$('.delete-user').submit(function (e) {
            e.preventDefault();

            if (confirm("Are you sure you want to delete?")) {
                $(this).submit();
            }
        });*/
    </script>
    <style>
        #dtBasicExample_filter {
            float: right !important;
        }
    </style>
@stop