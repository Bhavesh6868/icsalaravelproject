@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">

            {!! Form::model($user, ['method' => 'PATCH', ' enctype="multipart/form-data"', 'route' => ['users.update', $user->id]]) !!}
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title">Edit User {{ $user->userid }}</h4>
                                <p class="card-category">Change user Information Form.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('users.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>User Name:</strong>
                                        {!! Form::text('username', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Email:</strong>
                                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Mobile:</strong>
                                        {!! Form::number('mobile', null, array('placeholder' => 'mobile','class' => 'form-control' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Birth Date:</strong>
                                        {!! Form::date('birthdate', null, array('placeholder' => 'Birth date','class' => 'form-control' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Civil ID:</strong>
                                        {!! Form::text('civil_id', null, array('placeholder' => 'Civil ID','class' => 'form-control' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Position:</strong>
                                        {!! Form::text('position', null, array('placeholder' => 'Position','class' => 'form-control' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Gender:</strong>
                                        {!! Form::select('gender', ['' => 'select Gender', 'male' => 'Male', 'female'=>'Female'], old('gender', $user->gender), array('class' => 'form-control','id' => 'gender-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Nationality:</strong>
                                        {!! Form::text('nationality', null, array('placeholder' => 'Nationality','class' => 'form-control' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <strong>Password:</strong>
                                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <strong>Confirm Password:</strong>
                                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>


                            <div id="addRow" class="addRow">
                                @if(count($userBranch) > 0)
                                    @php
                                        $kkk = 0;
                                        //$totalSale = 0
                                    @endphp
                                    @foreach($userBranch as $k => $v)
                                        @php
                                            $inc = $kkk++;
                                        @endphp
                                        {{--{{ $k  }}--}}
                                        {{--{!! Form::select('roles['.$kkk++.'][]', $roles, $k, array('class' => 'role-select form-control', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'role-select')) !!}--}}
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label for="">Select Role</label>
                                                {!! Form::select('roles['.($inc).'][]', $roles, $k, array('class' => 'role-select form-control', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'role-select')) !!}
                                            </div>
                                            <div class="col-md-5">
                                                <label for="">Select Branches</label>
                                                {!! Form::select('branch['.($inc).'][]', $branches,$v, array('class' => 'branch-select form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
                                            </div>
                                            <div class="col-md-2" style="margin-top:26px;">
                                                @if($inc == 0)
                                                    <button type="button" id="addMore" class="btn btn-success btn-sm"><i
                                                                class="fa fa-plus"></i></button>
                                                @else
                                                    <button type="button" class="btn btn-danger btn-sm removeaddmore"><i
                                                                class="fa fa-trash-o"></i></button>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="">Select Role</label>
                                            {!! Form::select('roles[0][]', $roles, null, array('class' => 'role-select form-control', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'role-select')) !!}
                                        </div>
                                        <div class="col-md-5">
                                            <label for="">Select Branches</label>
                                            {!! Form::select('branch[0][]', $branches,null, array('class' => 'branch-select form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
                                        </div>
                                        <div class="col-md-2" style="margin-top:26px;">
                                            <button type="button" id="addMore" class="btn btn-success btn-sm"><i
                                                        class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                               {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Role:</strong>
                                        {!! Form::select('rolesss[]', $roles,$userRole, array('class' => 'role-select form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'role-select')) !!}
                                    </div>
                                </div>--}}
                                {{--<div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <strong>Branch:</strong>
                                        {!! Form::select('branch[]', $branches,$userBranch, array('class' => 'form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
                                    </div>
                                </div>--}}
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Address:</strong>
                                        {!! Form::textarea('address', null, array('placeholder' => 'Address','class' => 'form-control', 'rows' => "4" )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Want to create a Instructor?:</strong>
                                        {!! Form::select('isInstructor', ['No' => 'No', 'Yes'=>'Yes'], old('gender', $user->isInstructor), array('class' => 'form-control')) !!}
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="row">
                            <div class="col-sm-12" style="margin-top: 1rem;">
                                <label class="cabinet pull-center">
                                    <figure>
                                        <img src="{{ old('avatar', $user->avatar) }}"
                                             class="gambar img-responsive img-thumbnail"
                                             style="width: 200px; height: 250px;" id="item-img-output"/>
                                        <figcaption style="width: 35%"><i
                                                    class="pull-center material-icons">camera_alt</i>
                                            <input accept="image/png, image/jpeg" type="file"
                                                   class="item-img file center-block" name="avatar" style="width: 15%"/>
                                        </figcaption>
                                    </figure>
                                </label>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                            <h4 class="card-title">Alec Thompson</h4>
                            <p class="card-description">
                                Don't be scared of the truth because we need to restart the human foundation in truth
                                And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                            </p>
                            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/croppie.css') }}">
    <script type="text/javascript" src="{{ asset('assets/js/croppie.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.6/handlebars.min.js"></script>
    <script id="document-template" type="text/x-handlebars-template">
        <div class="row delete_add_more_item" id="delete_add_more_item">
            <div class="col-md-5">
                <label for="">Select Role</label>
                {!! Form::select('roles[][]', $roles, null, array('class' => 'role-select form-control', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'role-select')) !!}
            </div>
            <div class="col-md-5">
                <label for="">Select Branch</label>
                {!! Form::select('branch[][]', $branches,null, array('class' => 'branch-select form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
            </div>
            <div class="col-md-2" style="margin-top:26px;">
                <button type="button" class="btn btn-danger btn-sm removeaddmore"><i class="fa fa-trash-o"></i></button>
                {{--<i class="removeaddmore fa fa-trash-o" style="cursor:pointer;color:red;">Remove</i>--}}
            </div>
        </div>
    </script>
    <script type="text/javascript">
        var kk = (typeof '{{ count($userBranch) }}' != 'undefined' && '{{ count($userBranch) }}' != '') ? '{{ count($userBranch)-1 }}' : 0;

        //alert('{{ count($userBranch) }}')
        $(document).on('click', '#addMore', function () {
            kk++;

            $('.table').show();

            var task_name = $("#task_name").val();
            var cost = $("#cost").val();

            var source = '<div class="row delete_add_more_item" id="delete_add_more_item"><div class="col-md-5"><label for="">Select Role</label><select data-style="btn-primary" data-live-search="true" name=roles[' + kk + '][] multiple="multiple" class="role-select form-control">@foreach($roles as $role) <option value="{{$role}}">{{$role}}</option> @endforeach</select></div><div class="col-md-5"><label for="">Select Branch</label><select data-style="btn-primary" data-live-search="true" name=branch[' + kk + '][] multiple="multiple" class="branch-select form-control">@foreach($branches as $branchId => $branchName) <option value="{{$branchId}}">{{$branchName}}</option> @endforeach</select></div><div class="col-md-2" style="margin-top:26px;"><button type="button" class="btn btn-danger btn-sm removeaddmore"><i class="fa fa-trash-o"></i></button></div></div>';

            var template = Handlebars.compile(source);

            var data = {
                task_name: task_name,
                cost: cost
            }

            var html = template(data);
            $("#addRow").append(html)
            $('.role-select').selectpicker();
            $('.branch-select').selectpicker();
            //    total_ammount_price();
        });

        $(document).on('click', '.removeaddmore', function (event) {
            $(this).closest('.delete_add_more_item').remove();
            total_ammount_price();
        });
        $(document).ready(function () {
            $('.role-select').selectpicker();
            $('.branch-select').selectpicker();

            // Start upload preview image
            //$(".gambar").attr("src", "https://user.gadjian.com/static/images/personnel_boy.png");
            var $uploadCrop,
                tempFilename,
                rawImg,
                imageId;

            function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.upload-demo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawImg = e.target.result;
                    }
                    reader.readAsDataURL(input.files[0]);
                }
                else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
            }

            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 200,
                    height: 250,
                },
                enforceBoundary: false,
                enableExif: true
            });
            $('#cropImagePop').on('shown.bs.modal', function () {
                // alert('Shown pop');
                $uploadCrop.croppie('bind', {
                    url: rawImg
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            });

            $('.item-img').on('change', function () {
                imageId = $(this).data('id');
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                readFile(this);
            });
            $('#cropImageBtn').on('click', function (ev) {
                /*$uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 150, height: 200}
                }).then(function (resp) {
                    $('#item-img-output').attr('src', resp);
                    $('#cropImagePop').modal('hide');
                });*/

                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (response) {
                    var _token = $('input[name=_token]').val();
                    $.ajax({
                        url: '{{ route("image_crop.upload") }}',
                        type: 'post',
                        data: {"image": response, _token: _token, 'username': '<?php echo $user->username ?>'},
                        dataType: "json",
                        success: function (data) {
                            //var crop_image = '<img src="'+data.path+'" />';
                            //$('#uploaded_image').html(crop_image);
                            $('#item-img-output').attr('src', data.path);
                            $('#cropImagePop').modal('hide');
                        }
                    });
                });
            });
            // End upload preview image
        })
    </script>

    <style>
        figcaption {
            display: block;
            cursor: pointer;
        }

        figcaption input.file {
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top: -30px;
        }

        #upload-demo {
            width: 350px;
            height: 350px;
            padding-bottom: 25px;
        }

        figcaption {
            position: absolute;
            bottom: 10%;
            left: 17%;
            color: #fff;
            width: 100%;
            /*padding-left: 9px;
            padding-bottom: 5px;*/
            text-shadow: 0 0 10px #000;
        }
    </style>
@stop