@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Generate Course Ads</h4>
                                <p class="card-category"> Course Ads Templated.</p>
                            </div>
                            <div class="pull-right">
                                @if(auth()->user()->canAccess('adscourse-create'))
                                    <a class="btn btn-success" href="{{ route('adscourse.create') }}"><i
                                                class="fa fa-plus"></i>&nbsp;&nbsp; Create New Ads Course</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table id="dtBasicExample" class="table table-striped table-bordered table-sm"
                                       cellspacing="0" width="100%">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($adscourses as $key => $adscourse)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $adscourse->icsacourse->name }}</td>
                                            <td>
                                                <span class="badge {{ $adscourse->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($adscourse->status) }}</span>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown">Action<span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a class="" href="{{ route('adscourse.show',$adscourse->id) }}"><i class="fa fa-eye"></i>&nbsp;Show Template</a></li>
                                                        @if(auth()->user()->canAccess('adscourse-edit'))
                                                            <li><a class="" href="{{ route('adscourse.edit',$adscourse->id) }}"><i class="fa fa-pencil"></i>&nbsp; Edit Template </a></li>
                                                        @endif
                                                        @if(auth()->user()->canAccess('adscourse-delete'))
                                                            <li>
                                                                <a href="#"><i class="fa fa-crosshairs"></i> &nbsp;
                                                                {!! Form::open(['method' => 'DELETE','route' => ['adscourse.destroy', $adscourse->id],'style'=>'display:inline', 'class' => 'delete-source']) !!}
                                                                @csrf
                                                                {!! Form::submit($adscourse->status == 'active' ? 'Make Disabled' : 'Make Active', ['class' => $adscourse->status == 'active' ? 'btn btn-outline-danger btn-sm customBtn' : 'btn btn-outline-success btn-sm customBtn', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                                {!! Form::close() !!}
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <style>
        .customBtn {
            padding: 0 !important;
            box-shadow: none;
            border: 0px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
    <style>
        #dtBasicExample_filter {
            float: right !important;
        }
    </style>
@stop