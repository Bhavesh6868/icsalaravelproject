@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <h4 class="card-title ">ICSA Student List</h4>
                                        <p class="card-category"> All ICSA Students.</p>
                                    </div>
                                    <div class="pull-right">
                                        @if(auth()->user()->canAccess('icsastudent-create'))
                                            <a style="padding: .54rem 2.14rem; margin-top: 0" class="btn btn-success" href="{{ route('icsastudents.create') }}"> Enroll New Student</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('route' => 'icsastudents.searchenroll', 'method'=>'POST')) !!}
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="">
                                        {!! Form::select('course_id', $courses, ($course_id ? $course_id : null), array('class' => 'form-control course_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'course-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6" style="padding-left: 0">
                                    <div class="">
                                        {!! Form::select('instructor_id', $instructor, ($instructor_id ? $instructor_id : null), array('class' => 'form-control instructor_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'instructor-select')) !!}
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-5">
                                    <div class="">
                                        {!! Form::date('start_date', $start_date, array('placeholder' => 'Name','class' => 'form-control fullname')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-5" style="padding-left: 0">
                                    <div class="">
                                        {!! Form::date('end_date', $end_date, array('placeholder' => 'Birth date','class ' => 'form-control birthdate' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-1" style="padding-left: 0">
                                    <div class="">
                                    <button type="submit" style="padding: .54rem 2.14rem; margin-top: 0; width: 100%" class="btn btn-primary"><i class="fa fa-search"></i> </button>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-1" style="padding-left: 0">
                                    <div class="">
                                        <a style="padding: .54rem 2.14rem; margin-top: 0; width: 100%" class="btn btn-warning" href="{{ route('icsastudents.index') }}"><i class="fa fa-trash-o"></i> </a>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>
                        <div class="card-body p-0">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="col-lg-12 col-md-12 p-0">
                                <div class="card"  style="z-index: 1 !important;">
                                    <div class="card-header card-header-tabs card-header-primary">
                                        <div class="nav-tabs-navigation">
                                            <div class="nav-tabs-wrapper">
                                                <span class="nav-tabs-title">Branch:</span>
                                                {{-- @foreach(auth()->user()->rolebranches as $branch)
                                                     <a class="dropdown-item {{auth()->user()->currentBranch == $branch->branch->id ? 'active' : ''}}" href="{{ route('users.changeBranch',$branch->branch->id) }}">{{ $branch->branch->name }}</a>
                                                 @endforeach--}}
                                                <ul class="nav nav-tabs" data-tabs="tabs">
                                                    {{--@if(auth()->user()->currentBranch != 3)--}}
                                                    @foreach(auth()->user()->rolebranches as $branch)
                                                        @if(auth()->user()->currentBranch != 3 && auth()->user()->currentBranch == $branch->branch->id)
                                                            <li class="nav-item">
                                                                <a class="nav-link {{auth()->user()->currentBranch == $branch->branch->id ? 'active' : ''}}"
                                                                   href="#{{ $branch->branch->name }}"
                                                                   data-toggle="tab">
                                                                    {{ $branch->branch->name }}
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                        @elseif(auth()->user()->currentBranch == 3 && $branch->branch->id != 3)
                                                            <li class="nav-item">
                                                                <a class="nav-link {{$branch->branch->id == 1 ? 'active' : ''}}"
                                                                   href="#{{ $branch->branch->name }}"
                                                                   data-toggle="tab">
                                                                    {{ $branch->branch->name }}
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content">
                                            @foreach(auth()->user()->rolebranches as $branch)
                                                @if(auth()->user()->currentBranch != 3 && auth()->user()->currentBranch == $branch->branch->id)
                                                    <div class="tab-pane  {{auth()->user()->currentBranch == $branch->branch->id ? 'active' : ''}}"
                                                         id="{{ $branch->branch->name }}">
                                                        <div class="table-responsive">
                                                            <table id="{{ $branch->branch->id }}"
                                                                   class="table table-condensed table-hover table-bordered table-striped table-hideable"
                                                                   cellspacing="0" width="100%">
                                                                <thead class=" text-primary">
                                                                <tr class="footer-restore-columns">
                                                                    <th colspan="8"><a class="restore-columns" href="#">Some columns hidden - click to show all</a></th>
                                                                </tr>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Enrollment</th>
                                                                    <th>Name</th>
                                                                    <th class="hide-col">Mobile<button class="pull-right btn btn-primary btn-condensed hide-column btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hide Column">
                                                                            <i class="fa fa-eye-slash"></i>
                                                                        </button></th>
                                                                    <th>Course</th>
                                                                    <th class="hide-col">Instructor<button class="pull-right btn btn-primary btn-condensed hide-column btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hide Column"><i class="fa fa-eye-slash"></i></button></th>
                                                                    <th>E-Date</th>
                                                                    <th class="hide-col">Nationality<button class="pull-right btn btn-primary btn-condensed hide-column btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hide Column">
                                                                            <i class="fa fa-eye-slash"></i>
                                                                        </button></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach ($icsastudents as $key => $icsastudent)
                                                                    <tr>
                                                                        <td>{{ ++$i }}</td>
                                                                        <td>
                                                                            <a href="{{ route('icsastudents.show',$icsastudent->id) }}">{{ $icsastudent->enrollment_id}}</a>
                                                                        </td>
                                                                        <td>{{ $icsastudent->icsastudentdetails->fullname}}</td>
                                                                        <td>{{ $icsastudent->icsastudentdetails->mobile }}</td>
                                                                        <td>{{ $icsastudent->icsacourse->name }}</td>
                                                                        <td>{{ $icsastudent->instructor->name }}</td>
                                                                        <td>{{ $icsastudent->enrollment_date }}</td>
                                                                        <td>{{ $icsastudent->icsastudentdetails->countries->name }}</td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                @elseif(auth()->user()->currentBranch == 3 && $branch->branch->id != 3)
                                                    <div class="tab-pane {{$branch->branch->id == 1 ? 'active' : ''}}"
                                                         id="{{ $branch->branch->name }}">
                                                        <div class="table-responsive">
                                                            <table id="{{ $branch->branch->id }}"
                                                                   class="table table-condensed table-hover table-bordered table-striped table-hideable"
                                                                   cellspacing="0" width="100%">
                                                                <thead class=" text-primary">
                                                                <tr class="footer-restore-columns">
                                                                    <th colspan="8"><a class="restore-columns" href="#">Some columns hidden - click to show all</a></th>
                                                                </tr>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Enrollment</th>
                                                                    <th>Name</th>
                                                                    <th class="hide-col">Mobile<button class="pull-right btn btn-primary btn-condensed hide-column btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hide Column">
                                                                            <i class="fa fa-eye-slash"></i>
                                                                        </button></th>
                                                                    <th>Course</th>
                                                                    <th class="hide-col">Instructor<button class="pull-right btn btn-primary btn-condensed hide-column btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hide Column">
                                                                            <i class="fa fa-eye-slash"></i>
                                                                        </button></th>
                                                                    <th>E-Date</th>
                                                                    <th class="hide-col">Nationality<button class="pull-right btn btn-primary btn-condensed hide-column btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hide Column">
                                                                            <i class="fa fa-eye-slash"></i>
                                                                        </button></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @php
                                                                    $k = 0
                                                                @endphp
                                                                @foreach ($icsastudents as $key => $icsastudent)
                                                                    @if(substr($icsastudent->enrollment_id, 0, 1) == $branch->branch->id)
                                                                        <tr>
                                                                            <td>{{ ++$k }}</td>
                                                                            <td>
                                                                                <a href="{{ route('icsastudents.show',$icsastudent->id) }}">{{ $icsastudent->enrollment_id}}</a>
                                                                            </td>
                                                                            <td>{{ $icsastudent->icsastudentdetails->fullname}}</td>
                                                                            <td>{{ $icsastudent->icsastudentdetails->mobile }}</td>
                                                                            <td>{{ $icsastudent->icsacourse->name }}</td>
                                                                            <td>{{ $icsastudent->instructor->name }}</td>
                                                                            <td>{{ $icsastudent->enrollment_date }}</td>
                                                                            <td>{{ $icsastudent->icsastudentdetails->countries->name }}</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="table-responsive">
                                <table id="dtBasicExample" class="table table-striped table-bordered table-sm"
                                       cellspacing="0" width="100%">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Enrollment</th>
                                        <th>Name</th>
                                        <th>Course</th>
                                        <th>Mobile</th>
                                        <th>Instructor</th>
                                        <th>E-Date</th>
                                        <th>Nationality</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($icsastudents as $key => $icsastudent)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>
                                                <a href="{{ route('icsastudents.show',$icsastudent->id) }}">{{ $icsastudent->enrollment_id}}</a>
                                            </td>
                                            <td>{{ $icsastudent->icsastudentdetails->fullname}}</td>
                                            <td>{{ $icsastudent->icsastudentdetails->mobile }}</td>
                                            <td>{{ $icsastudent->icsacourse->name }}</td>
                                            <td>{{ $icsastudent->instructor->name }}</td>
                                            <td>{{ $icsastudent->enrollment_date }}</td>
                                            <td>{{ $icsastudent->icsastudentdetails->countries->name }}</td>
                                            --}}{{--<td>
                                                <a class="btn btn-info"
                                                   href="{{ route('icsastudents.show',$icsastudent->id) }}">Show</a>
                                                @if(auth()->user()->canAccess('$icsastudent-edit'))
                                                    <a class="btn btn-primary"
                                                       href="{{ route('icsastudents.edit',$icsastudent->id) }}">Edit</a>
                                                @endif
                                                @if(auth()->user()->canAccess('$icsastudent-delete'))
                                                    {!! Form::open(['method' => 'DELETE','route' => ['icsastudents.destroy', $icsastudent->id],'style'=>'display:inline', 'class' => 'delete-icsastudent']) !!}
                                                    @csrf
                                                    {!! Form::submit($icsastudent->status == 'active' ? 'Disabled' : 'Active', ['class' => $icsastudent->status == 'active' ? 'btn btn-danger' : 'btn btn-success', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            </td>--}}{{--
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#course-select').selectpicker();
            $('#instructor-select').selectpicker();
            $('#1, #2, #3, #4, #5, #6, #7, #8').DataTable({
               /* "columnDefs": [
                    { "orderable": false, "targets": 4 }
                ]*/
                "bSort" : false
            });
            //$('.dataTables_length').addClass('bs-select');
        });
        $(function() {
            // on init
            $(".table-hideable .hide-col").each(HideColumnIndex);

            // on click
            $('.hide-column').click(HideColumnIndex)

            function HideColumnIndex() {
                var $el = $(this);
                var $cell = $el.closest('th,td')
                var $table = $cell.closest('table')

                // get cell location - https://stackoverflow.com/a/4999018/1366033
                var colIndex = $cell[0].cellIndex + 1;

                // find and hide col index

                $table.find("tbody tr, thead tr")
                    .children(":nth-child(" + colIndex + ")")
                    .addClass('hide-col');
                $('.hide-col').hide();
                // show restore footer
                $table.find(".footer-restore-columns").show()
            }

            // restore columns footer
            $(".restore-columns").click(function(e) {
                $('.hide-col').show();

                var $table = $(this).closest('table')
                $table.find(".footer-restore-columns").hide()
                $table.find("th, td")
                    .removeClass('hide-col');

            })
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            })

        })
    </script>
    <style>
        .table-hideable td,
        .table-hideable th {
            width: auto;
            transition: width .5s, margin .5s;
        }

        .btn-condensed.btn-condensed {
            padding: 0 5px;
            box-shadow: none;
        }

        /*.hide-col {
            width: 0px !important;
            height: 0px !important;
            display: block !important;
            overflow: hidden !important;
            margin: 0 !important;
            padding: 0 !important;
            border: none !important;
        }*/
        /*.dataTables_filter {
            float: right !important;
        }*/
    </style>
@stop