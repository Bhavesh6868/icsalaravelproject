@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Student Information</h4>
                                <p class="card-category"> All Information</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="javascript:history.back()"> Back</a>
                                {{--{{ url()->previous()  }}
                                {{ redirect()->back()->getTargetUrl()  }}
                                {{ redirect()->back()->getTargetUrl()  }}--}}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 style="background: #9B34B2; color: white;" class="text-left  p-1">Personal Details</h4>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3 text-center">
                                    <img height="220" width="220" src="{{ $icsastudent->icsastudentdetails->avatar }}"
                                         class="img-circle img-responsive" alt="Placeholder image">

                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Enrollment No:</strong>
                                                {{ $icsastudent->enrollment_id }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Civil Id:</strong>
                                                {{ $icsastudent->civil_id }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Birth Date:</strong>
                                                {{ date('d M Y' , strtotime($icsastudent->icsastudentdetails->birthdate)) }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Name:</strong>
                                                {{ $icsastudent->icsastudentdetails->fullname }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <div class="form-group">
                                                <strong>Course Name:</strong>
                                                {{ $icsastudent->icsacourse->name }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Gender:</strong>
                                                {{ $icsastudent->icsastudentdetails->gender }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Email:</strong>
                                                {{ $icsastudent->icsastudentdetails->email }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Mobile:</strong>
                                                {{ $icsastudent->icsastudentdetails->mobile }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Occupation:</strong>
                                                {{ $icsastudent->icsastudentdetails->occupation }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Facebook Name:</strong>
                                                {{ $icsastudent->icsastudentdetails->facebook }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Set Hours:</strong>
                                                {{ $icsastudent->setHours }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Frequency:</strong>
                                                {{ $icsastudent->frequency }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Schedule:</strong>
                                                {{ $icsastudent->schedule }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Timing:</strong>
                                                {{ $icsastudent->time }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Nationality:</strong>
                                                {{ $icsastudent->icsastudentdetails->countries->name }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <strong>Currently Residing:</strong>
                                                {{ $icsastudent->icsastudentdetails->currentlyresiding->name }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-12">
                                    <h4 style="background: #9B34B2; color: white;" class="text-left  p-1">Joining & Enrollment Date</h4>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <strong>Join Date:</strong>
                                                {{ date('d M y', strtotime($icsastudent->join_date)) }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <strong>Enrollment Date:</strong>
                                                {{ date('d M y h:i:s A', strtotime($icsastudent->enrollment_date)) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="font-size: 16px">
                                <div class="col-md-12 text-left">
                                    <h4 style="background: #9B34B2; color: white;"
                                        class="text-left border-bottom p-1">Payment Details</h4>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Course Fees:</strong>
                                        {{ $icsastudent->icsacourse->course_fee }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Paid Amount:</strong>
                                        {{ $paidAmt }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Discount Amount:</strong>
                                        {{ $discountAmt }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Balance Amount:</strong>
                                        {{ $icsastudent->icsacourse->course_fee - ($paidAmt+$discountAmt) }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Additional Amount:</strong>
                                        {{ $additionalAmt }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Total Paid Includind Additional:</strong>
                                        {{ $paidAmt+ $additionalAmt }}
                                    </div>
                                </div>
                                @if($refundAmtSum > 0)
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <strong>Refund Amount:</strong>
                                            {{ $refundAmtSum }}
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if(count($icsastudent->icsapaymentrefund)>0)
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive" style="font-weight: bold">
                                            <h4 style="background: #9B34B2; color: white;" class="text-left  p-1">
                                                Refund Details</h4>
                                            <table class="table table-striped table-bordered table-sm"
                                                   cellspacing="0" width="100%">
                                                <thead>
                                                <tr style="background: #eec6f7; font-weight: bold">
                                                    <td>#</td>
                                                    <td>Amount</td>
                                                    <td>Reason</td>
                                                    <td>Date</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($icsastudent->icsapaymentrefund as $key => $refund)
                                                    <tr>
                                                        <td>{{ ++$key }}</td>
                                                        <td>{{ $refund->amount }}</td>
                                                        <td>{{ $refund->reason }}</td>
                                                        <td>{{ date('Y-m-d h:i A', strtotime($refund->date_refunded))}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 style="background: #9B34B2; color: white;"
                                        class="text-left border-bottom p-1">Payment Details</h4>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-sm"
                                               cellspacing="0" width="100%">
                                            <tr style="background: #eec6f7; font-weight: bold">
                                                <td>#</td>
                                                <td>Pay Mehtod</td>
                                                <td>Date</td>
                                                <td>Amount</td>
                                                <td>Branch</td>
                                                <td>Process By</td>
                                                <td>Note</td>
                                            </tr>
                                            <tr>
                                            @if( count($icsastudent->icsapaymentdetail) > 0 )
                                                @foreach($icsastudent->icsapaymentdetail as $key => $paydetail)
                                                    <tr>
                                                        <td>{{ ++$key }}</td>
                                                        <td>{{ $paydetail->pay_detail }}</td>
                                                        <td>{{ $paydetail->date_paid }}</td>
                                                        <td>{{ $paydetail->amount }}</td>
                                                        <td>{{ $paydetail->paymentmethods->name }}</td>
                                                        <td>{{ ucfirst($paydetail->process_by) }}</td>
                                                        <td>{{ ucfirst($paydetail->transfer_note) }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7">No Date</td>
                                                </tr>
                                                @endif
                                                </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop