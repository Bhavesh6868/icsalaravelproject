@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-body pull-center bg-white">
                        {!! Form::open(array('route' => 'icsastudents.getenrolldetails', 'method'=>'POST')) !!}
                        <div class="row pull-center">
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center errormessage d-none" >
                                <div class="alert alert-danger">
                                    Sorry Information Not Found
                                </div>
                            </div>
                        </div>
                        <div class="row pull-center">
                            <div class="col-xs-12 col-sm-12 col-md-2 text-center">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 pull-center">
                                <div class="form-group row">
                                    {!! Form::number('enrollment_id', $icsastudent ? old('enrollment_id', $icsastudent->enrollment_id) : Session::get('enroll'), array('placeholder' => 'Enter Enrollment Number','class' => 'form-control registration_id')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 pull-center">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-md " style="margin: 0">Load</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            {!! Form::model($icsastudent ? $icsastudent : null, ['enctype' => "multipart/form-data", 'method' => 'PATCH','route' => ['icsastudents.update', $icsastudent ? $icsastudent->id : 1]]) !!}
            {{--{!! Form::model($icsastudent, ['method' => 'PATCH','route' => ['icsastudents.update', $icsastudent->id], ' enctype="multipart/form-data"']) !!}--}}
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit Student Details</h4>
                                <p class="card-category"> Please Enter all details</p>
                            </div>
                            {{--<div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('users.index') }}"> Back</a>
                            </div>--}}
                        </div>
                        <div class="card-body">
                            {{--<div class="row">
                                <div class="col-lg-12 margin-tb">
                                    <div class="pull-left">
                                        <h2>Create New User</h2>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
                                    </div>
                                </div>
                            </div>--}}


                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {{--@if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif--}}

                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Name:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::text('fullname', $icsastudent ? $icsastudent->icsastudentdetails->fullname : null, array('placeholder' => 'Name','class' => 'form-control fullname')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Birth Date:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::date('birthdate', $icsastudent ? $icsastudent->icsastudentdetails->birthdate : null, array('placeholder' => 'Birth date','class ' => 'form-control birthdate' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Civil ID:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::text('civil_id', null, array('placeholder' => 'Civil ID','class' => 'form-control civil_id' ,'readonly' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Gender:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::select('gender', ['' => 'select Gender', 'male' => 'Male', 'female'=>'Female'], $icsastudent ? $icsastudent->icsastudentdetails->gender : null, array('class' => 'form-control gender','id' => 'gender-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Nationality:</strong><i class="fa fa-asterisk"
                                                                        style="color: #bd2130; font-size: 12px;"></i>
                                        {{--{!! Form::text('nationality', null, array('placeholder' => 'Nationality','class' => 'form-control' )) !!}--}}
                                        {!! Form::select('nationality', $nationality,$icsastudent ? $icsastudent->icsastudentdetails->nationality : null, array('class' => 'form-control nationality','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'nationality-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Currently Residing:</strong>
                                        <i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {{--{!! Form::text('currently_residing', null, array('placeholder' => 'Currently Residing','class' => 'form-control username')) !!}--}}
                                        {!! Form::select('currently_residing', $nationality,$icsastudent ? $icsastudent->icsastudentdetails->currently_residing : null, array('class' => 'form-control currently_residing','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'currently_residing-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Address:</strong>
                                        {!! Form::textarea('address', $icsastudent ? $icsastudent->icsastudentdetails->address : null, array('placeholder' => 'Address','class' => 'form-control address', 'rows' => "2" )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Occupation:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::text('occupation', $icsastudent ? $icsastudent->icsastudentdetails->occupation : null, array('placeholder' => 'Position','class' => 'form-control occupation', 'autocomplete' => 'off' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Email:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::text('email', $icsastudent ? $icsastudent->icsastudentdetails->email : null, array('placeholder' => 'Email','class' => 'form-control email')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Mobile:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {{--{!! Form::number('mobile', $icsastudent ? $icsastudent->icsastudentdetails->mobile : null, array('placeholder' => 'mobile','class' => 'form-control mobile' )) !!}--}}
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <select class="form-control col-sm-4 mobileIsd" name="isd_code" data-live-search="true" required>
                                                    @foreach($isd as $code)
                                                        <option value="{{ $code->phonecode }}"  {{ ($icsastudent) ? ( $icsastudent->icsastudentdetails->isd_code == $code->phonecode ? 'selected' : '') : null }}>+{{ $code->phonecode.' : '.$code->nicename }}</option>
                                                    @endforeach
                                                </select>
                                                {!! Form::number('mobile', $icsastudent ? $icsastudent->icsastudentdetails->mobile : null, array('placeholder' => 'mobile','class' => 'form-control mobile' )) !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>(WhatsApp,IMO,Viber)#:</strong>
                                        {{--{!! Form::number('skype', $icsastudent ? $icsastudent->icsastudentdetails->skype : null, array('placeholder' => 'mobile','class' => 'form-control skype' )) !!}--}}
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <select class="form-control col-sm-4 mobileIsd" name="skype_isd_code" data-live-search="true" required>
                                                    @foreach($isd as $code)
                                                        <option value="{{ $code->phonecode }}" {{ ($icsastudent) ? ($icsastudent->icsastudentdetails->skype_isd_code == $code->phonecode ? 'selected' : '') : null }}>+{{ $code->phonecode.' : '.$code->nicename }}</option>
                                                    @endforeach
                                                </select>
                                                {!! Form::number('skype', $icsastudent ? $icsastudent->icsastudentdetails->skype : null, array('placeholder' => 'mobile','class' => 'form-control skype' )) !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Facebook Name:</strong>
                                        {!! Form::text('facebook', $icsastudent ? $icsastudent->icsastudentdetails->facebook : null, array('placeholder' => 'mobile','class' => 'form-control facebook' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Join Date:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::date('join_date', $icsastudent ? date('Y-m-d', strtotime($icsastudent->join_date)) : null, array('placeholder' => 'Join Date','class ' => 'form-control join_date' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Course:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {{--{{ dd($branches, $userBranch) }}--}}
                                        {!! Form::select('course_id', $courses, null, array('class' => 'form-control course_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'course-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Batch:</strong>
                                        {!! Form::select('batch_id', $icsabatch,null, array('class' => 'form-control batch_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'batch-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Instructor:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::select('instructor_id', $instructor,null, array('class' => 'form-control instructor_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'instructor-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Frequency:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::select('frequency', [
                                        '' => 'Select Frequency',
                                        'Once a Week' => 'Once a Week',
                                        'Twice a Week'=>'Twice a Week',
                                        'Thrice a Week'=>'Thrice a Week',
                                        'Once a Month'=>'Once a Month',
                                        'Everyday'=>'Everyday',
                                        ], null, array('class' => 'form-control frequency','id' => 'gender-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Schedule:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::select('schedule[]', [
                                        '' => 'Select Schedule',
                                        'Monday' => 'Monday',
                                        'Tuesday'=>'Tuesday',
                                        'Wednesday'=>'Wednesday',
                                        'Friday'=>'Friday',
                                        'Saturday'=>'Saturday',
                                        'Sunday'=>'Sunday',
                                        ], $icsastudent ? explode(',', $icsastudent->schedule) : [], array('class' => 'form-control schedule', 'multiple', 'id' => 'schedule-select', 'data-live-search="true"','data-style="btn-primary"',)) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Timing:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::text('time', null, array('placeholder' => 'Ex: 10AM-1PM','class' => 'form-control time')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Source:</strong><i class="fa fa-asterisk" style="color: #bd2130; font-size: 12px;"></i>
                                        {!! Form::select('source_id', $source,null, array('class' => 'form-control source_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'instructor-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Set Hours:</strong>
                                        {!! Form::text('setHours', null, array('placeholder' => 'Set Learning Hours','class' => 'form-control setHours')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Special Note:</strong>
                                        {!! Form::text('note', null, array('placeholder' => 'Note','class' => 'form-control note')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary" {{ ($icsastudent) ? '' : 'disabled' }}>Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-profile">
                        {{--<div class="card-avatar">
                            <a href="javascript:;">
                                <img class="img" src="../../assets/img/faces/marc.jpg"/>
                            </a>
                        </div>--}}
                        <div class="row">
                            <div class="col-sm-12" style="margin-top: 1rem;">
                                <label class="cabinet pull-center">
                                    <figure>
                                        <img src="{{ $icsastudent ? old('avatar',$icsastudent->icsastudentdetails->avatar) :asset('assets/img/personnel_boy.png') }}"
                                             class="gambar img-responsive img-thumbnail"
                                             style="width: 200px; height: 250px;" id="item-img-output"/>
                                        <figcaption style="width: 35%"><i
                                                    class="pull-center material-icons">camera_alt</i>
                                            <input accept="image/png, image/jpeg" type="file"
                                                   class="item-img file center-block" name="avatar" style="width: 15%"/>
                                        </figcaption>
                                    </figure>
                                </label>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                            <h4 class="card-title">Alec Thompson</h4>
                            <p class="card-description">
                                Don't be scared of the truth because we need to restart the human foundation in truth
                                And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                            </p>
                            <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/croppie.css') }}">
    <script type="text/javascript" src="{{ asset('assets/js/croppie.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#course-select').selectpicker();
            $('.mobileIsd').selectpicker();
            $('#schedule-select').selectpicker();
            $('#batch-select').selectpicker();
            $('#instructor-select').selectpicker();
            $('#nationality-select').selectpicker();
            $('#currently_residing-select').selectpicker();

            $(document).delegate('#course-select', 'change', function (event) {
                var id = $(this).val();
                if (typeof id != "undefined" && id != '') {
                    var branchid = '{{ auth()->user()->currentBranch }}';
                    var _token = $('input[name=_token]').val();
                    $.ajax({
                        url: '{{ route("icsaregistration.getbatches") }}',
                        type: 'post',
                        data: {'courseid': id, 'branchid': branchid, _token: _token},
                        dataType: "json",
                        beforeSend: function () {
                            // Show image container
                            $(".loader").show();
                        },
                        success: function (data) {
                            $('#batch-select').html(data.icsabatch);
                            $('#batch-select').selectpicker('refresh');
                        },
                        complete: function (data) {
                            // Hide image container
                            $(".loader").hide();
                        }
                    });
                }
            });

            $(document).delegate('#currently_residing-select', 'change', function (event) {
                var countryid = $(this).val();
                if (typeof countryid != "undefined" && countryid != '') {
                    var _token = $('input[name=_token]').val();
                    $.ajax({
                        url: '{{ route("icsaregistration.getcountrycode") }}',
                        type: 'post',
                        data: {'countryid': countryid, _token: _token},
                        dataType: "json",
                        beforeSend: function () {
                            // Show image container
                            $(".loader").show();
                        },
                        success: function (data) {
                            /*$('.skype').val(data.country);
                            $('.mobile').val(data.country);*/
                            $('.mobileIsd').val(data.country);
                            $('.mobileIsd').selectpicker('refresh');
                        },
                        complete: function (data) {
                            // Hide image container
                            $(".loader").hide();
                        }
                    });
                }
            });

            //$(".gambar").attr("src", "https://user.gadjian.com/static/images/personnel_boy.png");
            var $uploadCrop,
                tempFilename,
                rawImg,
                imageId;

            function readFile(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.upload-demo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawImg = e.target.result;
                    }
                    reader.readAsDataURL(input.files[0]);
                }
                else {
                    swal("Sorry - you're browser doesn't support the FileReader API");
                }
            }

            $uploadCrop = $('#upload-demo').croppie({
                viewport: {
                    width: 200,
                    height: 250,
                },
                enforceBoundary: false,
                enableExif: true
            });
            $('#cropImagePop').on('shown.bs.modal', function () {
                // alert('Shown pop');
                $uploadCrop.croppie('bind', {
                    url: rawImg
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            });

            $('.item-img').on('change', function () {
                imageId = $(this).data('id');
                tempFilename = $(this).val();
                $('#cancelCropBtn').data('id', imageId);
                readFile(this);
            });
            $('#cropImageBtn').on('click', function (ev) {
                /*$uploadCrop.croppie('result', {
                    type: 'base64',
                    format: 'jpeg',
                    size: {width: 150, height: 200}
                }).then(function (resp) {
                    $('#item-img-output').attr('src', resp);
                    $('#cropImagePop').modal('hide');
                });*/

                $uploadCrop.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (response) {
                    var _token = $('input[name=_token]').val();
                    var username = $('.civil_id').val();
                    $.ajax({
                        url: '{{ route("image_crop.upload") }}',
                        type: 'post',
                        data: {"image": response, _token: _token, 'username': username},
                        dataType: "json",
                        success: function (data) {
                            //var crop_image = '<img src="'+data.path+'" />';
                            //$('#uploaded_image').html(crop_image);
                            $('#item-img-output').attr('src', data.path);
                            $('#cropImagePop').modal('hide');
                        }
                    });
                });
            });
            // End upload preview image
        })
    </script>

    <style>
        figcaption {
            display: block;
            cursor: pointer;
        }

        figcaption input.file {
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top: -30px;
        }

        #upload-demo {
            width: 350px;
            height: 350px;
            padding-bottom: 25px;
        }

        figcaption {
            position: absolute;
            bottom: 10%;
            left: 17%;
            color: #fff;
            width: 100%;
            /*padding-left: 9px;
            padding-bottom: 5px;*/
            text-shadow: 0 0 10px #000;
        }
    </style>
@stop