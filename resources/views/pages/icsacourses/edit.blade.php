@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit Branch</h4>
                                <p class="card-category"> Edit Branch name.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('icsacourse.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            {!! Form::model($icsacourse, ['method' => 'PATCH','route' => ['icsacourse.update', $icsacourse->id]]) !!}
                                @csrf
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <strong>Name:</strong>
                                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <strong>Course Fee:</strong>
                                            {!! Form::text('course_fee', null, array('placeholder' => 'Course Fees','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <strong>Duration Month:</strong>
                                            {!! Form::text('duration_month', null, array('placeholder' => 'Duration Month','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <strong>Duration Hours:</strong>
                                            {!! Form::text('duration_hours', null, array('placeholder' => 'Duration Hours','class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <strong>Is Group Course?:</strong>
                                            {!! Form::select('is_group_course', ['0' => 'No', '1' =>'Yes'], old('is_group_course', $icsacourse->is_group_course), array('class' => 'form-control','id' => 'gender-select')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <strong>Want to Display In Registration Form? :</strong>
                                            {!! Form::select('show_in_register_form', ['0' => 'No', '1' => 'Yes'], old('show_in_register_form', $icsacourse->show_in_register_form), array('class' => 'form-control','id' => 'gender-select')) !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop