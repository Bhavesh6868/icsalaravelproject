@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">ICSA Course Management</h4>
                                <p class="card-category"> All ICSA Courses.</p>
                            </div>
                            <div class="pull-right">
                                @if(auth()->user()->canAccess('branch-create'))
                                    <a class="btn btn-success" href="{{ route('icsacourse.create') }}"> Create New
                                        Branch</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table id="dtBasicExample" class="table table-striped table-bordered table-sm"
                                       cellspacing="0" width="100%">
                                    <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($icsacourse as $key => $branch)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $branch->name }}</td>
                                            <td>
                                                <span class="badge {{ $branch->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($branch->status) }}</span>
                                            </td>
                                            <td>
                                                <a class="btn btn-info btn-sm"
                                                   href="{{ route('icsacourse.show',$branch->id) }}"><i class="fa fa-eye"></i> </a>
                                                @if(auth()->user()->canAccess('branch-edit'))
                                                    <a class="btn btn-primary btn-sm"
                                                       href="{{ route('icsacourse.edit',$branch->id) }}"><i
                                                                class="fa fa-edit"></i></a>
                                                @endif
                                                @if(auth()->user()->canAccess('branch-delete'))
                                                    {!! Form::open(['method' => 'DELETE','route' => ['icsacourse.destroy', $branch->id],'style'=>'display:inline', 'class' => 'delete-icsacourse']) !!}
                                                    @csrf
                                                    {!! Form::submit($branch->status == 'active' ? 'Disabled' : 'Active', ['class' => $branch->status == 'active' ? 'btn btn-danger btn-sm' : 'btn btn-success btn-sm', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endif

                                                {{--<form action="{{ route('icsacourse.destroy', $branch->id) }}"
                                                      method="POST">

                                                    <a class="btn btn-info"
                                                       href="{{ route('icsacourse.show',$branch->id) }}">Show</a>

                                                    @if(auth()->user()->canAccess('branch-edit'))
                                                        <a class="btn btn-primary"
                                                           href="{{ route('icsacourse.edit',$branch->id) }}">Edit</a>
                                                    @endif
                                                    @if(auth()->user()->canAccess('branch-delete'))
                                                        @csrf
                                                        @method('DELETE')

                                                        <button type="submit" title="delete" class="btn btn-danger">
                                                            <i class="fa fa-eye"></i>

                                                        </button>
                                                    @endif
                                                </form>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
        /*$('.delete-icsacourse').submit(function (e) {
            e.preventDefault();
            alert('ss')

            if (confirm("Are you sure you want to delete?")) {
                $(this).submit();
            }
        });*/
    </script>
    <style>
        #dtBasicExample_filter {
            float: right !important;
        }
    </style>
@stop