@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Instructor Details</h4>
                                <p class="card-category"> Display user information.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('instructor.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">
                            {{--<div class="row">
                                <div class="col-lg-12 margin-tb">
                                    --}}{{--<div class="pull-left">
                                        <h2> Show User</h2>
                                    </div>--}}{{--
                                    <div class="pull-right">
                                        <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
                                    </div>
                                </div>
                            </div>--}}

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-9">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {{ $instructor->name }}
                                    </div>
                                    <div class="form-group">
                                        <strong>Email:</strong>
                                        {{ $instructor->user->email }}
                                    </div>
                                    <div class="form-group">
                                        <strong>Speaking Name:</strong>
                                        {{ $instructor->speaking_name }}
                                    </div>
                                    {{--<div class="form-group">
                                        <strong>Speaking Name:</strong>
                                        {{ $instructor->instructorcourse }}
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop