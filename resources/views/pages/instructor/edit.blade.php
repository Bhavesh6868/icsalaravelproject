@extends('layouts.default')
@section('content')
    <div class="spinner-border" id="loader" style="display: none"></div>
    <div class="content">
        <div class="container-fluid">
            {!! Form::model($instructor, ['method' => 'PATCH', ' enctype="multipart/form-data"', 'route' => ['instructor.update', $instructor->id]]) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit Instructor</h4>
                                <p class="card-category"> Please Enter all details</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('instructor.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">

                                        <strong>User:</strong>
                                        {!! Form::select('user', $user, old('user', $instructor->user_id), array('class' => 'form-control','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'sel_user', 'disabled')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">

                                        <strong>Courses:</strong>
                                        {!! Form::select('course[]', $course, $oldCourse, array('class' => 'form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'sel_course', '')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Basic Salary:</strong>
                                        {!! Form::text('basic_salary', null, array('placeholder' => 'Basic Salary','class' => 'form-control basic_salary' ,'')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>User Name:</strong>
                                        {!! Form::text('username', null, array('placeholder' => 'User Name','class' => 'form-control username' ,'readonly')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control name' ,'readonly')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Email:</strong>
                                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control email' ,'readonly')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Mobile:</strong>
                                        {!! Form::number('mobile', null, array('placeholder' => 'mobile','class' => 'form-control mobile' ,'readonly' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Birth Date:</strong>
                                        {!! Form::date('birthdate', null, array('placeholder' => 'Birth date','class' => 'form-control birthdate' ,'readonly' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Civil ID:</strong>
                                        {!! Form::text('civil_id', null, array('placeholder' => 'Civil ID','class' => 'form-control civil_id' ,'readonly' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Position:</strong>
                                        {!! Form::text('position', null, array('placeholder' => 'Position','class' => 'form-control position' ,'readonly', 'autocomplete' => 'off' )) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Gender:</strong>
                                        {!! Form::select('gender', ['' => 'select Gender', 'male' => 'Male', 'female'=>'Female'], null, array('class' => 'form-control gender' ,'disabled', 'id' => 'gender-select')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <div class="form-group">
                                        <strong>Nationality:</strong>
                                        {!! Form::text('nationality', null, array('placeholder' => 'Nationality','class' => 'form-control nationality' ,'readonly' )) !!}
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')

    <script type="text/javascript">
        $(document).ready(function () {

            if(typeof $('#sel_user').val() != "undefined") {
                $('#sel_user').trigger('change');
            }
            $('#sel_user').selectpicker();
            $('#sel_course').selectpicker();

        })
        $(document).delegate('#sel_user', 'change', function (event) {
            //$('#sel_user').change(function () {
            //alert('in')
            var id = $(this).val();
            var _token = $('input[name=_token]').val();
            $.ajax({
                url: '{{ route("users.getdetails") }}',
                type: 'post',
                data: {'userid': id, _token: _token},
                dataType: "json",
                beforeSend: function () {
                    // Show image container
                    $(".loader").show();
                },
                success: function (data) {
                    $('.username').val(data.userdetail.username);
                    $('.name').val(data.userdetail.name);
                    $('.email').val(data.userdetail.email);
                    $('.mobile').val(data.userdetail.mobile);
                    $('.birthdate').val(data.userdetail.birthdate);
                    $('.civil_id').val(data.userdetail.civil_id);
                    $('.position').val(data.userdetail.position);
                    $('.gender').val(data.userdetail.gender);
                    $('.nationality').val(data.userdetail.nationality)
                    //var crop_image = '<img src="'+data.path+'" />';
                    //$('#uploaded_image').html(crop_image);
                },
                complete: function (data) {
                    // Hide image container
                    $(".loader").hide();
                }
            });
        })
    </script>

    <style>
        figcaption {
            display: block;
            cursor: pointer;
        }

        figcaption input.file {
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top: -30px;
        }

        #upload-demo {
            width: 350px;
            height: 350px;
            padding-bottom: 25px;
        }

        figcaption {
            position: absolute;
            bottom: 10%;
            left: 17%;
            color: #fff;
            width: 100%;
            /*padding-left: 9px;
            padding-bottom: 5px;*/
            text-shadow: 0 0 10px #000;
        }
    </style>
@stop