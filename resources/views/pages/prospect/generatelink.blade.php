@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-left">
                                        <h4 class="card-title ">ICSA Student Payment Details</h4>
                                        <p class="card-category"> All ICSA Students Payment.</p>
                                    </div>
                                    <div class="pull-right">
                                        @if(auth()->user()->canAccess('icsastudent-create'))
                                            <a style="padding: .54rem 2.14rem; margin-top: 0" class="btn btn-success"
                                               href="{{ route('icsastudents.create') }}"> Enroll New Student</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('route' => 'generate.linkshow', 'method'=>'POST')) !!}
                            <br>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-3" style="">
                                    <div class="">
                                        {!! Form::select('frequency', [
                                        '' => 'Select Frequency',
                                        'crs' => 'Course Reservation'
                                        ], ($frequency ? $frequency : null), array('class' => 'form-control frequency','id' => 'frequency-select', 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4" style="padding-left: 0">
                                    <div class="">
                                        {!! Form::select('course_id', $courses, ($course_id ? $course_id : null), array('class' => 'form-control course_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'course-select', 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3" style="padding-left: 0">
                                    <div class="">
                                        {!! Form::select('branch_id', $branches, ($branch_id ? $branch_id : null), array('class' => 'form-control branch_id','', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select', 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-1" style="padding-left: 0">
                                    <div class="">
                                        <button type="submit" style="padding: .54rem 2.14rem; margin-top: 0;"
                                                class="btn btn-primary" title="Search Data"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        @if($course_id != 'null')
                            <div class="card-body">
                                {{--<div class="input-group">
                                    <input class="col-sm-11" id="foo" type="text" value="{{ route('refer.link', ['user'=>auth()->user()->username, 'course'=> $course_id, 'branch'=>$branch_id]) }}">
                                    <span class="input-group-button">
      <button class="btn-lg" type="button" data-clipboard-demo="" data-clipboard-target="#foo">
        <img class="clippy" src="https://clipboardjs.com/assets/images/clippy.svg" width="13" alt="Copy to clipboard">
      </button>
    </span>
                                </div>--}}
                                <div class="contact">
                                    <div class="btn-group col-lg-12">
                                        <a class="col-lg-11 btn btn-primary btn-email" href="{{ route('refer.link', ['user'=>auth()->user()->username, 'course'=> $course_id, 'branch'=>$branch_id]) }}" target="_blank">{{ route('refer.link', ['user'=>auth()->user()->username, 'course'=> $course_id, 'branch'=>$branch_id]) }}</a>
                                        <button type="button" class="col-lg-1 btn btn-outline-primary btn-copy js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="{{ route('refer.link', ['user'=>auth()->user()->username, 'course'=> $course_id, 'branch'=>$branch_id]) }}" title="Copy to clipboard">
                                            <!-- icon from google's material design library -->
                                            <img class="clippy" src="https://clipboardjs.com/assets/images/clippy.svg" width="23" alt="Copy to clipboard">
                                        </button>
                                    </div>
                                </div>
                                {{--{{ route('refer.link',['user'=>auth()->user()->username, 'branch'=> $course_id, 'course'=>$branch_id]) }}--}}
                               {{-- <a href="{{ route('refer.link', ['user'=>auth()->user()->username, 'course'=> $course_id, 'branch'=>$branch_id]) }}">Refer
                                    Link</a>--}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-script')
    <script type="text/javascript">

        function copyToClipboard(text, el) {
            var copyTest = document.queryCommandSupported('copy');
            var elOriginalText = el.attr('data-original-title');

            if (copyTest === true) {
                var copyTextArea = document.createElement("textarea");
                copyTextArea.value = text;
                document.body.appendChild(copyTextArea);
                copyTextArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'Copied!' : 'Whoops, not copied!';
                    el.attr('data-original-title', msg).tooltip('show');
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(copyTextArea);
                el.attr('data-original-title', elOriginalText);
            } else {
                // Fallback if browser doesn't support .execCommand('copy')
                window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
            }
        }

        $(document).ready(function() {
            // Initialize
            // ---------------------------------------------------------------------

            // Tooltips
            // Requires Bootstrap 3 for functionality
            $('.js-tooltip').tooltip();

            // Copy to clipboard
            // Grab any text in the attribute 'data-copy' and pass it to the
            // copy function
            $('.js-copy').click(function() {
                var text = $(this).attr('data-copy');
                var el = $(this);
                copyToClipboard(text, el);
            });
        });

        $(document).ready(function () {
            $('#course-select').selectpicker();
            $('#paymentod-select').selectpicker();

            $('#1, #2, #3, #4, #5, #6, #7, #8').DataTable({
                /* "columnDefs": [
                     { "orderable": false, "targets": 4 }
                 ]*/
                "bSort": false
            });
            //$('.dataTables_length').addClass('bs-select');
        });
        $(function () {
            // on init
            $(".table-hideable .hide-col").each(HideColumnIndex);

            // on click
            $('.hide-column').click(HideColumnIndex)

            function HideColumnIndex() {
                var $el = $(this);
                var $cell = $el.closest('th,td')
                var $table = $cell.closest('table')

                // get cell location - https://stackoverflow.com/a/4999018/1366033
                var colIndex = $cell[0].cellIndex + 1;

                // find and hide col index

                $table.find("tbody tr, thead tr")
                    .children(":nth-child(" + colIndex + ")")
                    .addClass('hide-col');
                $('.hide-col').hide();
                // show restore footer
                $table.find(".footer-restore-columns").show()
            }

            // restore columns footer
            $(".restore-columns").click(function (e) {
                $('.hide-col').show();

                var $table = $(this).closest('table')
                $table.find(".footer-restore-columns").hide()
                $table.find("th, td")
                    .removeClass('hide-col');

            });

            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            })

        })
    </script>
    <style>
        .table-hideable td,
        .table-hideable th {
            width: auto;
            transition: width .5s, margin .5s;
        }

        .btn-condensed.btn-condensed {
            padding: 0 5px;
            box-shadow: none;
        }

        /*.hide-col {
            width: 0px !important;
            height: 0px !important;
            display: block !important;
            overflow: hidden !important;
            margin: 0 !important;
            padding: 0 !important;
            border: none !important;
        }*/
        /*.dataTables_filter {
            float: right !important;
        }*/
    </style>
@stop