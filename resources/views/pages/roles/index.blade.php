@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Role Management</h4>
                                <p class="card-category"> All Roles Related to system access.</p>
                            </div>
                            <div class="pull-right">
                                @if(auth()->user()->canAccess('role-create'))
                                    <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table id="dtBasicExample" class="table" width="100%">
                                    <thead class=" text-primary">
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    @foreach ($roles as $key => $role)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>
                                                <span class="badge {{ $role->status == 'active' ? 'badge-success' : 'badge-danger' }}">{{ ucfirst($role->status) }}</span>
                                            </td>
                                            <td>
                                                <a class="btn btn-info btn-sm"
                                                   href="{{ route('roles.show',$role->id) }}"><i class="fa fa-eye"></i> </a>
                                                @if(auth()->user()->canAccess('role-edit'))
                                                    <a class="btn btn-primary btn-sm"
                                                       href="{{ route('roles.edit',$role->id) }}"><i class="fa fa-pencil"></i> </a>
                                                @endif
                                                @if(auth()->user()->canAccess('role-delete'))
                                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline', 'class' => 'delete-role']) !!}
                                                    {!! Form::submit($role->status == 'active' ? 'Make Disabled' : 'Make Active', ['class' => $role->status == 'active' ? 'btn btn-danger btn-sm' : 'btn btn-success btn-sm', 'onclick' => "return confirm('Are you sure you want to change status?')"]) !!}
                                                    {!! Form::close() !!}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dtBasicExample').DataTable();
            $('.dataTables_length').addClass('bs-select');
        });
        /*$('.delete-role').submit(function (e) {
            e.preventDefault();

            if (confirm("Are you sure you want to delete?")) {
                $(this).submit();
            }
        });*/
    </script>
    <style>
        #dtBasicExample_filter {
            float: right !important;
        }
    </style>
@stop