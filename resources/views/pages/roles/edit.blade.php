@extends('layouts.default')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="pull-left">
                                <h4 class="card-title ">Edit Role</h4>
                                <p class="card-category"> Edit Role name and select permissions.</p>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-warning" href="{{ route('roles.index') }}"> Back</a>
                            </div>
                        </div>
                        <div class="card-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                {{--<div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <strong>Branch:</strong>
                                        {!! Form::select('branch[]', $branches, $roleBranch, array('class' => 'form-control','multiple', 'data-live-search="true"', 'data-style="btn-primary"','id' => 'branch-select')) !!}
                                    </div>
                                </div>--}}
                                {{--<div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Permission:</strong>
                                        <br/>
                                        @foreach($permission as $value)
                                            <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                                {{ $value->name }}</label>
                                            <br/>
                                        @endforeach
                                    </div>
                                </div>--}}
                                <div class="col-md-12 border-top border-bottom m-1"><strong>
                                        <h2>Permissions</h2></strong></div>
                                <div class="col-md-3">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                         aria-orientation="vertical">
                                        @foreach($permiarry as $key => $value)
                                            <a class="nav-link {{ $i++ == 0 ? 'active' : '' }}" href="#{{$key}}"
                                               data-toggle="tab">{{ ucfirst($key) }}
                                                <div class="ripple-container"></div>
                                            </a>
                                        @endforeach

                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        {{ $i = '' }}
                                        @foreach($permiarry as $k => $value)
                                            <div class="tab-pane {{ $i++ == 0 ? 'active' : '' }}" id="{{ $k  }}">
                                                @foreach($value  as $ke => $val)
                                                    {{--<label>{{ Form::checkbox('permission[]', $val[$ke]['id'], in_array($val[$ke]['id'], $rolePermissions) ? true : false, array('class' => 'name')) }} {{ $val[$ke]['name'] }}</label>
                                                    <br/>--}}
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            {{ Form::checkbox('permission[]', $val[$ke]['id'], in_array($val[$ke]['id'], $rolePermissions) ? true : false, array('class' => 'name form-check-input')) }} {{ $val[$ke]['name'] }}
                                                            <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom-script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#branch-select').selectpicker();
        });
    </script>
@stop