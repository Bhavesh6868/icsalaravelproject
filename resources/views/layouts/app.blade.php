<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="">

    @yield('content')
</div>
</body>
</html>