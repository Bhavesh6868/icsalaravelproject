<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="wrapper ">
    <div class="loader"></div>
    @include('includes.sidebar')
    <div class="main-panel">
        @include('includes.header')
        @yield('content')
        @include('includes.footer')
        @yield('custom-script')
    </div>
</div>
<div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-cog fa-2x"> </i>
        </a>
        <ul class="dropdown-menu">
            <li class="header-title"> Roles</li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger active-color">
                    <div class="badge-colors ml-auto mr-auto">
                        <span class="badge filter badge-purple custombadge" data-color="purple"></span>
                        <span class="badge filter badge-azure custombadge" data-color="azure"></span>
                        <span class="badge filter badge-green custombadge" data-color="green"></span>
                        <span class="badge filter badge-warning custombadge" data-color="orange"></span>
                        <span class="badge filter badge-danger custombadge" data-color="danger"></span>
                        <span class="badge filter badge-rose active custombadge" data-color="rose"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            {{--<li class="header-title">Images</li>
            <li class="active">
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="{{ asset('assets/img/sidebar-1.jpg') }}" alt="">
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="{{ asset('assets/img/sidebar-2.jpg') }}" alt="">
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="{{ asset('assets/img/sidebar-3.jpg') }}" alt="">
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src="{{ asset('assets/img/sidebar-4.jpg') }}" alt="">
                </a>
            </li>--}}

            <li class="button-container text-center">
                @foreach(auth()->user()->roles as $data)
                    {{--<button id="twitter" class="btn btn-round float-left"><i class="fa fa-twitter"></i> &middot; {{ $data->name  }}</button>--}}
                    <a class="btn {{auth()->user()->currentRole == $data->id ? 'btn-primary' : 'btn btn-light black'}}" href="{{ route('users.changeRole',$data->id) }}" style="{{auth()->user()->currentRole == $data->id ? '' : 'color:black'}}"> {{ $data->name  }}</a>
                @endforeach
                <br>
            </li>
        </ul>
    </div>
</div>

</body>
</html>