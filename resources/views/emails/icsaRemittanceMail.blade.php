<!DOCTYPE html>
<html>
<head>
    <title>ItsolutionStuff.com</title>
</head>
<body>
Dear ICSA Team,
<br>Kindly find the attached proof of remittance. <br> <br>
<style>table, td, th {
        border: 1px solid black;
        padding: 10px;
        margin: 0;
    }

    th, td {
        white-space: nowrap;
    } </style>
<table border="1px" width="100%">
    <tr style="background-color: #c5e0b3; color: #080808; padding: 4%;">
        <td>E/No.</td>
        <td>Name</td>
        <td>Course</td>
        <td>Branch</td>
        <td>Bank</td>
        <td>Amount</td>
        <td>Transaction No</td>
        <td>Remittance Date</td>
        <td>Philippine Peso</td>
        <td>Enroll Date</td>
    </tr>
    @foreach($details['paymentdetails'] as $detail)
        <tr>
            <td>{{ $detail['enrollment_id'] }}</td>
            <td>{{ $detail['fullname'] }}</td>
            <td>{{ $detail['course'] }}</td>
            <td>{{ $detail['location'] }}</td>
            <td>{{ $detail['paymentmethods'] }}</td>
            <td>{{ $detail['amount'] }}</td>
            <td>{{ $detail['transaction_no'] }}</td>
            <td>{{ date('Y-m-d', strtotime($detail['transaction_date'])) }}</td>
            <td>{{ $detail['philippine_peso'] }}</td>
            <td>{{ $detail['enrollment_date'] }}</td>
        </tr>
    @endforeach
</table>
Please Click on <a href="https://icsa.us/icsa-system/icsashortcourse_remittance_report.php">this link</a> for check
Remittance Proof Report.<br> Thank You';

<p>Thank you</p>
</body>
</html>