<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
<link rel="icon" type="image/png" href="{{ asset('Images/logo128.png') }} ">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>
    ICSA Kuwait
</title>
<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- CSS Files -->
<link href="{{ asset('assets/css/material-dashboard.css') }}" rel="stylesheet" />
<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet" />
<!-- Bootstrap core CSS -->
<link href="{{ asset('theevent/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
{{--
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
--}}

<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
<style>
    .dropdown.bootstrap-select .btn{
        padding: 0.64rem 1.14rem !important;;
        margin: 0 !important;
    }
    .main-panel>.content {
        margin-top: 40px !important;
    }
    .custombadge {
        display: inline-block !important;
    }
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php  echo asset('Images/loader.gif') ?>') 50% 50% no-repeat rgb(249, 249, 249);
        opacity: .8;
    }
    .form-group.has-error {
        color: #bd2130;
    }
</style>
