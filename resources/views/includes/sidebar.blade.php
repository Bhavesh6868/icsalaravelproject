<div class="sidebar" data-color="purple" data-background-color="white"
     data-image="{{ asset('assets/img/sidebar-1.jpg') }}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="/dashboard" class="simple-text logo-normal">
            <img src="{{ asset('Images/logo128.png') }}" class="img pull-center" style="width:40px">ICSA
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            {{--{{ auth()->user()->currentRole == 2 && auth()->user()->can('role-edit')  }} == {{ auth()->user()->currentRole }} == --}}
            {{--{{ dd(Session::get('permission_array')) }}--}}
            {{--{{ auth()->user()->canAccess('user-delete') }}--}}
            <li class="nav-item {{ request()->is('dashboard') ? 'active' : ''  }}  ">
                <a class="nav-link" href="/dashboard">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item pmd-user-info">
                <a data-toggle="collapse" href="#collapseExample" class="nav-link btn-user media align-items-center">
                    <i class="material-icons">admin_panel_settings</i>
                    <p style="width: 100%">Administrator</p>
                    <i class="material-icons md-light ml-2 pmd-sm pull-right">more_vert</i>
                </a>
                <ul class="collapse {{ request()->is('users*') || request()->is('branch*') || request()->is('roles*') || request()->is('instructor*') || request()->is('icsacourse*') || request()->is('source*') || request()->is('icsabatch*') || request()->is('paymentmethod') || request()->is('emailtemplate*') || request()->is('adscourse*') ? 'show' : ''  }}"
                    id="collapseExample" data-parent="#basicSidebar" style="list-style: none">
                    @if(auth()->user()->canAccess('user-list'))
                        <li class="nav-item {{ (request()->is('users') || request()->is('users/*')) ? 'active' : ''  }}">
                            <a class="nav-link" href="/users">
                                <i class="material-icons">person</i>
                                <p>Users</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('instructor-list'))
                        <li class="nav-item {{ (request()->is('instructor') || request()->is('instructor/*')) ? 'active' : ''  }}">
                            <a class="nav-link" href="/instructor">
                                <i class="material-icons">person</i>
                                <p>Instructor</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('icsacourse-list'))
                        <li class="nav-item {{ (request()->is('icsacourse') || request()->is('icsacourse/*')) ? 'active' : ''  }}">
                            <a class="nav-link" href="/icsacourse">
                                <i class="material-icons">person</i>
                                <p>ICSA Course</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('branch-list'))
                        <li class="nav-item {{ (request()->is('branch') || request()->is('branch/*')) ? 'active' : ''  }}">
                            <a class="nav-link" href="/branch">
                                <i class="material-icons">device_hub</i>
                                <p>Branch</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('role-list'))
                        <li class="nav-item {{ request()->is('roles') || request()->is('roles/*') ? 'active' : ''  }}">
                            <a class="nav-link" href="/roles">
                                <i class="material-icons">how_to_reg</i>
                                <p>Roles</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('source-list'))
                        <li class="nav-item {{ request()->is('source') || request()->is('source/*') ? 'active' : ''  }}">
                            <a class="nav-link" href="/source">
                                <i class="material-icons">how_to_reg</i>
                                <p>Source</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('icsabatch-list'))
                        <li class="nav-item {{ request()->is('icsabatch') || request()->is('icsabatch/*') ? 'active' : ''  }}">
                            <a class="nav-link" href="/icsabatch">
                                <i class="material-icons">how_to_reg</i>
                                <p>ICSA Batch</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('paymentmethod-list'))
                        <li class="nav-item {{ request()->is('paymentmethod') || request()->is('paymentmethod/*') ? 'active' : ''  }}">
                            <a class="nav-link" href="/paymentmethod">
                                <i class="material-icons">monetization_on</i>
                                <p>Payment Method</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('emailtemplate-list'))
                        <li class="nav-item {{ request()->is('emailtemplate') || request()->is('emailtemplate/*') ? 'active' : ''  }}">
                            <a class="nav-link" href="/emailtemplate">
                                <i class="material-icons">email</i>
                                <p>Email Templates</p>
                            </a>
                        </li>
                    @endif
                    @if(auth()->user()->canAccess('adscourse-list'))
                        <li class="nav-item {{ request()->is('adscourse') || request()->is('adscourse/*') ? 'active' : ''  }}">
                            <a class="nav-link" href="/adscourse">
                                <i class="material-icons">category</i>
                                <p>Ads Courses</p>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
            <li class="nav-item pmd-user-info">
                <a data-toggle="collapse" href="#collapseIcsa" class="nav-link btn-user media align-items-center">
                    <i class="material-icons">foundation</i>
                    <p style="width: 100%">ICSA Registration</p>
                    <i class="material-icons md-light ml-2 pmd-sm pull-right">more_vert</i>
                </a>
                <ul class="collapse {{ request()->is('icsastudents*') || request()->is('icsapayment*') ? 'show' : ''  }}"
                    id="collapseIcsa" data-parent="#basicSidebar" style="list-style: none">
                    <li class="nav-item {{ (request()->routeIs('icsastudents.create')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('icsastudents.create') }}">
                            <i class="material-icons">person</i>
                            <p>New Student</p>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('icsastudents/editenrollment') || request()->is('icsastudents/*/edit')) ? 'active' : ''  }}">
                        <a class="nav-link" href="/icsastudents/editenrollment">
                            <i class="material-icons">edit</i>
                            <p>Edit Student</p>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('icsastudents.index') || request()->routeIs('icsastudents.show') || request()->routeIs('icsastudents.searchenroll')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('icsastudents.index') }}">
                            <i class="material-icons">list</i>
                            <p>List Of Student</p>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('icsapayment/addpayment') || request()->is('icsapayment/addpayment/*')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('icsapayment.addpayment') }}">
                            <i class="material-icons">payments</i>
                            <p>Add Payment</p>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('icsapaymentrefund/refundpayment') || request()->is('icsapaymentrefund/refundpayment/*')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('icsapayment.refundpayment') }}">
                            <i class="material-icons">payments</i>
                            <p>Refund Payment</p>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('icsapayment/printinvoice') || request()->is('icsapayment/printinvoice/*')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('icsapayment.showinvoice') }}">
                            <i class="material-icons">print</i>
                            <p>Print Invoice</p>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('icsapayment.index') || request()->routeIs('icsapayment.searchenroll')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('icsapayment.index') }}">
                            <i class="material-icons">list</i>
                            <p>Payment Report</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item pmd-user-info">
                <a data-toggle="collapse" href="#collapseMarketer" class="nav-link btn-user media align-items-center">
                    <i class="material-icons">groups</i>
                    <p style="width: 100%">Prospects</p>
                    <i class="material-icons md-light ml-2 pmd-sm pull-right">more_vert</i>
                </a>
                <ul class="collapse {{ request()->is('generate*') ? 'show' : ''  }}"
                    id="collapseMarketer" data-parent="#basicSidebar" style="list-style: none">
                    <li class="nav-item {{ (request()->routeIs('generate.link') || request()->routeIs('generate.linkshow')) ? 'active' : ''  }}">
                        <a class="nav-link" href="{{ route('generate.link') }}">
                            <i class="material-icons">qr_code</i>
                            <p>Generate Link</p>
                        </a>
                    </li>
                </ul>
            </li>
            @if(auth()->user()->canAccess('role-list'))
                <li class="nav-item {{ request()->is('user') ? 'active' : ''  }}">
                    <a class="nav-link" href="/user">
                        <i class="material-icons">person</i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('tables') ? 'active' : ''  }}">
                    <a class="nav-link" href="/tables">
                        <i class="material-icons">content_paste</i>
                        <p>Table List</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('typography') ? 'active' : ''  }}">
                    <a class="nav-link" href="/typography">
                        <i class="material-icons">library_books</i>
                        <p>Typography</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('icons') ? 'active' : ''  }}">
                    <a class="nav-link" href="/icons">
                        <i class="material-icons">bubble_chart</i>
                        <p>Icons</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('map') ? 'active' : ''  }}">
                    <a class="nav-link" href="/map">
                        <i class="material-icons">location_ons</i>
                        <p>Maps</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('notifications') ? 'active' : ''  }}">
                    <a class="nav-link" href="/notifications">
                        <i class="material-icons">notifications</i>
                        <p>Notifications</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>